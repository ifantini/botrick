<?php
require_once("../conexion.php");
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
}
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php")?>
	<div id="header">
		<div>
			<a href="/index.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
	<?php $select="herramientas"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_tool.php");?>
              
          </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
<div class="list">
        <h3><?php echo $text["Planes entrenamiento"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["planentrenamiento_texto1"][$_SESSION[IDIOMA]];?>
        	<br><br>
        </p>
        <p><?php echo $text["planentrenamiento_texto2"][$_SESSION[IDIOMA]];?><br><br></p>
				<form style="float: left;" action="planentreno.php" method="post">
                <table cellspacing="2">
                  <tr>
                    <td style="padding-bottom:15px; padding-right: 15px;"><select name="cbo_pos" size="1" id="cbo_pos" style="width: 100%">
                    <?php 
                    $sentencia = pg_query($con,"select nombre,id_planes_entrenamiento from planes_entrenamiento where id_listado_plan_entrenamiento=1 order by id_planes_entrenamiento;");
					while($rs = pg_fetch_array($sentencia)){?>
                     <option value="<?php echo $rs['id_planes_entrenamiento'];  ?>" <?php if($_POST['cbo_pos']==$rs['id_planes_entrenamiento']){echo " selected";}?>  ><?php echo $rs['nombre']; ?> </option><?php } ?>
                    </select></td>
                    <td style="padding-bottom: 15px;"   colspan="2" align="center"><input name="Desplegar" type="submit" id="Desplegar" value="Desplegar"></td>
                  </tr>
                </table>
			</form>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Porteria"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Defensa"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Jugadas"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Asistencias"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Lateral"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Anotacion"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Balon Parado"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $valor = FALSE;
		  if($_POST['Desplegar']){//if que recibe la info del formulario
		  	$pos = is_numeric($_POST['cbo_pos'])?$_POST['cbo_pos']:0;
			$sentencia = pg_query($con,"select * from habilidad_plan_entrenamiento where id_planes_entrenamiento=$pos order by Edad;");
		  		while($rs_plan = pg_fetch_array($sentencia)){
					$valor = TRUE;
		  ?>
          <tr class="modo1">
            <th><?php echo $rs_plan['edad'];?></td>
            <td><?php echo $rs_plan['porteria'];?></td>
            <td><?php echo $rs_plan['defensa'];?></td>
            <td><?php echo $rs_plan['jugadas'];?></td>
            <td><?php echo $rs_plan['pases'];?></td>
            <td><?php echo $rs_plan['lateral'];?></td>
            <td><?php echo $rs_plan['anotacion'];?></td>
            <td><?php echo $rs_plan['balonparado'];?></td>
          </tr>
          <?php
				}//end while
		  }//end if
		  ?>
        </table>    
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>
