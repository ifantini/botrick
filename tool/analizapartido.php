<?php
require_once("../conexion.php");
require_once("../nombres.php");
require '../PHT.php';
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
}
require_once("../textos.php");
require_once("../head.php");
require_once("funciones_tool.php");
?>
<?php $id_partido=is_numeric($_GET['id'])?$_GET['id']:0;?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																								
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<?php $select="herramientas"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />																																																																																																
			<div>	
				<?php include_once("menu_tool.php");?>     
				<?php //$test = file_get_contents('http://botrick.altirogames.com/tool/test.py'); echo $test;?>                         
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
			
		</div>
		
 		<div class="list">
 		<h3><?php echo $text["buscar_partido"][$_SESSION[IDIOMA]];?></h3>
 		<form style="float: left;" action="analizapartido.php" method="get">
 		<table cellspacing="2">
 		<tr>
 		<td style="padding-bottom:15px; padding-right: 15px;"><input type="text" name="id" value="<?php echo $id_partido;?>"></td>
 		<td style="padding-bottom: 15px;"   colspan="2" align="center"><input type="submit" value="Buscar"></td>
 		</tr>
 		</table>
 		</form>
        <?php 
        if($id_partido>0){
        $consultar = pg_query($con,"select a.*,b.nombre hnombre,b.def_der hdd,b.def_cen hdc,b.def_izq hdi,b.def_ind hdin,b.medio hm,b.ata_der had,b.ata_cen hac,b.ata_izq hai,b.ata_ind hain,c.nombre anombre,c.def_der atdd,c.def_cen adc,c.def_izq adi,c.def_ind adin,c.medio am,c.ata_der aad,c.ata_cen aac,c.ata_izq aai,c.ata_ind aain,b.tactica ht,b.nivel_tactica hnt,c.tactica awt,c.nivel_tactica ant,d.nombre honomtac,e.nombre awnomtac from partido a left join partido_calificacion b on a.id_equipo_home=b.id_equipo and a.id_partido=b.id_partido left join partido_calificacion c on a.id_equipo_away=c.id_equipo and a.id_partido=c.id_partido left join nombres d on b.tactica=d.id_hattrick left join nombres e on c.tactica=e.id_hattrick where d.tipo='tactica' and e.tipo='tactica' and e.idioma='$_SESSION[IDIOMA]' and d.idioma='$_SESSION[IDIOMA]' and a.id_partido=$id_partido");
		$rs = pg_fetch_array($consultar);
        ?>
        <h3><?php echo $text["Calificaciones"][$_SESSION[IDIOMA]];?></h3>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla sortable">
        	<tr>
				<td/><th colspan="2"><?php echo $rs['hnombre'];?></th><th><?php echo $rs['gol_local'].' - '.$rs['gol_visita'];?></th><th colspan="2"><?php echo $rs['anombre'];?></th><td/>
        	</tr>
        	<tr>
				<th/><th><?php echo $text["denominacion"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["calificacion"][$_SESSION[IDIOMA]];?></th>
				<th style="width: 68px;"><?php echo $text["dominio"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["calificacion"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["denominacion"][$_SESSION[IDIOMA]];?></th><th/>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hdd']; $v2=$rs['aai'];?>
        		<td><?php echo $text["Defensa derecha"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Ataque izquierdo"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hdc']; $v2=$rs['aac'];?>
        		<td><?php echo $text["Defensa central"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Ataque central"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hdi']; $v2=$rs['aad'];?>
        		<td><?php echo $text["Defensa izquierda"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Ataque derecho"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hdin']; $v2=$rs['aain'];?>
        		<td><?php echo $text["Defensa libres indirectos"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Ataque libres indirectos"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr><td>&nbsp;</td></tr>
        	<tr class="modo3"><?php $v1=$rs['hm']; $v2=$rs['am'];?>
        		<td><?php echo $text["Mediocampo"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Mediocampo"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr><td>&nbsp;</td></tr>
        	<tr class="modo3"><?php $v1=$rs['had']; $v2=$rs['adi'];?>
        		<td><?php echo $text["Ataque derecho"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Defensa izquierda"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hac']; $v2=$rs['adc'];?>
        		<td><?php echo $text["Ataque central"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Defensa central"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hai']; $v2=$rs['atdd'];?>
        		<td><?php echo $text["Ataque izquierdo"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Defensa derecha"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr class="modo1"><?php $v1=$rs['hain']; $v2=$rs['adin'];?>
        		<td><?php echo $text["Ataque libres indirectos"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo denominacion($v1, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $v1;?></td>
        		<td class="modo2"><div style="background-color: #33EE00; height: 100%; position: absolute; left: 0px; width: <?php echo number_format(100*$v1/($v1+$v2),0);?>%;"></div><span style="position: absolute;">&nbsp;</span><div style="position: absolute; margin: 3px; width: 68px;"><?php echo number_format(100*$v1/($v1+$v2),0).'% - '.number_format(100*$v2/($v1+$v2),0).'%';?></div></td>
        		<td><?php echo $v2;?></td>
        		<td><?php echo denominacion($v2, "$_SESSION[IDIOMA]", "calificacion",$con);?></td>
        		<td><?php echo $text["Defensa libres indirectos"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        	<tr><td>&nbsp;</td></tr>
        	<tr class="modo3">
        		<td><?php echo $text["tactica"][$_SESSION[IDIOMA]];?></td>
        		<td><?php echo $rs['honomtac'];?></td>
        		<td><?php echo $rs['hnt'];?></td>
        		<td></td>
        		<td><?php echo $rs['ant'];?></td>
        		<td><?php echo $rs['awnomtac'];?></td>
        		<td><?php echo $text["tactica"][$_SESSION[IDIOMA]];?></td>
        	</tr>
        </table>
        <?php 
        $consultar = pg_query($con,"select 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Derecha' then 1 else 0 end) hdg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Derecha' then 1 else 0 end) hdn, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Centro' then 1 else 0 end) hmg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Centro' then 1 else 0 end) hmn,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Izquierda' then 1 else 0 end) hig, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Izquierda' then 1 else 0 end) hin,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Penal' then 1 else 0 end) hpg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Penal' then 1 else 0 end) hpn,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Tiro libre directo' then 1 else 0 end) hldg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Tiro libre directo' then 1 else 0 end) hldn,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Normal' and zona='Tiro libre indirecto' then 1 else 0 end) hlig, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Normal' and zona='Tiro libre indirecto' then 1 else 0 end) hlin,
        
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Derecha' then 1 else 0 end) hdg_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Derecha' then 1 else 0 end) hdn_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Centro' then 1 else 0 end) hmg_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Centro' then 1 else 0 end) hmn_ca,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Izquierda' then 1 else 0 end) hig_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Izquierda' then 1 else 0 end) hin_ca,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Penal' then 1 else 0 end) hpg_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Penal' then 1 else 0 end) hpn_ca,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Tiro libre directo' then 1 else 0 end) hldg_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Tiro libre directo' then 1 else 0 end) hldn_ca,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Tiro libre indirecto' then 1 else 0 end) hlig_ca, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Tiro libre indirecto' then 1 else 0 end) hlin_ca,

        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and zona='Long Shot' then 1 else 0 end) hlsg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and zona='Long Shot' then 1 else 0 end) hlsn,
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='Gol' and tipoocasion='Evento especial' then 1 else 0 end) hseg, 
        sum(case when b.id_equipo=a.id_equipo_home and subtipo='No Gol' and tipoocasion='Evento especial' then 1 else 0 end) hsen,

        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Derecha' then 1 else 0 end) adg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Derecha' then 1 else 0 end) adn, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Centro' then 1 else 0 end) amg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Centro' then 1 else 0 end) amn,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Izquierda' then 1 else 0 end) aig, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Izquierda' then 1 else 0 end) ain,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Penal' then 1 else 0 end) apg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Penal' then 1 else 0 end) apn,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Tiro libre directo' then 1 else 0 end) aldg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Tiro libre directo' then 1 else 0 end) aldn,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Normal' and zona='Tiro libre indirecto' then 1 else 0 end) alig, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Normal' and zona='Tiro libre indirecto' then 1 else 0 end) alin,
        
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Derecha' then 1 else 0 end) adg_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Derecha' then 1 else 0 end) adn_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Centro' then 1 else 0 end) amg_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Centro' then 1 else 0 end) amn_ca,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Izquierda' then 1 else 0 end) aig_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Izquierda' then 1 else 0 end) ain_ca,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Penal' then 1 else 0 end) apg_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Penal' then 1 else 0 end) apn_ca,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Tiro libre directo' then 1 else 0 end) aldg_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Tiro libre directo' then 1 else 0 end) aldn_ca,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Contra ataque' and zona='Tiro libre indirecto' then 1 else 0 end) alig_ca, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Contra ataque' and zona='Tiro libre indirecto' then 1 else 0 end) alin_ca,

        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and zona='Long Shot' then 1 else 0 end) alsg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and zona='Long Shot' then 1 else 0 end) alsn,
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='Gol' and tipoocasion='Evento especial' then 1 else 0 end) aseg, 
        sum(case when b.id_equipo=a.id_equipo_away and subtipo='No Gol' and tipoocasion='Evento especial' then 1 else 0 end) asen

        from partido a left join partido_evento b on a.id_partido=b.id_partido left join evento c on b.id_evento=c.id_evento where a.id_partido=$id_partido");
		$gol = pg_fetch_array($consultar);
        ?>
        <h3><?php echo $text["distribucion ataques"][$_SESSION[IDIOMA]];?></h3>
		<table border="0" cellpadding="0" cellspacing="0" class="tabla sortable">
        <tr><td/><th colspan="2"><?php echo $rs['hnombre'];?></th><th colspan="2"><?php echo $rs['anombre'];?></th></tr>
        <tr><th><?php echo $text["Zona de ataque"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Goles"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Ocasiones fallidas"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Goles"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Ocasiones fallidas"][$_SESSION[IDIOMA]];?></th></tr>
        <?php $v1=$gol['hdg']; $v2=$gol['hdn']; $v3=$gol['adg']; $v4=$gol['adn']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Derecha"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hmg']; $v2=$gol['hmn']; $v3=$gol['amg']; $v4=$gol['amn']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Centro"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hig']; $v2=$gol['hin']; $v3=$gol['aig']; $v4=$gol['ain']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Izquierda"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hpg']; $v2=$gol['hpn']; $v3=$gol['apg']; $v4=$gol['apn']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Penal"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hldg']; $v2=$gol['hldn']; $v3=$gol['aldg']; $v4=$gol['aldn']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Tiro libre directo"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hlig']; $v2=$gol['hlin']; $v3=$gol['alig']; $v4=$gol['alin']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Tiro libre indirecto"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hlsg']; $v2=$gol['hlsn']; $v3=$gol['alsg']; $v4=$gol['alsn']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Tiro lejano"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hdg_ca']; $v2=$gol['hdn_ca']; $v3=$gol['adg_ca']; $v4=$gol['adn_ca']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["CA Derecha"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hmg_ca']; $v2=$gol['hmn_ca']; $v3=$gol['amg_ca']; $v4=$gol['amn_ca']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["CA Centro"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hig_ca']; $v2=$gol['hin_ca']; $v3=$gol['aig_ca']; $v4=$gol['ain_ca']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["CA Izquierda"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hldg_ca']; $v2=$gol['hldn_ca']; $v3=$gol['aldg_ca']; $v4=$gol['aldn_ca']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["CA Tiro libre directo"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hlig_ca']; $v2=$gol['hlin_ca']; $v3=$gol['alig_ca']; $v4=$gol['alin_ca']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["CA Tiro libre indirecto"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        <?php $v1=$gol['hseg']; $v2=$gol['hsen']; $v3=$gol['aseg']; $v4=$gol['asen']; if ($v1+$v2+$v3+$v4>0){?>
        <tr class="modo1"><td><?php echo $text["Evento especial"][$_SESSION[IDIOMA]];?></td><td><?php echo $v1;?></td><td><?php echo $v2;?></td><td><?php echo $v3;?></td><td><?php echo $v4;?></td></tr>
        <?php }?>
        </table>
        <h3><?php echo $text["ditribucion teorica"][$_SESSION[IDIOMA]];?></h3>
        <?php 
        $h_aow=(($rs['ht']==4)?$rs['hnt']:0);
        $h_aim=(($rs['ht']==3)?$rs['hnt']:0);
        $h_tl=2*(($rs['ht']==8)?$rs['hnt']:0);
        $a_aow=(($rs['awt']==4)?$rs['ant']:0);
        $a_aim=(($rs['awt']==3)?$rs['ant']:0);
        $a_tl=2*(($rs['awt']==8)?$rs['ant']:0);
        
        $h_d=number_format(2*100/8+$h_aow/2-$h_aim/2-$h_tl*2/7,1);
        $h_c=number_format(3*100/8-$h_aow+$h_aim-$h_tl*3/7,1);
        $h_i=$h_d;
        $h_p=number_format(100/24,1);
        $h_tld=number_format(100/24,1);
        $h_tli=number_format(100/24,1);
        $h_tl=number_format($h_tl,1);
        $a_d=number_format(2*100/8+$a_aow/2-$a_aim/2-$a_tl*2/7,1);
        $a_c=number_format(3*100/8-$a_aow+$a_aim-$a_tl*3/7,1);
        $a_i=$a_d;
        $a_p=number_format(100/24,1);
        $a_tld=number_format(100/24,1);
        $a_tli=number_format(100/24,1);
        $a_tl=number_format($a_tl,1);
        $h_ed=0.80*ocasion(11, 5,$rs['had'] ,$rs['adi']);
        $h_ec=0.80*ocasion(11, 5,$rs['hac'] ,$rs['adc']);
        $h_ei=0.80*ocasion(11, 5,$rs['hai'] ,$rs['atdd']);
        $h_ein=0.90*ocasion(13, 5,$rs['hain'] ,$rs['adin']);
        $a_ed=0.80*ocasion(11, 5,$rs['aad'] ,$rs['hdi']);
        $a_ec=0.80*ocasion(11, 5,$rs['aac'] ,$rs['hdc']);
        $a_ei=0.80*ocasion(11, 5,$rs['aai'] ,$rs['hdd']);
        $a_ein=0.90*ocasion(13, 5,$rs['aain'] ,$rs['hdin']);
        $h_em=ocasion(13, 6,$rs['hm'] ,$rs['am']);
        $a_em=ocasion(13, 6,$rs['am'] ,$rs['hm']);
        ?>
        
        <table border="0" cellpadding="0" cellspacing="0" class="tabla sortable">
        <tr><td/><th colspan="3"><?php echo $rs['hnombre'];?></th><th colspan="3"><?php echo $rs['anombre'];?></th></tr>
        <tr><th><?php echo $text["Zona de ataque"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Ocasiones"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Efectividad"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Goles esperados"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Ocasiones"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Efectividad"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Goles esperados"][$_SESSION[IDIOMA]];?></th></tr>
        <tr class="modo1"><td><?php echo $text["Derecha"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_d.'%'; ?></td><td><?php echo number_format(100*$h_ed,1).'%'; ?></td><td><?php echo number_format($h_d*$h_ed*$h_em/10,2); ?></td><td><?php echo $a_d.'%'; ?></td><td><?php echo number_format(100*$a_ed,1).'%'; ?></td><td><?php echo number_format($a_d*$a_ed*$a_em/10,2); ?></td></tr>
        <tr class="modo1"><td><?php echo $text["Centro"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_c.'%'; ?></td><td><?php echo number_format(100*$h_ec,1).'%'; ?></td><td><?php echo number_format($h_c*$h_ec*$h_em/10,2); ?></td><td><?php echo $a_c.'%'; ?></td><td><?php echo number_format(100*$a_ec,1).'%'; ?></td><td><?php echo number_format($a_c*$a_ec*$a_em/10,2); ?></td></tr>
        <tr class="modo1"><td><?php echo $text["Izquierda"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_i.'%'; ?></td><td><?php echo number_format(100*$h_ei,1).'%'; ?></td><td><?php echo number_format($h_i*$h_ei*$h_em/10,2); ?></td><td><?php echo $a_i.'%'; ?></td><td><?php echo number_format(100*$a_ei,1).'%'; ?></td><td><?php echo number_format($a_i*$a_ei*$a_em/10,2); ?></td></tr>
        <tr class="modo1"><td><?php echo $text["Penal"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_p.'%'; ?></td><td>?</td><td>?</td><td><?php echo $a_p.'%'; ?></td><td>?</td><td>?</td></tr>
        <tr class="modo1"><td><?php echo $text["Tiro libre directo"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_tld.'%'; ?></td><td>?</td><td>?</td><td><?php echo $a_tld.'%'; ?></td><td>?</td><td>?</td></tr>
        <tr class="modo1"><td><?php echo $text["Tiro libre indirecto"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_tli.'%'; ?></td><td><?php echo number_format(100*$h_ein,1).'%'; ?></td><td><?php echo number_format($h_tli*$h_ein*$h_em/10,2); ?></td><td><?php echo $a_tli.'%'; ?></td><td><?php echo number_format(100*$a_ein,1).'%'; ?></td><td><?php echo number_format($a_tli*$a_ein*$a_em/10,2); ?></td></tr>
        <tr class="modo1"><td><?php echo $text["Tiro lejano"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_tl.'%'; ?></td><td>?</td><td>?</td><td><?php echo $a_tl.'%'; ?></td><td>?</td><td>?</td></tr>
        <tr class="modo1"><td><?php echo $text["Total conocido"][$_SESSION[IDIOMA]];?></td><td><?php echo $h_d+$h_c+$h_i+$h_tli.'%'; ?></td><td><?php echo number_format(100*($h_d*$h_ed+$h_c*$h_ec+$h_i*$h_ei+$h_tli*$h_ein)/($h_d+$h_c+$h_i+$h_tli),1).'%'; ?></td><td><?php echo number_format(($h_d*$h_ed+$h_c*$h_ec+$h_i*$h_ei+$h_tli*$h_ein)*$h_em/10,2); ?></td><td><?php echo $a_d+$a_c+$a_i+$a_tli.'%'; ?></td><td><?php echo number_format(100*($a_d*$a_ed+$a_c*$a_ec+$a_i*$a_ei+$a_tli*$a_ein)/($a_d+$a_c+$a_i+$a_tli),1).'%'; ?></td><td><?php echo number_format(($a_d*$a_ed+$a_c*$a_ec+$a_i*$a_ei+$a_tli*$a_ein)*$a_em/10,2); ?></td></tr>
		</table>
		<?php 
        $consultar = pg_query($con,"select * from partido_evento a left join evento b on a.id_evento=b.id_evento where  id_partido=$id_partido and tipo='Ocasion' order by minuto,index");
        ?>
		<h3><?php echo $text["Eventos partido"][$_SESSION[IDIOMA]];?></h3>
		<table border="0" cellpadding="0" cellspacing="0" class="tabla sortable">
		<tr><th><?php echo $text["Minuto"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Equipo"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Tipo"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Zona"][$_SESSION[IDIOMA]];?></th><th><?php echo $text["Resultado"][$_SESSION[IDIOMA]];?></th></tr>
		<?php while($event = pg_fetch_array($consultar)){?>
		<tr class="modo1"><td><?php echo $event['minuto'];?></td><td><?php if($event['id_equipo']==$rs['id_equipo_home']){echo $rs['hnombre'];}else{echo $rs['anombre'];}?></td><td><?php echo $event['tipoocasion'];?></td><td><?php echo $event['zona'];?></td><td><?php echo $event['subtipo'];?></td></tr>	
			
		<?php }?>
		
		</table>
		<?php }?>
		</div>
	</div>
	<?php include_once('../footer.php');?>
</body>
</html>