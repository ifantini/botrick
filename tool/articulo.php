<?php
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
}
require_once("../textos.php");
require_once("../head.php");
require_once("../conexion.php");
if($_GET['ID']){
	$_SESSION['ID_ARTICULO'] = is_numeric($_GET['ID'])?$_GET['ID']:0;
}
if($_POST['Enviar']){
	$comentario = str_replace(';', '',pg_escape_string($_POST['txt_comentario']));
	$id_articulo_com = $_SESSION['ID_ARTICULO'];
	$sentencia = pg_query($con,"insert into comentario_articulo values(DEFAULT,$id_articulo_com,$user,'".str_replace("'","''",$comentario)."')");
}
if($_GET['Eliminar']){
 if (isset ($_SESSION['USUARIO']) && $permisos == 4){
	$id = is_numeric($_GET['Eliminar'])?$_GET['Eliminar']:0;
	$eliminar = pg_query($con,"delete from comentario_articulo where id_comentario_articulo = '$id'");
 }
}
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																								
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<?php $select="herramientas"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />																																																																																																
			<div>	
                <img src="../images/botriteca.jpg">
				<?php include_once("menu_tool.php");?>
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <?php
		if($_SESSION['ID_ARTICULO']){
			$id_art = $_SESSION['ID_ARTICULO'];
			$buscar = pg_query($con,"select * from articulo where id_articulo = $id_art");
			if($tener = pg_fetch_array($buscar)){
		?>
 		<div class="list">
			<h3><?php echo $tener['nombre'];?></h3>
            <div class="articulo">
        	<p><?php echo $tener['contenido'];?></p>
            </div>
		</div>
        <?php
			}
			if (isset ($_SESSION['USUARIO']) ){
		?>
        <div class="list">
        <h3><?php echo $text["comentarios text2"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="articulo.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["Comentario"][$_SESSION[IDIOMA]];?>:</td>
                </tr>
                <tr>
                <td><textarea name="txt_comentario" cols="40" rows="10"></textarea></td>
                </tr>
                <tr>
                <td colspan="2" align="center"><input name="Enviar" type="submit" id="Enviar" value="Enviar"></td>
                </tr>
                </table>        
        </form>
        <?php
			}else{
		?>
        <div class="list">
        <h3><?php echo $text["comentarios text3"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["comentarios text4"][$_SESSION[IDIOMA]];?>.<br><br></p>
        <?php
			}
		?>
        <h3><?php echo $text["Comentarios"][$_SESSION[IDIOMA]];?></h3>
        <?php
		$id_articulo_com = $_SESSION['ID_ARTICULO'];
		$listar = pg_query($con,"select * from comentario_articulo where id_articulo = $id_articulo_com order by id_articulo desc");
		while($rs = pg_fetch_array($listar)){
			$persona = $rs['id_usuario'];
			$saber = pg_query($con,"select nombre from usuario where id_usuario = '$persona'");
			if($tener = pg_fetch_array($saber)){
		?>
        <p><strong><?php echo $tener['nombre'];?></strong> dijo:</p>
        <table width="700"><tr><td bgcolor="#CCCCCC" align="center"><p><em><?php echo $rs['comentario'];?></em><br><br></p></td><?php if($permisos == 4){?><td align="right"><a href="articulo.php?Eliminar=<?php echo $rs['id_comentario_articulo'];?>"><img src="../images/icons/delete_x16_h.ico"></a></td><?php }?></tr></table>
        <p><br><br></p>
        <?php
			}
		}
		?>
        </div>
        <?php
		}
		?>
	</div>
	<?php include_once('../footer.php');?>
</body>
</html>