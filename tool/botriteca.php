<?php
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
}
require_once("../textos.php");
require_once("../head.php");
require_once("../conexion.php");
if($_POST['Crear']){
	$nombre = str_replace(';', '',pg_escape_string($_POST['txt_nom_art']));
	$contenido = str_replace(';', '',pg_escape_string($_POST['editor']));
	$insertar = pg_query($con,"insert into articulo values(DEFAULT,657,'$nombre','$contenido')");
}
if($_GET['Eliminar']){
 if (isset ($_SESSION['USUARIO']) && $permisos == 4 || $user == 657){
	$id = is_numeric($_GET['Eliminar'])?$_GET['Eliminar']:0;
	$eliminar2 = pg_query($con,"delete from comentario_articulo where id_articulo = '$id'");
	$eliminar = pg_query($con,"delete from articulo where id_articulo = '$id'");
 }
}
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																								
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<?php $select="herramientas"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />																																																																																																
			<div>	
                <img src="../images/botriteca.jpg">
				<?php include_once("menu_tool.php");?>
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
 		<div class="list">
			<h3><?php echo $text["Botriteca"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["Botriteca_intro"][$_SESSION[IDIOMA]];?></p>
		</div>
        <div class="list">
			<h3><?php echo $text["Botriteca_articulos"][$_SESSION[IDIOMA]];?></h3>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
        <tr>
        <th><?php echo $text["Botriteca_tabla"][$_SESSION[IDIOMA]];?></th>
        <?php
		if (isset ($_SESSION['USUARIO']) ){
			if($permisos == 4 || $user == 657){
		?>
        <th><img src="../images/icons/delete_x16_h.ico"></th>
        <?php
			}
		}
		?>
        </tr>
		<?php
		$articulos = pg_query($con,"select * from articulo order by id_articulo asc");
		while($existen = pg_fetch_array($articulos)){
		?>
        <tr class="modo1">
        <th><a href="articulo.php?ID=<?php echo $existen['id_articulo'];?>">- <?php echo $existen['nombre'];?></a></th>
        <?php
		if (isset ($_SESSION['USUARIO']) ){
			if($permisos == 4 || $user == 657){
		?>
        <td><a href="botriteca.php?Eliminar=<?php echo $existen['id_articulo'];?>"><img src="../images/icons/delete_x16_h.ico"></a></td>
        <?php
			}
		}
		?>
        </tr>
        <?php
		}
		?>
        </table>
		</div>
	</div>
	<?php include_once('../footer.php');?>
</body>
</html>