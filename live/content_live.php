<div class="barra_marcador">
<div class="marcador_local">
<p id="nombre_local"></p>
</div>
<div class="marcador">
<p id="gol_local"></p>
</div>
<div class="marcador">
<p id="gol_visita"></p>
</div>
<div class="marcador_visita">
<p id="nombre_visita"></p>
</div>
</div>
<table id="formacion" class="lineup">
<tr>
<td></td><td></td><td id="home_rol_100"></td><td></td><td></td>
</tr>
<tr>
<td id="home_rol_101"></td><td id="home_rol_102"></td><td id="home_rol_103"></td><td id="home_rol_104"></td><td id="home_rol_105"></td>
</tr>
<tr>
<td id="home_rol_106"></td><td id="home_rol_107"></td><td id="home_rol_108"></td><td id="home_rol_109"></td><td id="home_rol_110"></td>
</tr>
<tr>
<td></td><td id="home_rol_111"></td><td id="home_rol_112"></td><td id="home_rol_113"></td><td></td>
</tr>
<tr>
<td></td><td id="away_rol_113"></td><td id="away_rol_112"></td><td id="away_rol_111"></td><td></td>
</tr>
<tr>
<td id="away_rol_110"></td><td id="away_rol_109"></td><td id="away_rol_108"></td><td id="away_rol_107"></td><td id="away_rol_106"></td>
</tr>
<tr>
<td id="away_rol_105"></td><td id="away_rol_104"></td><td id="away_rol_103"></td><td id="away_rol_102"></td><td id="away_rol_101"></td>
</tr>
<tr>
<td></td><td></td><td id="away_rol_100"></td><td></td><td></td>
</tr>


</table>
<div id="reportes">

</div>
	
<?php
try
{
	$HT = $_SESSION['HT'];
	// Get live object
	// PHT manage all requests for viewNew and viewAll action, as index for last events
	// if you want to force viewAll, set a parameter to true in getLive() function
	// if you want to get starting lineup, set a second parameter to true in getLive() function
	$live = $HT->getLive(true,true);
	// This checks if new events are available for all matches in your live
	if($live->getMatchNumber())
	{
		// Parse all matches and display new events
		for($m=1; $m<=$live->getMatchNumber(); $m++)
		{
			if ($live->getMatch($m)->getId()!=$_SESSION['matchID_live']){
				continue;
			}
			$match = $live->getMatch($m);
			echo "<script type='text/javascript'>document.getElementById('nombre_local').innerHTML='".$match->getHomeTeamName()."';</script>";
			echo "<script type='text/javascript'>document.getElementById('gol_local').innerHTML='".$match->getHomeGoals()."';</script>";
			echo "<script type='text/javascript'>document.getElementById('gol_visita').innerHTML='".$match->getAwayGoals()."';</script>";
			echo "<script type='text/javascript'>document.getElementById('nombre_visita').innerHTML='".$match->getAwayTeamName()."';</script>";
			date_default_timezone_set('Europe/Belgrade');
			$tiempo=$match->getDate('U')-date('U');
			if($tiempo<0){
				$local = $match->getStartingLineupHome();
				for($i=1; $i<=$local->getPlayersNumber(); $i++){
					$jugador=$HT->getPlayer($local->getPlayer($i)->getId());
					$nombre = htmlentities(substr($jugador->getFirstName(),0,1).'. '.substr($jugador->getLastName(),0,10),ENT_QUOTES, "UTF-8");
					$role = $local->getPlayer($i)->getRole();
					$orden = $local->getPlayer($i)->getIndividualOrder();
					if (is_numeric($orden)){
						echo "<script type=\"text/javascript\">carga($role,'$nombre','home',$orden);</script>";}
				}
				$visita = $match->getStartingLineupAway();
				for($i=1; $i<=$visita->getPlayersNumber(); $i++){
					$jugador=$HT->getPlayer($visita->getPlayer($i)->getId());
					$nombre = htmlentities(substr($jugador->getFirstName(),0,1).'. '.substr($jugador->getLastName(),0,10),ENT_QUOTES, "UTF-8");
					$role = $visita->getPlayer($i)->getRole();
					$orden = $visita->getPlayer($i)->getIndividualOrder();
					if (is_numeric($orden)){
						echo "<script type=\"text/javascript\">carga($role,'$nombre','away',$orden);</script>";
					}
				}
				$reportes="";
				for($i=1; $i<=$match->getEventNumber(); $i++)
				{
					$evento=$match->getEvent($i);
					$reporte=str_replace("'","&lsquo;",str_replace('<br />', '',$evento->getText()));
					$equipoevento=$evento->getSubjectTeamId();
					if ($equipoevento==$match->getHomeTeamId()){
						$reportes = "<div class=\"home\"><p>$reporte</p></div>".$reportes;
					}
					else if ($equipoevento==$match->getAwayTeamId()){
						$reportes = "<div class=\"away\"><p>$reporte</p></div>".$reportes;
					}
					else{
					$reportes = "<div id=\"div_reportes\"><p>$reporte</p></div>".$reportes;
					}
				}
					
			}	
			else{
				$reportes = "<div id=\"div_reportes\"><p>Faltan ".round($tiempo/60)." minutos para el comienzo del partido.</p></div>";
			}
			echo "<script type='text/javascript'>document.getElementById('reportes').innerHTML='".$reportes."';</script>";
		}
	}
	$_SESSION['HT']= $HT;
}
	catch(HTError $e){

}
?>
