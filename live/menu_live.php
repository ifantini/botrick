<?php 
require_once("../textos.php");
require_once("../PHT.php");
session_start();
?>
<div>
	<input type="text" id="matchID_text">
	<button id="agregar_partido"><?php echo $text["Agregar"][$_SESSION[IDIOMA]];?></button>
	<button id="borrar_todos"><?php echo $text["Borrar todos"][$_SESSION[IDIOMA]];?></button>
		<script type="text/javascript">
		$('#borrar_todos').click(function (){	
			$.get('/live/live_request.php?action=delete', function(data) {
			$('#menu_live').html(data);
			});
		});
		$('#agregar_partido').click(function (){
			$.get('/live/live_request.php?matchID='+$('#matchID_text').val(), function(data) {
			$('#menu_live').html(data);
			});
		});
	</script>
</div>
<?php 
if(isset($_SESSION['HT'])){
	$HT=$_SESSION['HT'];
	$live=$HT->getLive(true,true);
	if (isset($_SESSION[matchID_live])){
	?><script type="text/javascript">
		$.get('/live/live_request.php?id=<?php echo $_SESSION[matchID_live];?>', function(data) {
			$('#content_live').html(data);
		});
	</script>
	<?php
		
	}
	

if($live->getMatchNumber())
{	
	// Parse all matches and display new events
	for($m=1; $m<=$live->getMatchNumber(); $m++)
	{
		$match = $live->getMatch($m);
		$idm=$match->getId();
	?>
	<div class="lista_partido <?php if ($idm!=$_SESSION[matchID_live]) echo 'des';?>seleccionado" id="partido_numero_<?php echo $idm;?>">
		<p class="lista_nombre"><?php echo $match->getHomeTeamName();?></p>
		<p class="lista_resultado" id="local_numero_<?php echo $idm;?>"><?php echo $match->getHomeGoals();?></p>
		<p class="lista_nombre"><?php echo $match->getAwayTeamName();?></p>
		<p class="lista_resultado" id="visita_numero_<?php echo $idm;?>"><?php echo $match->getAwayGoals();?></p>
		<script type="text/javascript">
			$('#partido_numero_<?php echo $idm;?>').click(function (){
				$('.lista_partido').removeClass('seleccionado');
				$('.lista_partido').addClass('desseleccionado');
				$('#partido_numero_<?php echo $idm;?>').removeClass('desseleccionado');
				
				$.get('/live/live_request.php?id=<?php echo $idm;?>', function(data) {
					
					$('#content_live').html(data);	
					$('#partido_numero_<?php echo $idm;?>').addClass('seleccionado');
					
				});
				
			});
		</script>
	</div>
	<?php 
	}
}else{
	?>
	<script type="text/javascript">
		$.get('/live/live_request.php?id=0', function(data) {
			$('#content_live').html(data);
		});
	</script>
	<?php 	
}						
}
?>