<?php
require_once("../PHT.php");
session_start();
try
{	

	$HT = $_SESSION['HT'];
	
	$live = $HT->getLive(true);
	if($live->getMatchNumber())
	{
		// Parse all matches and display new events
		$resultado[marcador]=array();
		for($m=1; $m<=$live->getMatchNumber(); $m++){
			
			$match = $live->getMatch($m);
			$ultimo=$match->getLastEventIndexShown();
			$resultado[ultimo]=$ultimo;
			date_default_timezone_set('Europe/Belgrade');
			$idm=$match->getId();
			$datos[id]=$idm;
			$datos[local]=$match->getHomeGoals();
			$datos[visita]=$match->getAwayGoals();
			$resultado[marcador][]=$datos;
			$tiempo=$match->getDate('U')-date('U');
			if($tiempo<0){
				if ($idm==$_SESSION['matchID_live']){
					$resultado[gol_local]=$match->getHomeGoals();
					$resultado[gol_visita]=$match->getAwayGoals();
					$reportes="";
					for($i=1; $i<=$match->getEventNumber(); $i++)
					{
						if($i>$ultimo){
							$clase=" new";
						}
						else{
							$clase="";
						}
						
						$evento=$match->getEvent($i);
						$reporte=str_replace("'","&lsquo;",str_replace('<br />', '',$evento->getText()));
						$equipoevento=$evento->getSubjectTeamId();
						if ($equipoevento==$match->getHomeTeamId()){
							$reportes = "<div class=\"home$clase\"><p>$reporte</p></div>".$reportes;
						}
						else if ($equipoevento==$match->getAwayTeamId()){
							$reportes = "<div class=\"away$clase\"><p>$reporte</p></div>".$reportes;
						}
						else{
							$reportes = "<div class=\"$clase\"><p>$reporte</p></div>".$reportes;
						}
			
					}
				}
				
			}else{
				$reportes = "<div id=\"div_reportes\"><p>Faltan ".round($tiempo/60)." minutos para el comienzo del partido.</p></div>";
				$resultado[gol_local]=0;
				$resultado[gol_visita]=0;
				
			}
			$resultado[reportes]=$reportes;
		}
	}
	$_SESSION['HT']= $HT;
}
catch(HTError $e){
	echo $e->getError();
}

echo json_encode($resultado);
?>