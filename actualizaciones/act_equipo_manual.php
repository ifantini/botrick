<?php
require_once ("../conexion.php");
require_once ("../PHT.php");
require_once ("../tool/funciones_tool.php");
require_once ("funciones.php");
session_start ();
if (isset ( $_SESSION ['USUARIO'] )) {
	try {
		$HT = new CHPPConnection ( 'ES4aPZphU94aQ4VXRKO5lW', 'CE14MkbgXyDY84qPaPYXCTAiTd6VFOMDTEtjYOWjBMk' );
		$HT->setOauthToken ( $_SESSION ['T1'] );
		$HT->setOauthTokenSecret ( $_SESSION ['T2'] );
		$_SESSION [EQUIPO1] = $HT->getPrimaryTeam ()->getTeamId ();
		$team1 = $HT->getPrimaryTeam ();
		if (! is_null ( $HT->getSecondaryTeam () )) {
			$_SESSION [EQUIPO2] = $HT->getSecondaryTeam ()->getTeamId ();
			$team2 = $HT->getSecondaryTeam ();
		} else {
			$_SESSION [EQUIPO2] = null;
		}
		
		act_manager ( $HT, $con );
		act_equipo ( $team1, $HT, $con );
		act_estadio ( $HT, $team1->getTeamId (), $con );
		act_jugadores ( $HT, $team1->getTeamId (), $con );
		act_entreno ( $HT, $team1->getTeamId (), $con );
		act_staff ( $HT->getClub ( $team1->getTeamId () ), $team1->getTeamId (), $con );
		act_partidos ( $HT, $team1->getTeamId (), $con );
		
		if (isset ( $_SESSION [EQUIPO2] )) {
			act_equipo ( $team2, $HT, $con );
			act_estadio ( $HT, $team2->getTeamId (), $con );
			act_jugadores ( $HT, $team2->getTeamId (), $con );
			act_entreno ( $HT, $team2->getTeamId (), $con );
			act_staff ( $HT->getClub ( $team2->getTeamId () ), $team2->getTeamId (), $con );
			act_partidos ( $HT, $team2->getTeamId (), $con );
		}
	} catch ( HTError $e ) {
		// if ($_GET['lugar']=='home'){
		// header("Location:../home.php");
		// }else{
		// header('Location:miequipo.php');
		// }
		echo $e;
	}
	
	header ( 'Location:../club/miequipo.php' );
	
} else {
	// header ( 'Location:../error.php' );
	echo "error";
}
?>