<?php
// In callback url page:
echo date ( 'H:i:s' ) . "Comienza todo\n";
require_once ("../conexion.php");
require_once ("../PHT.php");
require_once ("../tool/funciones_tool.php");
require_once ("funciones.php");

$temp = is_numeric ( $_GET ['temp'] ) ? $_GET ['temp'] : 0;
$semana = is_numeric ( $_GET ['semana'] ) ? $_GET ['semana'] : 0;
$iduser = is_numeric ( $_GET ['id'] ) ? $_GET ['id'] : 0;

$query = pg_query ( $con, "select token,tokensecreto from usuario where id_usuario=$iduser" );
echo date ( 'H:i:s' ) . "Comienza con conexion<br/>";
while ( $rsteam = pg_fetch_array ( $query ) ) {
	try {
		$userToken = $rsteam ['token'];
		$userTokenSecret = $rsteam ['tokensecreto'];
		$HT = new CHPPConnection ( 'ES4aPZphU94aQ4VXRKO5lW', 'CE14MkbgXyDY84qPaPYXCTAiTd6VFOMDTEtjYOWjBMk' );
		$HT->setOauthToken ( $userToken );
		$HT->setOauthTokenSecret ( $userTokenSecret );
		
		$_SESSION [EQUIPO1] = $HT->getPrimaryTeam ()->getTeamId ();
		$team1 = $HT->getPrimaryTeam ();
		if (! is_null ( $HT->getSecondaryTeam () )) {
			$_SESSION [EQUIPO2] = $HT->getSecondaryTeam ()->getTeamId ();
			$team2 = $HT->getSecondaryTeam ();
		} else {
			$_SESSION [EQUIPO2] = null;
		}
		
		// Actualiza manager
		act_manager ( $HT, $con );
		act_equipo ( $team1, $HT, $con, TRUE, $temp, $semana );
		act_estadio ( $HT, $team1->getTeamId (), $con );
		act_jugadores ( $HT, $team1->getTeamId (), $con, TRUE, $temp, $semana );
		act_entreno ( $HT, $team1->getTeamId (), $con, TRUE, $temp, $semana );
		act_staff ( $HT->getClub ( $team1->getTeamId () ), $team1->getTeamId (), $con, TRUE, $temp, $semana );
		act_partidos ( $HT, $team1->getTeamId (), $con );
		
		if (isset ( $_SESSION [EQUIPO2] )) {
			act_equipo ( $team2, $HT, $con, TRUE, $temp, $semana );
			act_estadio ( $HT, $team2->getTeamId (), $con );
			act_jugadores ( $HT, $team2->getTeamId (), $con, TRUE, $temp, $semana );
			act_entreno ( $HT, $team2->getTeamId (), $con, TRUE, $temp, $semana );
			act_staff ( $HT->getClub ( $team2->getTeamId () ), $team2->getTeamId (), $con, TRUE, $temp, $semana );
			act_partidos ( $HT, $team2->getTeamId (), $con );
		}
		
		echo date ( 'H:i:s' ) . " finalizado con exito";
	} catch ( HTError $e ) {
		echo $e->getMessage ();
	}
}

?>

