<?php
function act_staff($club, $IDEquipo, $con, $hist=FALSE,$temp=0,$semana=0) {
	
	// Actualiza staff
	$consultar = pg_query ( $con, "select id_equipo from staff where id_equipo=$IDEquipo" );
	$rs = pg_fetch_array ( $consultar );
	$auxiliares = $club->getAssistantTrainerLevels ();
	$psicologos = $club->getSportPsychologistLevels ();
	$portavoces = $club->getSpokespersonLevels ();
	$fisios = $club->getFormCoachLevels ();
	$doctores = $club->getMedicLevels ();
	$financiero = $club->getFinancialDirectorLevels ();
	if ($rs == false) {
		$poner = "insert into staff values(default,$IDEquipo,$auxiliares,$psicologos,$portavoces,$fisios,$doctores,$financiero)";
		$poner2 = pg_query ( $con, $poner );
	} else {
		$delponer = "update staff set auxiliares=$auxiliares,psicologos=$psicologos,portavoces=$portavoces,fisioterapeutas=$fisios,doctores=$doctores,financiero=$financiero where id_equipo=$IDEquipo";
		$delponer2 = pg_query ( $con, $delponer );
	}
	
	if($hist){
		$poner = "insert into historia_staff values(default,$IDEquipo,$temp,$semana,$auxiliares,$psicologos,$portavoces,$fisios,$doctores)";
		$poner2 = pg_query($con,$poner);
	}
}
function act_manager($HT, $con) {
	// Actualiza manager
	$ID = $HT->getTeam ()->getUserId ();
	$fechaHT = $HT->getTeam ()->getSignupDate ();
	// Actualiza manager
	$consultar = pg_query ( $con, "select id_manager from manager where id_manager=$ID" );
	$rs = pg_fetch_array ( $consultar );
	if ($rs == false) {
		$ingresar = "insert into manager values('$ID','$fechaHT','$lastlogin')";
		$ejecutar = pg_query ( $con, $ingresar );
	}
	$preguntar = pg_query ( $con, "update usuario set id_manager=$ID where id_usuario = $user" );
	$result = pg_fetch_array ( $preguntar );
	$preguntar = pg_query ( $con, "update manager set lastlogin='$lastlogin' where id_manager = $ID" );
	$result = pg_fetch_array ( $preguntar );

}
function act_equipo($equipo, $HT, $con, $hist=FALSE,$temp=0,$semana=0) {
	// Actualiza equipo
	echo "e1";
	$IDEquipo = $equipo->getTeamId ();
	$ID = $equipo->getUserId ();
	$pais = $equipo->getLeagueId ();
	$nombre = str_replace ( "'", "''", $equipo->getTeamName () );
	$espiritu = $HT->getTraining ( $IDEquipo )->getTeamSpirit ();
	$confianza = $HT->getTraining ( $IDEquipo )->getSelfConfidence ();
	$plata = $HT->getEconomy(HTMoney::Chile,$IDEquipo)->getCash();
	$socios = $equipo->getFanClubSize ();

	echo $IDEquipo;
	$consultar = pg_query ( $con, "select granja from equipo where ID_Equipo=$IDEquipo" );
	$rs = pg_fetch_array ( $consultar );
	echo $rs ['granja'];
	if ($rs == false) {
		$insertar = "insert into equipo values($IDEquipo,$ID,'$nombre',$pais,$confianza,$espiritu,$plata,-1,$socios,TRUE)";
		$insertar2 = pg_query ( $con, $insertar );
		$granja = - 1;
	} else {
		echo $ID;
		echo $plata;
		$delequip = "update equipo set nombre='$nombre',confianza=$confianza,espiritu=$espiritu,dinero=$plata,id_manager=$ID,socios=$socios,activo=TRUE where ID_Equipo=$IDEquipo";
		echo $delequip;
		$delequip2 = pg_query ( $con, $delequip );
		$granja = $rs ['granja'];
	}
	
	if($hist){
		//Inserta a historia_equipo
		$almacenar = "insert into historia_equipo values(default,$IDEquipo,$ID,$temp,$semana,now(),'$nombre',$pais,$confianza,$espiritu,$plata,$granja,$socios)";
		$update2 = pg_query($con,$almacenar);
	}
	echo "e5";
}
function act_estadio($HT, $IDEquipo, $con) {
	$arena = $HT->getArenaDetailsByTeam ( $IDEquipo );
	$capacity = $arena->getCurrentCapacity ();
	$IDEstadio = $arena->getId ();
	$galeria = $capacity->getTerraces ();
	$platea = $capacity->getBasic ();
	$plateat = $capacity->getRoof ();
	$palcos = $capacity->getVip ();
	
	$consultar = pg_query ( $con, "select id_estadio from estadio where id_estadio=$IDEstadio" );
	$rs = pg_fetch_array ( $consultar );
	if ($rs == false) {
		$poner = "insert into estadio values($IDEstadio,$IDEquipo,$galeria,$platea,$plateat,$palcos)";
		$poner2 = pg_query ( $con, $poner );
	} else {
		$delponer = "update estadio set galeria=$galeria,platea=$platea,plateatechada=$plateat,palcos=$palcos,id_equipo=$IDEquipo where ID_Estadio=$IDEstadio";
		$delponer2 = pg_query ( $con, $delponer );
	}
}
function act_jugadores($HT, $IDEquipo, $con, $hist=FALSE,$temp=0,$semana=0) {
	$teamPlayers = $HT->getTeamPlayers ( $IDEquipo );
	
	$delponer = "update jugador set activo=FALSE where id_equipo=$IDEquipo";
	$delponer2 = pg_query ( $con, $delponer );
	for($i = 1; $i <= $teamPlayers->getNumberPlayers (); $i ++) {
		$player = $teamPlayers->getPlayer ( $i );
		$nom_jugador = str_replace ( "'", "''", $player->getName () );
		$dias = $player->getAge () * 112 + $player->getDays ();
		$fecha_nac = "now() - interval '$dias days'";
		$especialidad = $player->getSpeciality ();
		$IDJugador = $player->getId ();
		$pais = $player->getCountryId ();
		$tarjetas = $player->getCards ();
		$lealtad = $player->getLoyalty ();
		if ($player->hasMotherClubBonus ()) {
			$club_madre = "TRUE";
		} else {
			$club_madre = "FALSE";
		}
		$lesion = $player->getInjury ();
		if ($tarjetas == 'Not available') {
			// if ($_GET ['lugar'] == 'home') {
			// header ( 'Location:../home.php' );
			// } else {
			// header ( 'Location:miequipo.php' );
			// }
			echo "sin tarjetas";
		}
		$forma = $player->getForm ();
		$con_jugador = $player->getStamina ();
		$jugadas = $player->getPlaymaker ();
		$defensa = $player->getDefender ();
		$asistencias = $player->getPassing ();
		$lateral = $player->getWinger ();
		$anotacion = $player->getScorer ();
		$porteria = $player->getKeeper ();
		$bp = $player->getSetPieces ();
		$exp = $player->getExperience ();
		$lid = $player->getLeadership ();
		$tsi = $player->getTsi ();
		$sueldo = $player->getSalary ( HTMoney::Chile );
		$onsale = "FALSE";
		if ($player->isTransferListed ()) {
			$onsale = "TRUE";
		}
		;
		if ($player->isTrainer ()) {
			$nivdt = $player->getTrainerSkill ();
			$tipdt = $player->getTrainerType ();
		} else {
			$nivdt = "null";
			$tipdt = "null";
		}
		$consultar = pg_query ( $con, "select id_jugador from jugador where id_jugador=$IDJugador" );
		$rs = pg_fetch_array ( $consultar );
		if ($rs == false) {
			$guardar = "insert into jugador values($IDJugador,$IDEquipo,$fecha_nac,'$nom_jugador',$especialidad,$pais,$tarjetas,$lesion,$forma,$con_jugador,$jugadas,$defensa,$asistencias,$lateral,$anotacion,$porteria,$bp,$exp,$lid,$nivdt,$tipdt,$onsale,TRUE,$tsi,$sueldo,null,$lealtad,$club_madre)";
			$guardar2 = pg_query ( $con, $guardar );
		} else {
			$guardar = "update jugador set id_equipo=$IDEquipo,tarjetas=$tarjetas,lesion=$lesion,forma=$forma,condicion=$con_jugador,jugadas=$jugadas,defensa=$defensa,asistencias=$asistencias,lateral=$lateral,anotacion=$anotacion,porteria=$porteria,balonparado=$bp,experiencia=$exp,liderazgo=$lid,nivelentrenador=$nivdt,menatalidadentrenador=$tipdt,en_venta=$onsale,activo=TRUE,tsi=$tsi,sueldo=$sueldo,lealtad=$lealtad,club_madre=$club_madre where id_jugador=$IDJugador";
			$guardar2 = pg_query ( $con, $guardar );
		}
		
		if($hist){
			$guardatestado="insert into historia_jugador values(default,$IDJugador,$IDEquipo,$temp,$semana,now(),$tarjetas,$lesion,$forma,$con_jugador,$jugadas,$defensa,$asistencias,$lateral,$anotacion,$porteria,$bp,$exp,$lid,$nivdt,$tipdt,$sueldo,$tsi,$lealtad,$club_madre)";
			$guardatestado2 = pg_query($con,$guardatestado);
		}
	}
}
function act_entreno($HT, $IDEquipo, $con, $hist=FALSE,$temp=0,$semana=0) {
	$training = $HT->getTraining ( $IDEquipo );
	$IDDT = $training->getTrainerId ();
	$entreno = $training->getTrainingType ();
	$intensidad = $training->getTrainingLevel ();
	$condicion = $training->getStaminaTrainingPart ();
	
	$consultar = pg_query ( $con, "select id_equipo from entreno where id_equipo=$IDEquipo" );
	$rs = pg_fetch_array ( $consultar );
	if ($rs == false) {
		$poner = "insert into entreno values(default,$IDEquipo,$IDDT,$entreno,$intensidad,$condicion)";
		$poner2 = pg_query ( $con, $poner );
	} else {
		$delponer = "update entreno set id_entrenador=$IDDT,entreno=$entreno,intensidad=$intensidad,condicion=$condicion where id_equipo=$IDEquipo";
		$delponer2 = pg_query ( $con, $delponer );
	}
	
	if($hist){
		$poner = "insert into historia_entreno values(default,$IDEquipo,$IDDT,$temp,$semana,now(),$entreno,$intensidad,$condicion)";
		$poner2 = pg_query($con,$poner);
	}
}
function act_partidos($HT, $IDEquipo, $con) {
	$partidos = $HT->getSeniorTeamMatches ( $IDEquipo )->getLastMatches ();
	$contador=0;
	foreach ( $partidos as $i ) {
		$contador = $contador+1;
		if ($contador>30) continue;
		$id_partido = $i->getId ();
		$tipo = $i->getType();
		include ("act_partido.php");
	}
}
?>