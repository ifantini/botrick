<?php
$consultar = pg_query ( $con, "select count(*) count from partido where id_partido=$id_partido and tipo=$tipo" );
$rs = pg_fetch_array ( $consultar );
if (empty ( $HT )) {
	$HT = new CHPPConnection ( 'ES4aPZphU94aQ4VXRKO5lW', 'CE14MkbgXyDY84qPaPYXCTAiTd6VFOMDTEtjYOWjBMk' );
	$HT->setOauthToken ( 't8l9xQtvLlIN2xvB' );
	$HT->setOauthTokenSecret ( 'nYiCqfBGwKOUzCMd' );
}

// $id_partido=$_GET['id'];
if ($rs ['count'] == 0) {
// 	echo "entre con ".$id_partido.";".$tipo;
	if ($tipo < 20) {
		$Detail = $HT->getSeniorMatchDetails ( $id_partido );
	} else {
		$Detail = $HT->getTournamentMatchDetails ( $id_partido );
	}
	$Local = $Detail->getHomeTeam ();
	$Visita = $Detail->getAwayTeam ();
	
	if ($Local->getMidfieldRating () != '') {
		
		// inserta partido
		// $id_partido=$_GET['id'];
		$id_estadio = $Detail->getArena ()->getId ();
		$id_equipo_home = $Local->getId ();
		$id_equipo_away = $Visita->getId ();
		$fecha = $Detail->getStartDate ();
// 		$tipo = $Detail->getType ();
		$gol_local = $Local->getGoals ();
		$gol_visita = $Visita->getGoals ();
		$posesion_first = $Detail->getHomeTeamPossessionFirstHalf ();
		$posesion_second = $Detail->getHomeTeamPossessionSecondHalf ();
		$publico = $Detail->getArena ()->getSpectators ();
		$clima = $Detail->getArena ()->getWeatherId ();
		
		$q = "insert into partido values ($id_partido,$id_estadio,$id_equipo_home,$id_equipo_away,'$fecha',$tipo,$gol_local,$gol_visita,$posesion_first,$posesion_second,$publico,$clima)";
// 		echo $q.'<br/>';
		pg_query ( $con, $q );
		
		for($i = 1; $i < 3; $i ++) {
			if ($i == 1) {
				$equipo = $Local;
			} else {
				$equipo = $Visita;
			}
			
			// inserta calificaciones
			
			// $id_partido=$_GET['id'];
			$id_equipo = $equipo->getId ();
			$nombre = str_replace ( "'", "''", $equipo->getName () );
			$def_der = $equipo->getRightDefenseRating ();
			$def_cen = $equipo->getCentralDefenseRating ();
			$def_izq = $equipo->getLeftDefenseRating ();
			$def_ind = $equipo->getIndirectSetPiecesDefenseRating ();
			$medio = $equipo->getMidfieldRating ();
			$ata_der = $equipo->getRightAttackRating ();
			$ata_cen = $equipo->getCentralAttackRating ();
			$ata_izq = $equipo->getLeftAttackRating ();
			$ata_ind = $equipo->getIndirectSetPiecesAttackRating ();
			$tactica = $equipo->getTacticType ();
			$nivel_tactica = $equipo->getTacticSkill ();
			$actitud = $equipo->getAttitude () == "" ? "null" : $equipo->getAttitude ();
			
			$formacion = $equipo->getFormation ();
			$q = "insert into partido_calificacion values (default,$id_partido,$id_equipo,'$nombre',$def_der,$def_cen,$def_izq,$def_ind,$medio,$ata_der,$ata_cen,$ata_izq,$ata_ind,$tactica,$nivel_tactica,$actitud,'$formacion',$tipo)";
// 			echo $q.'<br/>';
			pg_query ( $con, $q );
			// inserta alineacion
			
			// $id_partido
			// $id_equipo
			$Lineup = $HT->getSeniorLineup ( $id_partido, $id_equipo );
			
			for($j = 1; $j <= $Lineup->getFinalLineup ()->getPlayersNumber (); $j ++) {
				$player = $Lineup->getFinalLineup ()->getPlayer ( $j );
				$posicion = $player->getRole ();
				$orden = $player->getIndividualOrder ();
				if ($posicion < 100) {
					$orden = 'null';
				}
				$total_star = $player->getRatingStars ();
				$total_star = ($total_star == 'Not Available') ? 'null' : 2 * $total_star;
				$yellow_star = $player->getRatingStarsAtEndOfMatch ();
				$yellow_star = ($yellow_star == 'Not Available') ? 'null' : 2 * $yellow_star;
				$id_jug = $player->getId ();
				$q = "insert into partido_alineacion values (default,$id_partido,$id_equipo,$posicion,$orden,$total_star,$yellow_star,$id_jug,$tipo)";
// 				echo $q.'<br/>';
				pg_query ( $con, $q );
			}
		}
		
		// inserta eventos
		for($k = 1; $k <= $Detail->getEventNumber (); $k ++) {
			$event = $Detail->getEvent ( $k );
			// $id_partido
			$index = $k;
			$id_equipo = $event->getSubjectTeamId ();
			$id_jugador_object = $event->getObjectPlayerId ();
			$id_jugador_subject = $event->getSubjectPlayerId ();
			$main_key = $event->getMainKey ();
			$key = $event->getKey ();
			$minuto = $event->getMinute ();
			$q = "insert into partido_evento values (default,$id_partido,$index,$id_equipo,$id_jugador_object,$id_jugador_subject,$main_key,'$key',$minuto,$tipo)";
			// echo $q.'<br/>';
			pg_query ( $con, $q );
		}
	}
}
?>
