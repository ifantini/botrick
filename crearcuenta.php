<?php
include("conexion.php"); 
include("PHT.php");
session_start();
$HT = $_SESSION[HT];
try
 {
 $HT->retrieveAccessToken($_REQUEST['oauth_token'], $_REQUEST['oauth_verifier']);
 $userToken = $HT->getOauthToken();
 $userTokenSecret = $HT->getOauthTokenSecret();
}
catch(HTError $e)
{
 echo $e->getMessage();
}
require_once("textos.php");
require_once("head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/title1.gif" alt="" width="209" height="30" /><br />
			<p>
            <?php echo $text["crearcuenta text1"][$_SESSION[IDIOMA]];?>.
            </p>
		  </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
</div>
        <div class="list">
        <form action="responder.php" method="post">
<table width="400">
  <tr>
    <td><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?>:</td>
    <td><label>
      <input type="text" name="txt_usuario" id="txt_usuario">
    </label></td>
  </tr>
  <tr>
    <td><?php echo $text["Contrasena"][$_SESSION[IDIOMA]];?>:</td>
    <td><label>
      <input type="password" name="txt_pass" id="txt_pass">
    </label></td>
  </tr>
  <tr>
    <td><?php echo $text["Repetir Contrasena:"][$_SESSION[IDIOMA]];?></td>
    <td><label>
      <input type="password" name="txt_repass" id="txt_repass">
    </label></td>
  </tr>
  <tr>
    <td colspan="2" align="center"><input name="Enviar" type="submit" value="<?php echo $text["Enviar"][$_SESSION[IDIOMA]];?>"></td>
  </tr>
</table>
        </form>
</div>
<?php include_once('footer.php');?>
</body>
</html>