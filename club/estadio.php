<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$val = FALSE;
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];	
	if($_GET[equipo]==2){
		$teamid=$_SESSION[EQUIPO2];
	}
	else{
		$teamid=$_SESSION[EQUIPO1];
	}
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		$manager = $rs['id_manager'];
		$tener = pg_query($con,"select id_equipo,pais,socios from equipo where id_manager = '$manager' and id_equipo=$teamid");
		if($recibir = pg_fetch_array($tener)){
			$socios = 25*$recibir['socios'];
			$equipo = $recibir['id_equipo'];
			$sel_estadio = "select * from estadio where id_equipo = '$equipo'";
			$eje_estadio = pg_query($con,$sel_estadio);
			if($rs_estadio = pg_fetch_array($eje_estadio)){
				$galeria = $rs_estadio['galeria'];
				$platea = $rs_estadio['platea'];
				$platea_t = $rs_estadio['plateatechada'];
				$vip = $rs_estadio['palcos'];
				$total = $galeria + $platea + $platea_t + $vip;	 
				}
			}
		}
	else{
		header('Location:../error.php');
	}
}
else{
	header('Location:../error.php');
}
if($_POST['Calcular']){
	$t = $_POST['total'];
    $g = $t * 0.59;
	$p = $t * 0.24;
	$pt = $t * 0.14;
	$v = $t * 0.03;
	$val = TRUE;
}
$titulo = "miequipo";
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="miequipo"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_club.php");?>   
	  	  </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Mi Estadio"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["estadio text1"][$_SESSION[IDIOMA]];?></p>
		<div class="main_part">		
			<div class="block">
				<div class="title"><?php echo $text["Estadio Actual"][$_SESSION[IDIOMA]];?></div>
                <h4><?php echo $text["Galeria"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Plateas"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Techadas"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Palcos VIP"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Total"][$_SESSION[IDIOMA]];?>:</h4>
			</div>
            <div class="block" align="right">
				<div class="title"><?php echo $text["Cantidades"][$_SESSION[IDIOMA]];?></div>
                <h4><?php echo number_format($galeria,0,",",".");?></h4>
                <h4><?php echo number_format($platea,0,",",".");?></h4>
                <h4><?php echo number_format($platea_t,0,",",".");?></h4>
                <h4><?php echo number_format($vip,0,",",".");?></h4>
                <h4><?php echo number_format($total,0,",",".");?></h4>
                
			</div>
            <div class="block"></div>
            <?php
				$gal = $total * 0.59;
				$pla = $total * 0.24;
				$plat = $total * 0.14;
				$pal = $total * 0.03;
				$tot = $gal + $pla + $plat + $pal;
            	$gal2 = $socios * 0.59;
				$pla2 = $socios * 0.24;
				$plat2 = $socios * 0.14;
				$pal2 = $socios * 0.03;
				$tot2 = $gal2 + $pla2 + $plat2 + $pal2;
				if($galeria < $gal){
					$gas_gal = (round($gal)-$galeria)*9000;
				}elseif($galeria > $gal){
					$gas_gal = ($galeria-round($gal))*1200;
				}else{
					$gas_gal = 0;
				}
				if($platea < $pla){
					$gas_pla = (round($pla)-$platea)*15000;
				}elseif($platea > $pla){
					$gas_pla = ($platea-round($pla))*1200;
				}else{
					$gas_pla = 0;
				}
				if($platea_t < $plat){
					$gas_plat = (round($plat)-$platea_t)*18000;
				}elseif($platea_t > $plat){
					$gas_plat = ($platea_t-round($plat))*1200;
				}else{
					$gas_plat = 0;
				}
				if($vip < $pal){
					$gas_vip = (round($pal)-$vip)*60000;				
				}elseif($vip > $pal){
					$gas_vip = ($vip-round($pal))*1200;
				}else{
					$gas_vip = 0;
				}
				//-----------------------------------
				if($galeria < $gal2){
					$gas_gal2 = (round($gal2)-$galeria)*9000;
				}elseif($galeria > $gal2){
					$gas_gal2 = ($galeria-round($gal2))*1200;
				}else{
					$gas_gal2 = 0;
				}
				if($platea < $pla2){
					$gas_pla2 = (round($pla2)-$platea)*15000;
				}elseif($platea > $pla2){
					$gas_pla2 = ($platea-round($pla2))*1200;
				}else{
					$gas_pla2 = 0;
				}
				if($platea_t < $plat2){
					$gas_plat2 = (round($plat2)-$platea_t)*18000;
				}elseif($platea_t > $plat2){
					$gas_plat2 = ($platea_t-round($plat2))*1200;
				}else{
					$gas_plat2 = 0;
				}
				if($vip < $pal2){
					$gas_vip2 = (round($pal2)-$vip)*60000;				
				}elseif($vip > $pal2){
					$gas_vip2 = ($vip-round($pal2))*1200;
				}else{
					$gas_vip2 = 0;
				}
				$gas_DI = $gas_gal + $gas_pla + $gas_plat + $gas_vip + 2000000;
				$gas_EI = $gas_gal2 + $gas_pla2 + $gas_plat2 + $gas_vip2 + 2000000;
			?>
            <div class="block" align="center">
            <div class="title"><?php echo $text["Distribucion Ideal"][$_SESSION[IDIOMA]];?></div>
            	<h4><?php echo number_format($gal,0,",",".");?></h4>
                <h4><?php echo number_format($pla,0,",",".");?></h4>
                <h4><?php echo number_format($plat,0,",",".");?></h4>
                <h4><?php echo number_format($pal,0,",",".");?></h4>
                <h4><?php echo number_format($tot,0,",",".");?></h4><br>
            </div>
            <div class="block" align="center">
            <div class="title"><?php echo $text["Estadio Ideal"][$_SESSION[IDIOMA]];?></div>
            	<h4><?php echo number_format($gal2,0,",",".");?></h4>
                <h4><?php echo number_format($pla2,0,",",".");?></h4>
                <h4><?php echo number_format($plat2,0,",",".");?></h4>
                <h4><?php echo number_format($pal2,0,",",".");?></h4>
                <h4><?php echo number_format($tot2,0,",",".");?></h4>                
                <br>
            </div>
		<br><br><p><?php echo $text["costos distribucion ideal"][$_SESSION[IDIOMA]];?>: <strong>$<?php echo number_format($gas_DI,0,",",".");?></strong></p>
        <p><?php echo $text["costos estadio ideal"][$_SESSION[IDIOMA]];?><strong>$<?php echo number_format($gas_EI,0,",",".");?></strong></p>
        </div>
        <h3><br><br><?php echo $text["Calcular Distribucion Ideal"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["estadio text2"][$_SESSION[IDIOMA]];?>
        	</p>
        <?php
		if($val == TRUE){
		?>
		<div class="main_part">
            <div class="block">
				<div class="title"><?php echo $text["Zonas"][$_SESSION[IDIOMA]];?></div>
                <h4><?php echo $text["Galeria"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Plateas"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Techadas"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Palcos VIP"][$_SESSION[IDIOMA]];?>:</h4>
                <h4><?php echo $text["Total"][$_SESSION[IDIOMA]];?>:</h4>
			</div>	
			<div class="block" align="right">
                <div class="title"><?php echo $text["Cantidades"][$_SESSION[IDIOMA]];?></div>
                    <h4><?php echo number_format($g,0,",",".");?></h4>
                    <h4><?php echo number_format($p,0,",",".");?></h4>
                    <h4><?php echo number_format($pt,0,",",".");?></h4>
                    <h4><?php echo number_format($v,0,",",".");?></h4>
                    <h4><?php echo number_format($t,0,",",".");?></h4><br>        
        	</div>
        </div>
        <?php
		}	
		?>      
        <div class="block">
        <form action="estadio.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["Capacidad Total a calcular"][$_SESSION[IDIOMA]];?>:</td><td><input name="total" type="text" id="total" /></td>
                </tr>
                <tr>
                <td colspan="2" align="center"><input name="Calcular" type="submit" id="Calcular" value="Calcular" /></td>
                </tr>
                </table>        
        </form>
        </div>
        <h3><em><?php echo $text["estadio text3"][$_SESSION[IDIOMA]];?></em></h3>       
		</p>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>
