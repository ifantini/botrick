<?php
function tipopartido($tipo){
	if ($tipo==2){
		return "matchQualification";
	}
	elseif ($tipo==3){
		return "matchCup";
	}
	elseif ($tipo==1){
		return "matchLeague";
	}
	elseif ($tipo==4 || $tipo==5 || $tipo==12){
		return "matchFriendly";
	}
	elseif ($tipo==7){
		return "matchMasters";
	}
	elseif ($tipo==61){
		return "matchSingleMatch";
	}
	elseif ($tipo==62){
		return "matchTournamentLadder";
	}
	elseif ($tipo==50 || $tipo==51){
		return "matchTournament";
	}
	else{
		return "matchFriendly";
	}	
}
?>