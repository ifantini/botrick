<?php
require_once("../conexion.php");
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];	
	if($_GET[equipo]==2){
		$teamid=$_SESSION[EQUIPO2];
	}
	else{
		$teamid=$_SESSION[EQUIPO1];
	}
	$tener = pg_query($con,"select a.id_equipo from usuario u left join equipo a on u.id_manager=a.id_manager where u.id_usuario = $user and a.id_equipo=$teamid");
	if($recibir = pg_fetch_array($tener)){
		$team = $recibir['id_equipo'];
	}
	else{
	header('Location:../error.php');
	}
}
else{
	header('Location:../error.php');
}
require_once("funciones_club.php");
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="miequipo"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_club.php");?>   
	  	  </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3<?php echo $text["Partidos jugados"][$_SESSION[IDIOMA]];?>></h3>
        <table class="tabla3">
        <?php
		  $sentencia = "select a.tipo,a.fecha,a.id_partido,b.nombre nombre_home,c.nombre nombre_away,gol_local,gol_visita,clima,publico from partido a left join partido_calificacion b on a.id_equipo_home=b.id_equipo and a.id_partido=b.id_partido left join partido_calificacion c on a.id_equipo_away=c.id_equipo and a.id_partido=c.id_partido where a.id_equipo_home = $team or a.id_equipo_away = $team order by a.fecha desc limit 100";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
		?>
		<tr class="modo1">
			<td><?php echo $rs[fecha];?></td>
			<td><img src="/images/flags/transparent.gif" class="<?php echo tipopartido($rs[tipo]); ?>"></td>
			<td><a href="/tool/analizapartido.php?id=<?php echo$rs[id_partido] ?>"><?php echo "$rs[nombre_home] - $rs[nombre_away]"; ?></a></td>
			<td><?php echo "$rs[gol_local] - $rs[gol_visita]"; ?></td>
			<td><img src="/images/icons/clima<?php echo $rs[clima]; ?>.png"></td>
			<td><?php echo $rs[publico]; ?></td>
		</tr>
		
		<?php }?>
		
		</table>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>