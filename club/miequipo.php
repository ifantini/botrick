<?php
require_once("../conexion.php");
require_once("../nombres.php");
require_once("../textos.php");
session_start();
if (empty($_SESSION['USUARIO'])){
	header('Location:../index.php');
}

if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	if($_GET[equipo]==2){
		$teamid=$_SESSION[EQUIPO2];
	}
	else{
		$teamid=$_SESSION[EQUIPO1];
	}
	$tener = pg_query($con,"select u.nombre username,a.id_manager,a.nombre teamnombre,a.id_equipo,a.pais,e.nombre nombreentre,b.intensidad,b.condicion conshare,c.nivelentrenador  from usuario u left join equipo a on u.id_manager=a.id_manager left join entreno b on a.id_equipo=b.id_equipo left join jugador c on b.id_entrenador=c.id_jugador left join nombres e on b.entreno=e.id_hattrick where e.tipo='entrenamiento' and e.idioma='$_SESSION[IDIOMA]' and u.id_usuario = $user and a.id_equipo=$teamid");
	if($recibir = pg_fetch_array($tener)){
		$username = $recibir['username'];
		$teamname = $recibir['teamnombre'];
		$manager = $recibir['id_manager'];
		$equipo = $recibir['id_equipo'];
		$country = $recibir['pais'];
		$condicion = $recibir['conshare'];
		$intensidad = $recibir['intensidad'];
		$nombreentre = $recibir['nombreentre'];
		$nivel = $recibir['nivelentrenador'];
		$sentencia = pg_query($con,"select * from equipo,usuario where id_usuario = $user and usuario.id_manager = equipo.id_manager and activo = TRUE and id_equipo=$teamid");
		if($rs_sen = pg_fetch_array($sentencia)){
			$creado = $rs_sen['fechacreacion'];
			$lev_con = $rs_sen['confianza'];
			$lev_esp = $rs_sen['espiritu'];
			$socios = $rs_sen['socios'];
			$est_granja = $rs_sen['granja'];
			if($est_granja == -1){$tipo_granja = $text["No soy granja"][$_SESSION[IDIOMA]];}elseif($est_granja == 0){$tipo_granja = $text["Cualquier entrenamiento"][$_SESSION[IDIOMA]];}else{$tipo_granja = denominacion($est_granja,$_SESSION[IDIOMA],'entrenamiento',$con);}
			$confianza=denominacion($lev_con,$_SESSION[IDIOMA],'confianza',$con);
			$espiritu=denominacion($lev_esp,$_SESSION[IDIOMA],'espiritu',$con);
		} 
	}
else{
	header('Location:../error.php');
	}
}
$titulo = "miequipo";
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																								
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<?php $select="miequipo"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />																																																																																																
			<div>	
				<?php include_once("menu_club.php");?>                              
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
 		<div class="list">
 			<a href="../actualizaciones/act_equipo_manual.php"><?php echo $text["Actualizar"][$_SESSION[IDIOMA]];?></a>
			<table class="tabla3">
            <tr>
            	<th colspan="4"><?php echo "Equipo de ".$username;?></th>
            </tr>
            <tr class="modo1">
            	<td class="flag"><img src="/images/flags/transparent.gif" style="background: transparent url(/images/flags/flags.gif) no-repeat -<?php echo 20*$country;?>px 0;"></img></td>
            	<td><?php echo $teamname;?>&nbsp;(<?php echo $teamid;?>)</td>
                <td><div class="Estilo1"><?php echo $text["Estado Granja"][$_SESSION[IDIOMA]];?>:</div></td>
                <td><div class="Estilo1"><?php echo $tipo_granja;?></div></td>
            </tr>
            <tr class="modo1">
            	<td><strong><?php echo $text["Socios"][$_SESSION[IDIOMA]];?>:</strong>&nbsp;</td>
                <td align="left"><?php echo number_format($socios,0,",",".");?></td>
                <td><strong><?php echo $text["Registrado el"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $creado;?></td>
            </tr>
            <tr class="modo1">
            	<td><strong><?php echo $text["Confianza"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $confianza;?></td>
                <td><strong><?php echo $text["Espiritu"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $espiritu;?></td>
            </tr>
            <tr class="modo1">
            	<td><strong><?php echo $text["Entrenamiento"][$_SESSION[IDIOMA]];?>:</strong>&nbsp;</td>
                <td align="left"><?php echo $nombreentre;?>&nbsp;(<?php echo $intensidad;?>%)</td>
                <td><strong><?php echo $text["Condicion"][$_SESSION[IDIOMA]];?>:</strong>&nbsp;</td>
                <td align="left"><?php echo $condicion;?>%</td>                
            </tr> 
            </table>        
        <h3><br><br><?php echo $text["Alerta de Comentarios no leidos"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["miequipo text1"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
        if (isset ($_SESSION['USUARIO']) ){
			  $hay = FALSE;
			  $listar = pg_query($con,"select jugador.nombre,jugador.id_jugador,COUNT(comentario_jugador.id_jugador) as veces from jugador,equipo,comentario_jugador where equipo.id_equipo = '$teamid' and equipo.id_equipo = jugador.id_equipo and jugador.id_jugador = comentario_jugador.id_jugador and comentario_jugador.leido = FALSE group by jugador.nombre,jugador.id_jugador");
			  while($rsteam = pg_fetch_array($listar)){			  
			  	$id_jug_comp = $rsteam['id_jugador'];
			  	$comprobar_usu = pg_query($con,"select usuario.id_usuario from jugador,equipo,manager,usuario where jugador.id_jugador = $id_jug_comp and jugador.id_equipo = equipo.id_equipo and equipo.id_manager = manager.id_manager and manager.id_manager = usuario.id_manager");				
				if($tener_comprobacion = pg_fetch_array($comprobar_usu)){$dueno_comprobacion = $tener_comprobacion['id_usuario'];}
				if($dueno_comprobacion != $user){
					$hay = TRUE;
        ?>
        <tr class="modo1">
        	<th><?php echo substr($rsteam['nombre'],0,23);?></th>
            <td><?php echo $text["miequipo text2"][$_SESSION[IDIOMA]].$rsteam['veces'].$text["miequipo text3"][$_SESSION[IDIOMA]];?></td>
        </tr> 
        <?php	
				}
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="2" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		}
		?>      
        </table>    
			<h3><br><?php echo $text["Jugadores"][$_SESSION[IDIOMA]];?></h3>
			<div class="block">
            <table border="0" cellpadding="0" cellspacing="0" class="tabla sortable">
              <tr>
                <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
              	<th><img src="/images/icons/onsale.png"></th> 
                <th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
                <th><?php echo $text["EE"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
				<th><img src="/images/icons/2card.png"></th>
				<th><img src="/images/icons/1injury.png"></th>
				<th><img src="/images/icons/motherclub_icon.png"></th>
				<th><?php echo $text["Le"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Fo"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Co"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ju"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["De"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pa"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["La"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["An"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Po"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["BP"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ex"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Li"][$_SESSION[IDIOMA]];?></th>
				<th>TTI</th>
				<th><?php echo $text["Pot"][$_SESSION[IDIOMA]];?></th>
              </tr>
              <?php
			  $listar = pg_query($con,"select * from jugadores_equipo($equipo) order by nombre");
			  $i = 1;
			  $existen = FALSE;
			  if($recibir){
			  while($rsteam = pg_fetch_array($listar)){
			  		$existen = TRUE;
					$resto = $i%2;
              ?>
              <tr class="modo1">
                <th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
                <td sorttable_customkey="<?php if ($rsteam['en_venta']=="t"){?>0"><img src="/images/icons/onsale.png"><?php }else{echo '1">';}?></td>
                <td sorttable_customkey="<?php echo $rsteam['dias'];?>"><?php echo floor($rsteam['dias']/112).'.'.($rsteam['dias']%112);?></td>
                <td sorttable_customkey="<?php echo $rsteam['especialidad'];?>"><?php if($rsteam['especialidad']>0){echo '<img src="/images/icons/spec'.$rsteam['especialidad'].'.png">';}?></td>
				<td sorttable_customkey="<?php echo $rsteam['pais'];?>"><img src="/images/flags/<?php echo $rsteam['pais'];?>flag.png"></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tarjetas'];?>"><?php if($rsteam['tarjetas']>0){echo '<img src="/images/icons/'.$rsteam['tarjetas'].'card.png">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lesion'];?>"><?php if($rsteam['lesion']>0){echo '<img src="/images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="/images/icons/0injury.png">';}?></td>
				<td sorttable_customkey="<?php if ($rsteam['club_madre']=="t"){?>0"><img src="/images/icons/motherclub_icon.png"><?php }else{echo '1">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lealtad']; ?>"><?php echo $rsteam['lealtad'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['forma']; ?>"><?php echo $rsteam['forma'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['condicion']; ?>"><?php echo $rsteam['condicion'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['jugadas']; ?>"><?php echo $rsteam['jugadas'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['defensa']; ?>"><?php echo $rsteam['defensa'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['asistencias']; ?>"><?php echo $rsteam['asistencias'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lateral']; ?>"><?php echo $rsteam['lateral'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['anotacion']; ?>"><?php echo $rsteam['anotacion'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['porteria']; ?>"><?php echo $rsteam['porteria'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['balonparado']; ?>"><?php echo $rsteam['balonparado'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['experiencia']; ?>"><?php echo $rsteam['experiencia'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['liderazgo']; ?>"><?php echo $rsteam['liderazgo'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tti']; ?>"><?php echo $rsteam['tti'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['potencial']; ?>"><?php echo number_format ( 100*$rsteam['potencial'] , 1 ,  '.' ,',' )."%";?></td>
              </tr>
            <?php 
			}
			}
			if($existen == FALSE){
			?>
            <tr>
                <td colspan="17" align="center"><?php echo $text["No hay registros para mostrar."][$_SESSION[IDIOMA]];?></td>
            </tr>
            <?php
			}
			?>
            </table>
            </div>
		</div>
	</div>
	<?php include_once('../footer.php');?>
</body>
</html>