<?php
// In callback url page:
echo date ( 'H:i:s' ) . "Comienza todo\n";
require_once ("conexion.php");
require_once ("PHT.php");

$temp = is_numeric ( $_GET ['temp'] ) ? $_GET ['temp'] : 0;
$semana = is_numeric ( $_GET ['semana'] ) ? $_GET ['semana'] : 0;
$iduser = is_numeric ( $_GET ['id'] ) ? $_GET ['id'] : 0;

$query = pg_query ( $con, "select token,tokensecreto from usuario where id_usuario=$iduser" );
echo date ( 'H:i:s' ) . "Comienza con conexion<br/>";
while ( $rsteam = pg_fetch_array ( $query ) ) {
	try {
		$userToken = $rsteam ['token'];
		$userTokenSecret = $rsteam ['tokensecreto'];
		$HT = new CHPPConnection ( 'ES4aPZphU94aQ4VXRKO5lW', 'CE14MkbgXyDY84qPaPYXCTAiTd6VFOMDTEtjYOWjBMk' );
		$HT->setOauthToken ( $userToken );
		$HT->setOauthTokenSecret ( $userTokenSecret );
		$ID = $HT->getTeam ()->getUserId ();
		$fechaHT = $HT->getTeam ()->getSignupDate ();
		$lastlogin = $HT->getTeam ()->getLastLoginDate ();
		// Actualiza manager
		$consultar = pg_query ( $con, "select id_manager from manager where id_manager=$ID" );
		$rs = pg_fetch_array ( $consultar );
		if ($rs == false) {
			$ingresar = "insert into manager values('$ID','$fechaHT','$lastlogin')";
			$ejecutar = pg_query ( $con, $ingresar );
		}
		$preguntar = pg_query ( $con, "update usuario set id_manager=$ID where id_usuario = $iduser" );
		$result = pg_fetch_array ( $preguntar );
		$preguntar = pg_query ( $con, "update manager set lastlogin='$lastlogin' where id_manager = $ID" );
		$result = pg_fetch_array ( $preguntar );
		
		$team = $HT->getTeam ();
		$training = $HT->getTraining ();
		$arena = $HT->getArenaDetails ();
		$capacity = $arena->getCurrentCapacity ();
		$teamPlayers = $HT->getTeamPlayers ();
		$economy = $HT->getEconomy ( HTMoney::Chile ); // hasta ac� van las declaraciones de variables cortas para PHT
		$IDEquipo = $team->getTeamId ();
		$pais = $team->getLeagueId ();
		$pais = $pais;
		$nombre = str_replace ( "'", "''", $team->getTeamName () );
		$confianza = $training->getSelfConfidence ();
		$espiritu = $training->getTeamSpirit ();
		$plata = $economy->getCash ();
		$IDEstadio = $arena->getId ();
		$galeria = $capacity->getTerraces ();
		$platea = $capacity->getBasic ();
		$plateat = $capacity->getRoof ();
		$palcos = $capacity->getVip (); // hasta ac� las variables para la tabla estadio
		$IDDT = $training->getTrainerId ();
		$entreno = $training->getTrainingType ();
		$socios = $HT->getFans ()->getMembers ();
		$intensidad = $training->getTrainingLevel ();
		$condicion = $training->getStaminaTrainingPart (); // hasta ac� las variables para la tabla entreno
		                                                  // Actualiza equipo
		echo date ( 'H:i:s' ) . "Comienza actualiza equipo<br/>";
		$consultar = pg_query ( $con, "select granja from equipo where ID_Equipo=$IDEquipo" );
		$rs = pg_fetch_array ( $consultar );
		if ($rs == false) {
			$insertar = "insert into equipo values($IDEquipo,$ID,'$nombre',$pais,$confianza,$espiritu,$plata,-1,$socios,TRUE)";
			$insertar2 = pg_query ( $con, $insertar );
			$granja = - 1;
		} else {
			$delequip = "update equipo set nombre='$nombre',confianza=$confianza,espiritu=$espiritu,dinero=$plata,id_manager=$ID,socios=$socios,activo=TRUE where ID_Equipo=$IDEquipo";
			$delequip2 = pg_query ( $con, $delequip );
			$granja = $rs ['granja'];
		}
		// Inserta a historia_equipo
		$almacenar = "insert into historia_equipo values(default,$IDEquipo,$ID,$temp,$semana,now(),'$nombre',$pais,$confianza,$espiritu,$plata,$granja,$socios)";
		$update2 = pg_query ( $con, $almacenar );
		// Actualiza estadio
		$consultar = pg_query ( $con, "select id_estadio from estadio where id_estadio=$IDEstadio" );
		$rs = pg_fetch_array ( $consultar );
		if ($rs == false) {
			$poner = "insert into estadio values($IDEstadio,$IDEquipo,$galeria,$platea,$plateat,$palcos)";
			$poner2 = pg_query ( $con, $poner );
		} else {
			$delponer = "update estadio set galeria=$galeria,platea=$platea,plateatechada=$plateat,palcos=$palcos,id_equipo=$IDEquipo where ID_Estadio=$IDEstadio";
			$delponer2 = pg_query ( $con, $delponer );
		}
		// Actualiza jugadores
		$delponer = "update jugador set activo=FALSE where id_equipo=$IDEquipo";
		$delponer2 = pg_query ( $con, $delponer );
		for($i = 1; $i <= $teamPlayers->getNumberPlayers (); $i ++) {
			$player = $teamPlayers->getPlayer ( $i );
			$nom_jugador = str_replace ( "'", "''", $player->getName () );
			$dias = $player->getAge () * 112 + $player->getDays ();
			$fecha_nac = "now() - interval '$dias days'";
			$especialidad = $player->getSpeciality ();
			$IDJugador = $player->getId ();
			$pais = $player->getCountryId ();
			$tarjetas = $player->getCards ();
			$lesion = $player->getInjury ();
			$forma = $player->getForm ();
			$con_jugador = $player->getStamina ();
			$jugadas = $player->getPlaymaker ();
			$defensa = $player->getDefender ();
			$asistencias = $player->getPassing ();
			$lateral = $player->getWinger ();
			$anotacion = $player->getScorer ();
			$lealtad = $player->getLoyalty ();
			if ($player->hasMotherClubBonus ()) {
				$club_madre = "TRUE";
			} else {
				$club_madre = "FALSE";
			}
			$porteria = $player->getKeeper ();
			$tsi = $player->getTsi ();
			$sueldo = $player->getSalary ( HTMoney::Chile );
			$bp = $player->getSetPieces ();
			$exp = $player->getExperience ();
			$lid = $player->getLeadership ();
			$onsale = "FALSE";
			if ($player->isTransferListed ()) {
				$onsale = "TRUE";
			}
			;
			if ($player->isTrainer ()) {
				$nivdt = $player->getTrainerSkill ();
				$tipdt = $player->getTrainerType ();
			} else {
				$nivdt = "null";
				$tipdt = "null";
			}
			$consultar = pg_query ( $con, "select id_jugador from jugador where id_jugador=$IDJugador" );
			$rs = pg_fetch_array ( $consultar );
			if ($rs == false) {
				$guardar = "insert into jugador values($IDJugador,$IDEquipo,$fecha_nac,'$nom_jugador',$especialidad,$pais,$tarjetas,$lesion,$forma,$con_jugador,$jugadas,$defensa,$asistencias,$lateral,$anotacion,$porteria,$bp,$exp,$lid,$nivdt,$tipdt,$onsale,TRUE,$sueldo,$tsi,null,$lealtad,$club_madre)";
				$guardar2 = pg_query ( $con, $guardar );
			} else {
				$guardar = "update jugador set id_equipo=$IDEquipo,tarjetas=$tarjetas,lesion=$lesion,forma=$forma,condicion=$con_jugador,jugadas=$jugadas,defensa=$defensa,asistencias=$asistencias,lateral=$lateral,anotacion=$anotacion,porteria=$porteria,balonparado=$bp,experiencia=$exp,liderazgo=$lid,nivelentrenador=$nivdt,menatalidadentrenador=$tipdt,en_venta=$onsale,activo=TRUE,tsi=$tsi,sueldo=$sueldo,lealtad=$lealtad,club_madre=$club_madre where id_jugador=$IDJugador";
				$guardar2 = pg_query ( $con, $guardar );
			}
			
			$guardatestado = "insert into historia_jugador values(default,$IDJugador,$IDEquipo,$temp,$semana,now(),$tarjetas,$lesion,$forma,$con_jugador,$jugadas,$defensa,$asistencias,$lateral,$anotacion,$porteria,$bp,$exp,$lid,$nivdt,$tipdt,$sueldo,$tsi,$lealtad,$club_madre)";
			$guardatestado2 = pg_query ( $con, $guardatestado );
		}
		// Actualiza entreno
		$consultar = pg_query ( $con, "select id_equipo from entreno where id_equipo=$IDEquipo" );
		$rs = pg_fetch_array ( $consultar );
		if ($rs == false) {
			$poner = "insert into entreno values(default,$IDEquipo,$IDDT,$entreno,$intensidad,$condicion)";
			$poner2 = pg_query ( $con, $poner );
		} else {
			$delponer = "update entreno set id_entrenador=$IDDT,entreno=$entreno,intensidad=$intensidad,condicion=$condicion where id_equipo=$IDEquipo";
			$delponer2 = pg_query ( $con, $delponer );
		}
		
		$poner = "insert into historia_entreno values(default,$IDEquipo,$IDDT,$temp,$semana,now(),$entreno,$intensidad,$condicion)";
		$poner2 = pg_query ( $con, $poner );
		
		// Actualiza staff
		$consultar = pg_query ( $con, "select id_equipo from staff where id_equipo=$IDEquipo" );
		$rs = pg_fetch_array ( $consultar );
		$club = $HT->getClub ();
		$auxiliares = $club->getAssistantTrainerLevels ();
		$psicologos = $club->getSportPsychologistLevels ();
		$portavoces = $club->getSpokespersonLevels ();
		$fisios = $club->getFormCoachLevels ();
		$doctores = $club->getMedicLevels ();
		$financiero = $club->getFinancialDirectorLevels ();
		if ($rs == false) {
			$poner = "insert into staff values(default,$IDEquipo,$auxiliares,$psicologos,$portavoces,$fisios,$doctores,$financiero)";
			$poner2 = pg_query ( $con, $poner );
		} else {
			$delponer = "update staff set auxiliares=$auxiliares,psicologos=$psicologos,portavoces=$portavoces,fisioterapeutas=$fisios,doctores=$doctores,financiero=$financiero where id_equipo=$IDEquipo";
			$delponer2 = pg_query ( $con, $delponer );
		}
		
		$poner = "insert into historia_staff values(default,$IDEquipo,$temp,$semana,$auxiliares,$psicologos,$portavoces,$fisios,$doctores,$financiero)";
		$poner2 = pg_query ( $con, $poner );
		
		// Actualiza Partidos
		$partidos = $HT->getSeniorTeamMatches ()->getLastMatches ();
		// echo $partidos[4]->getId();
		foreach ( $partidos as $i ) {
			$id_partido = $i->getId ();
			include ("tool/act_partido.php");
		}
		
		echo date ( 'H:i:s' ) . " finalizado con exito";
	} catch ( HTError $e ) {
		echo $e->getMessage ();
	}
}

?>

