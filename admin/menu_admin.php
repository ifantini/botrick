<?php
require_once("../conexion.php");
?>
<img src="../images/titleadm.gif"  class="menuimg"  alt="" width="209" height="30" />
	<ul id="leftnav">
		<li><h3><?php echo $text["Admin"][$_SESSION[IDIOMA]];?></h3>
			<ul>
            	<li><a href="/admin/administracion.php">Vista General</a></li>
				<li><a href="/admin/usuarios.php">Usuarios</a></li>
				<li><a href="/admin/permisos.php">Permisos</a></li>
			</ul>
		</li>
        <?php $can_usuarios = pg_query($con,"select count(id_usuario) from usuario");
		if($rs_usu = pg_fetch_array($can_usuarios)){
			$conteo = $rs_usu[0];
		}?>
		<li><h3>Conteo de usuarios</h3>
			<ul>
				<li><?php echo $conteo;?> al <?php echo $fecha_actual = date("d/m/Y");?></li>
			</ul>
		</li>
	</ul>
