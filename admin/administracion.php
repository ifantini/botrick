<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
			if($permisos < 1){
				header('Location:../error.php');
			}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
	<?php $select="administracion"; include("../mainmenu.php");?>
	<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_admin.php");?>
	  	  </div>
			<img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <?php $can_usuarios = pg_query($con,"select count(id_usuario) from usuario");
		if($rs_usu = pg_fetch_array($can_usuarios)){
			$conteo = $rs_usu[0];
		}?>
        <h3><?php echo $text["administracion text2"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["administracion text3"][$_SESSION[IDIOMA]];?>: <strong><?php echo $conteo;?></strong><?php echo $text["administracion text4"][$_SESSION[IDIOMA]]; echo $fecha_actual = date("d/m/Y");?></p>
        <h3><br><br><?php echo $text["administracion text7"][$_SESSION[IDIOMA]];?></h3>
        <p>
        	<?php echo $text["administracion text5"][$_SESSION[IDIOMA]];?>.
        </p>
        <?php
		if($permisos == 4){
		?>
        <h3><br><br><?php echo $text["administracion text6"][$_SESSION[IDIOMA]];?></h3>
        <p align="center"><a target="_blank" href="https://accounts.google.com/ServiceLogin?service=analytics&passive=true&nui=1&hl=es&continue=https://www.google.com/analytics/settings/&followup=https://www.google.com/analytics/settings/"><img src="../images/google-analytics.png"></a></p>
        <?php
		}
		?>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>
