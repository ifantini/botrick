<?php
require_once("../conexion.php"); 
session_start();
$RegistrosAMostrar=50;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = $user");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos != 4){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_admin.php");?>
	  	  </div>
			<img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["usuarios text1"][$_SESSION[IDIOMA]];?></h3>
        <p>
        	<?php echo $text["usuarios text2"][$_SESSION[IDIOMA]];?>.</br></br>
        </p>
        <h3><?php echo $text["usuarios text3"][$_SESSION[IDIOMA]];?></h3>
        <div class="list">
        <form action="usuarios.php" method="post">
        <table cellspacing="5" width="300">
          <tr>
            <td><?php echo $text["usuarios text4"][$_SESSION[IDIOMA]];?>:</td>
            <td><input name="txt_usu" type="text" id="txt_usu"></td>
          </tr>
          <tr>
            <td align="center" colspan="2"><input name="Consultar" type="submit" id="Consultar" value="Consultar"></td>
          </tr>
        </table>
        </form>
        </div>
        <p>
        <?php
		if($_POST['Consultar']){
			$usuario = $_POST['txt_usu'];
			$consultar = pg_query($con,"select usuario.id_usuario,usuario.nombre,usuario.clave,perfil.permisos,perfil.nombre nombreperf,usuario.id_manager from usuario,perfil where usuario.id_perfil = perfil.id_perfil and usuario.id_manager=".$usuario);
		?>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Clave"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Perfil"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Permiso"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Modificar"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['id_manager'];?></th>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo $rs['clave'];?></td>
            <td><?php echo $rs['nombreperf'];?></td>
            <td><?php echo $rs['permisos'];?></td>
            <td><a href="/admin/mod_usuarios.php?Id=<?php echo $rs['id_usuario'];?>"><img src="../images/icons/refresh16.ico"></a></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        <?php
		}
		?>
        <h3><?php echo $text["usuarios text5"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <div class="block">
        <table class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Clave"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Perfil"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Permiso"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Modificar"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia = "select usuario.id_usuario,usuario.nombre,usuario.clave,perfil.permisos,perfil.nombre nombreperf,usuario.id_manager  from usuario,perfil where usuario.id_perfil = perfil.id_perfil order by permisos desc offset $RegistrosAEmpezar limit $RegistrosAMostrar";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['id_manager'];?></th>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo $rs['clave'];?></td>
            <td><?php echo $rs['nombreperf'];?></td>
            <td><?php echo $rs['permisos'];?></td>
            <td><a href="/admin/mod_usuarios.php?Id=<?php echo $rs['id_usuario'];?>"><img src="../images/icons/refresh16.ico"></a></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        <table><tr>
        <?php
$NroRegistros=pg_num_rows(pg_query($con,"select * from usuario,perfil where usuario.id_perfil = perfil.id_perfil order by permisos desc"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;

 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='/admin/usuarios.php?pag=1'><img src='../images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='/admin/usuarios.php?pag=$PagAnt'><img src='../images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='/admin/usuarios.php?pag=$PagSig'><img src='../images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='/admin/usuarios.php?pag=$PagUlt'><img src='../images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
  ?>
  		</tr></table>
        </div>        
        </div>
        </div>
	</div>
        <?php include_once('../footer.php');?>
    </body>
</html>
