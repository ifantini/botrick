<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos != 4){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Perfil']){
	$id_per = is_numeric($_GET['Perfil'])?$_GET['Perfil']:0;
	$preguntar = pg_query($con,"select * from perfil where id_perfil = '$id_per'");
	while($rs_per = pg_fetch_array($preguntar)){
		$nom = $rs_per['nombre'];
	}
}
if($_POST['Modificar']){
	$id = is_numeric($_POST['hide'])?$_POST['hide']:0;
	$nom = str_replace(';', '',pg_escape_string($_POST['txt_nom_per']));
	$pais = is_numeric($_POST['cbo_pais'])?$_POST['cbo_pais']:0;
	$per = is_numeric($_POST['cbo_per'])?$_POST['cbo_per']:0;
	$sentencia = "update perfil set nombre = '$nom', pais = '$pais', permisos = '$per' where id_perfil = '$id'";
	$ejecutar = pg_query($con,$sentencia);
	header('Location:permisos.php');
}
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_admin.php");?>
	  	  </div>
			<img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["mod_permisos text2"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="mod_permisos.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["mod_permisos text3"][$_SESSION[IDIOMA]];?>:</td><td><input name="txt_nom_per" type="text" size="15" value="<?php echo $nom;?>"></td>
                </tr>
                <tr>
                <td><?php echo $text["mod_permisos text4"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_pais" size="1" id="cbo_pais">
                  <option value="0" selected><?php echo $text["General"][$_SESSION[IDIOMA]];?></option>
                  <option value="17">Chile</option>
                </select></td>
                </tr>
                <tr>
                <td><?php echo $text["Permisos"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_per" size="1" id="cbo_per">
                  <option value="0" selected><?php echo $text["General"][$_SESSION[IDIOMA]];?></option>
                  <option value="1"><?php echo $text["Scout"][$_SESSION[IDIOMA]];?></option>
                  <option value="2"><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></option>
                  <option value="3"><?php echo $text["DT U20"][$_SESSION[IDIOMA]];?></option>
                  <option value="3"><?php echo $text["DT NT"][$_SESSION[IDIOMA]];?></option>
                </select></td>
                </tr>                
                <tr>
                <td colspan="2" align="center"><input name="Modificar" type="submit" id="Modificar" value="<?php echo $text["Modificar"][$_SESSION[IDIOMA]];?>"><input name="hide" type="hidden" value="<?php echo $id_per;?>"></td>
                </tr>
                </table>        
        </form>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>
