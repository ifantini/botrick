<?php
require_once("../conexion.php"); 
session_start();
$RegistrosAMostrar=30;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos != 4){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Eliminar']){
	$id_per = is_numeric($_GET['Eliminar']);
	$eliminar = pg_query($con,"delete from perfil where id_perfil = '$id_per'");
	header('Location:/admin/permisos.php');
}
if($_POST['Crear']){
	$nom = str_replace(';', '',pg_escape_string($_POST['txt_nom_per']));
	$pais = is_numeric($_POST['cbo_pais'])?$_POST['cbo_pais']:0;
	$per = is_numeric($_POST['cbo_per'])?$_POST['cbo_per']:0;
	$sentencia = "insert into perfil values(DEFAULT,'$nom','$pais','$per')";
	$ejecutar = pg_query($con,$sentencia);
}
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<?php include_once("menu_admin.php");?>
	  	  </div>
			<img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["permisos text1"][$_SESSION[IDIOMA]];?></h3>
        <p>
        	<?php echo $text["permisos text2"][$_SESSION[IDIOMA]];?>.
        </p>
        <h3></br></br><?php echo $text["permisos text3"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="permisos.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["permisos text4"][$_SESSION[IDIOMA]];?>:</td><td><input name="txt_nom_per" type="text" size="15"></td>
                </tr>
                <tr>
                <td><?php echo $text["permisos text5"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_pais" size="1" id="cbo_pais">
                  <option value="0" selected><?php echo $text["General"][$_SESSION[IDIOMA]];?></option>
                  <option value="17">Chile</option>
                </select></td>
                </tr>
                <tr>
                <td><?php echo $text["Permisos"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_per" size="1" id="cbo_per">
                  <option value="0" selected><?php echo $text["General"][$_SESSION[IDIOMA]];?></option>
                  <option value="1"><?php echo $text["Scout"][$_SESSION[IDIOMA]];?></option>
                  <option value="2"><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></option>
                  <option value="3"><?php echo $text["permisos text6"][$_SESSION[IDIOMA]];?></option>
                  <option value="3"><?php echo $text["permisos text7"][$_SESSION[IDIOMA]];?></option>
                </select></td>
                </tr>
                <tr>
                <td colspan="2" align="center"><input name="Crear" type="submit" id="Crear" value="Crear"></td>
                </tr>
                </table>        
        </form>
        <h3></br></br><?php echo $text["permisos text8"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Permisos"][$_SESSION[IDIOMA]];?></th>            
            <th><?php echo $text["Modificar"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Eliminar"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia = "select * from perfil order by permisos desc offset $RegistrosAEmpezar limit $RegistrosAMostrar";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <td><?php echo $rs['id_perfil'];?></td>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo $rs['pais'];?></td>
            <td><?php echo $rs['permisos'];?></td>            
            <td><a href="/admin/mod_permisos.php?Perfil=<?php echo $rs['id_perfil'];?>"><img src="../images/icons/refresh16.ico"></a></td>
            <td><a href="/admin/permisos.php?Eliminar=<?php echo $rs['id_perfil'];?>"><img src="../images/icons/delete_x16_h.ico"></a></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        <table><tr>
        <?php
$NroRegistros=pg_num_rows(pg_query($con,"select * from perfil order by permisos desc"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;

 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='/admin/permisos.php?pag=1'><img src='../images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='/admin/permisos.php?pag=$PagAnt'><img src='../images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='/admin/permisos.php?pag=$PagSig'><img src='../images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='/admin/permisos.php?pag=$PagUlt'><img src='../images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
  ?>
  		</tr></table>
        </div>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>