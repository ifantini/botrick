<?php
require_once("../conexion.php"); 
session_start();
$RegistrosAMostrar=10;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos != 4){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Id']){
	$id_usu = is_numeric($_GET['Id'])?$_GET['Id']:0;
	$preguntar = pg_query($con,"select * from usuario where id_usuario = '$id_usu'");
	while($rs_usu = pg_fetch_array($preguntar)){
		$nom = $rs_usu['nombre'];
		$per = $rs_usu['id_perfil'];
	}
}
if($_POST['Modificar']){
	$id = is_numeric($_POST['hide'])?$_POST['hide']:0;
	$nom = str_replace(';', '',pg_escape_string($_POST['txt_nom_per']));
	$per =  is_numeric($_POST['cbo_per'])?$_POST['cbo_per']:0;
	$sentencia = "update usuario set id_perfil = $per where id_usuario = $id";
	$ejecutar = pg_query($con,$sentencia);
	header('Location:/admin/usuarios.php');
}
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/index.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/titleadm.gif" alt="" width="209" height="30" />
				<p><strong><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?>: <?php echo $nombre;?></strong>.
                <br><br><?php echo $text["mod_usuarios text1"][$_SESSION[IDIOMA]];?>.
                <br><br><!--agregar acá más links -->
				</p>
	  	  </div>
			<img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["mod_usuarios text2"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="mod_usuarios.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["mod_usuarios text3"][$_SESSION[IDIOMA]];?>:</td><td><input name="txt_nom_per" type="text" size="15" value="<?php echo $nom;?>" readonly="readonly"></td>
                </tr>
                <tr>
                <td><?php echo $text["mod_usuarios text4"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_per" size="1" id="cbo_per">
                  <?php
	  			  $listar = "select * from perfil order by id_perfil";
				  $ejecutar = pg_query($con,$listar);
				  while($rs = pg_fetch_array($ejecutar)){
				  echo "<option value='$rs[id_perfil]'>$rs[nombre]</option>";
				  }
				  ?>
                </select></td>
                </tr>
                <tr>
                <td colspan="2" align="center"><input name="Modificar" type="submit" id="Modificar" value="Modificar"><input name="hide" type="hidden" value="<?php echo $id_usu;?>"></td>
                </tr>
                </table>        
        </form>
        <h3></br></br><?php echo $text["mod_usuarios text5"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia = "select * from perfil order by id_perfil asc offset $RegistrosAEmpezar limit $RegistrosAMostrar";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['id_perfil'];?></th>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo $rs['pais'];?></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        </div>
        <table><tr>
        <?php
$NroRegistros=pg_num_rows(pg_query($con,"select * from perfil order by id_perfil asc"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;

 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='/admin/mod_usuarios.php?pag=1'><img src='../images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='/admin/mod_usuarios.php?pag=$PagAnt'><img src='../images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='/admin/mod_usuarios.php?pag=$PagSig'><img src='../images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='/admin/mod_usuarios.php?pag=$PagUlt'><img src='../images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
  ?>
  		</tr></table>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>