<?php
require_once ("conexion.php");
session_start ();
$permitido = FALSE;
if (isset ( $_SESSION ['USUARIO'] )) {
	$user = $_SESSION ['USUARIO'];
	$permisos = $_SESSION ['PERMISOS'];
	$cargo = $_SESSION ['CARGO'];
	$nacion = $_SESSION [ID_PAIS];
} else {
	header ( 'Location:error.php' );
}
if ($_GET ['id']) {
	$id_jugador = is_numeric ( $_GET ['id'] ) ? $_GET ['id'] : 0;
} elseif ($_POST ['id']) {
	$id_jugador = is_numeric ( $_POST ['id'] ) ? $_POST ['id'] : 0;
} elseif ($_GET ['IDj']) {
	$id_jugador = is_numeric ( $_GET ['IDj'] ) ? $_GET ['IDj'] : 0;
} elseif ($_GET ['Actualizar']) {
	$id_jugador = is_numeric ( $_GET ['Actualizar'] ) ? $_GET ['Actualizar'] : 0;
} else {
	$id_jugador = is_numeric ( $_POST ['idjug_oculto'] ) ? $_POST ['idjug_oculto'] : 0;
}
$saber_dueno = pg_query ( $con, "select usuario.id_usuario from usuario,equipo,jugador,manager where jugador.id_jugador = $id_jugador and jugador.id_equipo = equipo.id_equipo and equipo.id_manager = manager.id_manager and usuario.id_manager = manager.id_manager" );
if ($tener_dueno = pg_fetch_array ( $saber_dueno )) {
	$dueno = $tener_dueno ['id_usuario'];
}
$consultar = pg_query ( $con, "select * from usuario where id_usuario = $user" );
$rs = pg_fetch_array ( $consultar );
if ($rs) {
	$nombre = $rs ['nombre'];
} else {
	header ( 'Location:error.php' );
}

if ($permisos < 1) {
	$consultar = pg_query ( $con, "select * from usuario a left join equipo b on a.id_manager=b.id_manager left join jugador c on c.id_equipo=b.id_equipo where a.id_usuario = $user and c.id_jugador=$id_jugador" );
	$rs = pg_fetch_array ( $consultar );
	if ($rs == FALSE) {
		header ( 'Location:error.php' );
	}
	if ($_GET ['Leido']) {
		$id_comentario = is_numeric ( $_GET ['Leido'] ) ? $_GET ['Leido'] : 0;
		$marcar = pg_query ( $con, "update comentario_jugador set leido = TRUE where id_comentario_jugador = '$id_comentario' and id_usuario<>$user" );
	}
} else {
	if ($_GET ['IDC']) {
		$eli = is_numeric ( $_GET ['IDC'] ) ? $_GET ['IDC'] : 0;
		$sen_eli = pg_query ( $con, "delete from comentario_jugador where id_comentario_jugador = '$eli'" );
	}
	if ($_GET ['Leido']) {
		$id_comentario = is_numeric ( $_GET ['Leido'] ) ? $_GET ['Leido'] : 0;
		$marcar = pg_query ( $con, "update comentario_jugador set leido = TRUE where id_comentario_jugador = '$id_comentario' and id_usuario<>$user" );
	}
}
if ($permisos > 1 && $_POST ['ActualizarHab']) {
	$cond = is_numeric (str_replace(",",".",$_POST ['condicion']) ) ? str_replace(",",".",$_POST ['condicion']) : 0;
	$jug = is_numeric (str_replace(",",".",$_POST ['jugadas']) ) ? str_replace(",",".",$_POST ['jugadas']) : 0;
	$pas = is_numeric (str_replace(",",".",$_POST ['pases']) ) ? str_replace(",",".",$_POST ['pases']) : 0;
	$def = is_numeric (str_replace(",",".",$_POST ['defensa']) ) ? str_replace(",",".",$_POST ['defensa']) : 0;
	$ano = is_numeric (str_replace(",",".",$_POST ['anotacion']) ) ? str_replace(",",".",$_POST ['anotacion']) : 0;
	$bp = is_numeric (str_replace(",",".",$_POST ['balonparado']) ) ? str_replace(",",".",$_POST ['balonparado']) : 0;
	$lat = is_numeric (str_replace(",",".",$_POST ['lateral']) ) ? str_replace(",",".",$_POST ['lateral']) : 0;
	$por = is_numeric (str_replace(",",".",$_POST ['porteria']) ) ? str_replace(",",".",$_POST ['porteria']) : 0;
	$comando = "select update_jug2($id_jugador,$cond,$jug,$def,$pas,$lat,$ano,$por,$bp)";
	$dumb = pg_query ( $con, $comando);
	
}

if ($_POST ['Enviar']) {
	$comentario = str_replace ( ';', '', pg_escape_string ( $_POST ['txt_comentario'] ) );
	$id_comentador = $user;
	$sentencia_comentario = "insert into comentario_jugador values(DEFAULT,$id_comentador,$id_jugador,now(),'" . (str_replace ( "'", "''", $comentario )) . "',FALSE)";
	$guardar_comentario = pg_query ( $con, $sentencia_comentario );
}

if ($_GET ['Jugador'] && $_GET ['Lista']) {
	$id_jugador = is_numeric ( $_GET ['Jugador'] ) ? $_GET ['Jugador'] : 0;
	$id_lista = is_numeric ( $_GET ['Lista'] ) ? $_GET ['Lista'] : 0;
	$_SESSION ['ID_LISTA'] = $id_lista;
	$sentencia = pg_query ( $con, "select nombre from listado where id_lista = $id_lista" );
	if ($ejecutar = pg_fetch_array ( $sentencia )) {
		$nom_lista = $ejecutar ['nombre'];
	}
	$sen = pg_query ( $con, "select nombre from jugador where id_jugador = $id_jugador" );
	if ($ejec = pg_fetch_array ( $sen )) {
		$nom_player = $ejec ['nombre'];
	}
}
if ($_POST ['Asignar']) {
	$id_cat_asig = is_numeric ( $_POST ['cbo_cat'] ) ? $_POST ['cbo_cat'] : 0;
	$id_jug_asig = is_numeric ( $_POST ['idjug_oculto'] ) ? $_POST ['idjug_oculto'] : 0;
	$id_lis_asig = is_numeric ( $_SESSION ['ID_LISTA'] ) ? $_SESSION ['ID_LISTA'] : 0;
	$id_lis_ocu = is_numeric ( $_POST ['listaoculta'] ) ? $_POST ['listaoculta'] : 0;
	$asignar = pg_query ( $con, "update jugador set id_planes_entrenamiento = '$id_cat_asig' where id_jugador = '$id_jug_asig'" );
	$id_jugador = $id_jug_asig;
	$id_lista = $_SESSION ['ID_LISTA'];
}
require_once ("textos.php");
$titulo = "jugador";
require_once ("head.php");
?>

<body><?php include_once("seguimientoanalytics.php")?>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
	<script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
	<script type="text/javascript">
      function drawVisualization() {
        // Create and populate the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'x');
        data.addColumn('number', '<?php echo $text["Graf_forma"][$_SESSION[IDIOMA]];?>');
        data.addColumn('number', '<?php echo $text["Graf_condicion"][$_SESSION[IDIOMA]];?>');
		<?php
		$sen_grafica = pg_query ( $con, "select temporada,semana,forma,condicion from historia_jugador where id_jugador = $id_jugador order by temporada desc,semana desc limit 40" );
		$da = array_reverse ( pg_fetch_all ( $sen_grafica ) );
		foreach ( $da as $v ) {
			echo "data.addRow(['T.$v[temporada]-S.$v[semana]',$v[forma],$v[condicion]]);";
		}
		?>

        // Create and draw the visualization.
        new google.visualization.LineChart(document.getElementById('visualization')).
            draw(data, {curveType: "none",
                        width: 650, height: 300,
                        vAxis: {maxValue: 10}}
                );
      }
      

      google.setOnLoadCallback(drawVisualization);
</script>
	<div id="header">
		<div>
			<a href="index.php" class="logo"><img src="images/logo2.png" alt="" width="192" height="42" /></a>
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<div class="column">
			<img src="images/top.gif" alt="" width="231" height="5" /><br />
			<div>
				<img src="images/titlejug.gif" alt="" width="209" height="30" />
				<p>
					<strong><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?>: <?php echo $nombre;?></strong>. <br> <br><?php echo $text["datos_jugador text1"][$_SESSION[IDIOMA]];?>.
                <?php if($_GET['Jugador'] || $_GET['Jugador_Cat'] || $id_lis_ocu != ""){?><br> <br> <a
						href="lista/usuarios_lis.php?ID=<?php echo $id_lista;?>"><?php echo $text["datos_jugador text11"][$_SESSION[IDIOMA]];?></a><?php }?>
				</p>
			</div>
			<img src="images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
		<div class="list">
        <?php
								if ($permisos > 1 || $user == $dueno) {
									$verificar = pg_query ( $con, "select * from jugador where id_jugador = $id_jugador" );
									if ($recibir_ver = pg_fetch_array ( $verificar )) {
										$tener_categoria = $recibir_ver ['id_planes_entrenamiento'];
										if (empty ( $tener_categoria ) == 1 || $_GET ['Actualizar']) {
											?>
        <h3><?php echo $text["datos_jugador text2"][$_SESSION[IDIOMA]];?><?php echo $recibir_ver['nombre'];?></h3>
			<p><?php echo $text["datos_jugador text3"][$_SESSION[IDIOMA]];?>.<br>
			</p>
			<div class="list">
				<form action="datos_jugador.php" method="post">
					<table width="300">
						<tr>
							<td><?php echo $text["Categorias"][$_SESSION[IDIOMA]];?>:</td>
							<td><select name="cbo_cat" size="1" id="cbo_cat">
                  <?php
											$listar = "select * from planes_entrenamiento order by id_planes_entrenamiento";
											$ejecutar = pg_query ( $con, $listar );
											while ( $rs = pg_fetch_array ( $ejecutar ) ) {
												echo "<option value='$rs[id_planes_entrenamiento]'>$rs[nombre]</option>";
											}
											?>
                </select></td>
						</tr>
						<tr>
							<td colspan="2"><input name="Asignar" type="submit" id="Asignar" value="Asignar"><input name="idjug_oculto" type="hidden"
								value="<?php echo $id_jugador;?>"><input name="listaoculta" type="hidden" value="<?php echo $id_lista;?>"></td>
						</tr>
					</table>
				</form>
			</div>
			<p>
				<br> <br>
			</p>
        <?
										}
									}
								}
								?>
		<?php
// 		$consultar = pg_query ( $con, "select *,EXTRACT(DAY FROM now()-fechanacimiento) dias from jugador where id_jugador=$id_jugador;" );
		$consultar = pg_query ( $con, "select * from datos_jug($id_jugador);" );
		$rsteam = pg_fetch_array ( $consultar );
		if ($rsteam) {
			$nacionalidad = $rsteam ['pais'];
			echo "<h3>" . $rsteam ['nombre'] . "</h3>";
			?>
         <div class="main_part">
				<div class="block" style="width: 350px;">
					<table width="300">
						<tr>
							<td><strong>ID Hattrick:</strong></td>
							<td><?php echo $id_jugador;?></td>
						</tr>
						<tr>
							<td><strong><?php echo $text["Pais"][$_SESSION[IDIOMA]];?>:</strong></td>
							<td><img src="images/flags/<?php echo $rsteam['pais'];?>flag.png"></td>
						</tr>
						<tr>
							<td><strong><?php echo $text["Edad"][$_SESSION[IDIOMA]];?>:</strong></td>
							<td><?php echo floor($rsteam['dias']/112).' a&ntildeos con '.($rsteam['dias']%112).' d&iacuteas';?></td>
						</tr>
						<tr>
							<td><strong><?php echo $text["Especialidad"][$_SESSION[IDIOMA]];?>:</strong></td>
							<td><?php if($rsteam['especialidad']>0){echo '<img src="images/icons/spec'.$rsteam['especialidad'].'.png">';}else{echo "Ninguna";}?></td>
						</tr>
						<tr>
							<td><strong><?php echo $text["Lesion"][$_SESSION[IDIOMA]];?>:</strong></td>
							<td><?php if($rsteam['lesion']>0){echo '<img src="images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="images/icons/0injury.png">';}if($rsteam['lesion']==-1){echo "Sin lesi&oacuten";}?></td>
						</tr>
						<tr>
							<td><strong><?php echo $text["Tarjetas"][$_SESSION[IDIOMA]];?>:</strong></td>
							<td><?php if($rsteam['tarjetas']>0){echo '<img src="images/icons/'.$rsteam['tarjetas'].'card.png">';}else{echo "Sin tarjetas";}}?></td>
						</tr>
						<tr>
						
						
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><strong><?php echo $text["Listas"][$_SESSION[IDIOMA]];?>:</strong></td>
						</tr>
						
						
                        <?php $listas = pg_query ( $con, "select * from listadojugador,listado where listadojugador.id_jugador = $id_jugador and listadojugador.id_lista = listado.id_lista" );
							  $esta_lista = FALSE;
							  while ( $listas_j = pg_fetch_array ( $listas ) ) {
								$esta_lista = TRUE;
																									?>
		                   	    <tr>
									<td></td>
									<td><div class="Estilo1"><?php echo "- ".$listas_j['nombre'];?>.</div></td>
								</tr>                        
		                        <?php	} 
                        if ($esta_lista == FALSE) {	?>
	                    	<tr>
								<td colspan="2" align="center"><?php echo $text["datos_jugador text4"][$_SESSION[IDIOMA]];?>.</td>
							</tr> 
                        <?php	} ?>
						
                    </table>
                    <?php if ($permisos >= 1) { ?>
                    <form action="datos_jugador.php" method="post">
                    	<table style="border:0px; width:700px;" class="tabla">
							<tr>
								<th><?php echo $text["Condicion"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Jugadas"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Defensa"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Asistencias"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Lateral"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Anotacion"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Porteria"][$_SESSION[IDIOMA]];?></th>
								<th><?php echo $text["Balon Parado"][$_SESSION[IDIOMA]];?></th>
								<th>TTI</th>
								<th><?php echo $text["Pot"][$_SESSION[IDIOMA]];?></th>
							</tr>
							<tr class="modo1">
								<td><input type="text" class="textinput" name="condicion" value="<?php echo $rsteam['condicion'];?>"></td>
								<td><input type="text" class="textinput" name="jugadas" value="<?php echo $rsteam['jugadas'];?>"></td>
								<td><input type="text" class="textinput" name="defensa" value="<?php echo $rsteam['defensa'];?>"></td>
								<td><input type="text" class="textinput" name="pases" value="<?php echo $rsteam['asistencias'];?>"></td>
								<td><input type="text" class="textinput" name="lateral" value="<?php echo $rsteam['lateral'];?>"></td>
								<td><input type="text" class="textinput" name="anotacion" value="<?php echo $rsteam['anotacion'];?>"></td>
								<td><input type="text" class="textinput" name="porteria" value="<?php echo $rsteam['porteria'];?>"></td>
								<td><input type="text" class="textinput" name="balonparado" value="<?php echo $rsteam['balonparado'];?>"></td>
								<td><?php echo $rsteam['tti'];?></td>
								<td><?php echo number_format ( 100*$rsteam['potencial'] , 1 ,  '.' ,',' )."%";?></td>
							</tr>
                    	</table>
                    	<input name="ActualizarHab" type="submit" value="Actualizar">
                    	<input name="id" type="hidden" value="<?php echo $id_jugador;?>">
                    </form>
                    <?php }
                    elseif ($user == $dueno){ ?>
                    <table style="border:0px; width:500px;" class="tabla">
						<tr>
							<th><?php echo $text["Condicion"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Jugadas"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Defensa"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Asistencias"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Lateral"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Anotacion"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Porteria"][$_SESSION[IDIOMA]];?></th>
							<th><?php echo $text["Balon Parado"][$_SESSION[IDIOMA]];?></th>
						</tr>
						<tr class="modo1">
							<td><?php echo $rsteam['condicion'];?></td>
							<td><?php echo $rsteam['jugadas'];?></td>
							<td><?php echo $rsteam['defensa'];?></td>
							<td><?php echo $rsteam['asistencias'];?></td>
							<td><?php echo $rsteam['lateral'];?></td>
							<td><?php echo $rsteam['anotacion'];?></td>
							<td><?php echo $rsteam['porteria'];?></td>
							<td><?php echo $rsteam['balonparado'];?></td>
						</tr>
                    </table>
                    <?php }?>
				</div>
				<div class="block">
					<form action="datos_jugador.php" method="post">
						<h4><?php echo $text["Comentario"][$_SESSION[IDIOMA]];?></h4>
						<h4>
							<textarea name="txt_comentario" cols="20" rows="5"></textarea>
						</h4>
						<h4>
							<input name="Enviar" type="submit" id="Enviar" value="Enviar"><input name="idjug_oculto" type="hidden" value="<?php echo $id_jugador;?>">
						</h4>
					</form>
				</div>
			</div>
        
		<?php
		$primero = pg_query ( $con, "select id_planes_entrenamiento from jugador where id_jugador = $id_jugador" );
		if ($segundo = pg_fetch_array ( $primero )) {
			$hay_plan = $segundo ['id_planes_entrenamiento'];
		}
		if (! empty ( $hay_plan ) == 1) {
			?>
        <h3>
				<br> <br><?php echo $text["datos_jugador text5"][$_SESSION[IDIOMA]];?></h3>
			<p>
				<br>
			</p>
			<table border="0" cellpadding="0" cellspacing="0" class="tabla">
				<tr>
					<th><?php if($permisos > 1){?><a href="datos_jugador.php?Actualizar=<?php echo $id_jugador;?>"><img src="images/icons/refresh16.ico"></a><?php }?></th>
					<th><?php echo $text["datos_jugador text6"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Jugadas"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Defensa"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Asistencias"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Lateral"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Anotacion"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Porteria"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Balon Parado"][$_SESSION[IDIOMA]];?></th>
				</tr>
        <?php
			$saber_entreno = pg_query ( $con, "select *,EXTRACT(DAY FROM now()-fechanacimiento) anos from jugador where id_jugador=$id_jugador and activo = TRUE" );
			if ($tener_entreno = pg_fetch_array ( $saber_entreno )) {
				$edad_entreno = ( int ) ($tener_entreno ['anos'] / 112);
				$id_categoria = $tener_entreno ['id_planes_entrenamiento'];
				$averiguar_cat = pg_query ( $con, "select * from habilidad_plan_entrenamiento,planes_entrenamiento where planes_entrenamiento.id_planes_entrenamiento = $id_categoria and planes_entrenamiento.id_planes_entrenamiento = habilidad_plan_entrenamiento.id_planes_entrenamiento and habilidad_plan_entrenamiento.edad = $edad_entreno" );
				if ($saber_cat = pg_fetch_array ( $averiguar_cat )) {
					?>
        <tr class="modo1">
					<th colspan="2"><?php echo $saber_cat['nombre'];?></th>
					<td><?php echo $saber_cat['edad'];?></td>
					<td><?php echo $saber_cat['jugadas'];?></td>
					<td><?php echo $saber_cat['defensa'];?></td>
					<td><?php echo $saber_cat['pases'];?></td>
					<td><?php echo $saber_cat['lateral'];?></td>
					<td><?php echo $saber_cat['anotacion'];?></td>
					<td><?php echo $saber_cat['porteria'];?></td>
					<td><?php echo $saber_cat['balonparado'];?></td>
				</tr>
        <?php
				}
			}
			?>
        </table>
        <?php
		}
		?>
        <h3>
				<br> <br><?php echo $text["datos_jugador text7"][$_SESSION[IDIOMA]];?></h3>
			<p>
				<br>
			</p>
			<table border="0" cellpadding="0" cellspacing="0" class="tabla">
				<tr>
					<th><?php echo substr($text["Temporada"][$_SESSION[IDIOMA]], 0, 4);?>.</th>
					<th><?php echo substr($text["Semana"][$_SESSION[IDIOMA]], 0, 4);?>.</th>
					<th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?>.</th>
					<th><img src="images/icons/1card.png" alt="Tarjeta"></th>
					<th><img src="images/icons/1injury.png" alt="Lesión"></th>
					<th><?php echo $text["Fo"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Co"][$_SESSION[IDIOMA]];?></th>
					<th>TSI</th>
            <?php
												$validacion = pg_query ( $con, "select usuario.id_usuario from usuario,manager,equipo,jugador where jugador.id_jugador = '$id_jugador' and jugador.id_equipo = equipo.id_equipo and equipo.id_manager = manager.id_manager and usuario.id_manager = manager.id_manager" );
												if ($validado = pg_fetch_array ( $validacion )) {
													$user_dueno = $validado ['id_usuario'];
												}
												if (($nacionalidad == $nacion && $permisos > 0) || $user_dueno == $user || $permisos == 4) {
													?>
			<th><?php echo $text["Ju"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["De"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Pa"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["La"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["An"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Po"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["BP"][$_SESSION[IDIOMA]];?></th>
            <?php } ?>
			<th><?php echo $text["Ex"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["Dueno"][$_SESSION[IDIOMA]];?></th>
            <?php
												if (($nacionalidad == $nacion && $permisos > 0) || $user_dueno == $user || $permisos == 4) {
													?>
		    <th><?php echo $text["Entrenamiento"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo $text["datos_jugador text8"][$_SESSION[IDIOMA]];?>.</th>
					<th><?php echo $text["datos_jugador text9"][$_SESSION[IDIOMA]];?>.</th>
					<th><?php echo $text["DT"][$_SESSION[IDIOMA]];?></th>
					<th><?php echo "aux"?></th>
					<th><?php echo "fis"?></th>
					<th><?php echo "doc"?></th>
            <?php } ?>
          </tr>
          <?php
										$valor = FALSE;
										$consultar = pg_query ( $con, "select * from historia_jugador($id_jugador,'$_SESSION[IDIOMA]')" );
										while ( $rsteam = pg_fetch_array ( $consultar ) ) {
											$valor = TRUE;
											?>
          <tr class="modo1">
					<td><?php echo $rsteam['temporada'];?>
					
					
					
					</th>
					<td><?php echo $rsteam['semana'];?></td>
					<td><?php echo floor($rsteam[edad]/112).'.'.($rsteam[edad]%112);?></td>
					<td><?php if($rsteam['tarjetas']>0){echo '<img src="images/icons/'.$rsteam['tarjetas'].'card.png">';}?></td>
					<td><?php if($rsteam['lesion']>0){echo '<img src="images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="images/icons/0injury.png">';}?></td>
					<td><?php echo $rsteam['forma'];?></td>
					<td><?php echo $rsteam['condicion'];?></td>
					<td><?php echo number_format($rsteam['tsi'],0,",",".");?></td>
            <?php
											if (($nacionalidad == 17 && $permisos > 0) || $user_dueno == $user || $permisos == 4) {
												?>                
				<td><?php echo $rsteam['jugadas'];?></td>
					<td><?php echo $rsteam['defensa'];?></td>
					<td><?php echo $rsteam['asistencias'];?></td>
					<td><?php echo $rsteam['lateral'];?></td>
					<td><?php echo $rsteam['anotacion'];?></td>
					<td><?php echo $rsteam['porteria'];?></td>
					<td><?php echo $rsteam['balonparado'];?></td>
            <?php } ?>                
				<td><?php echo $rsteam['experiencia'];?></td>
					<td><?php echo $rsteam['nomequip'];?></td>
            <?php
											if (($nacionalidad == 17 && $permisos > 0) || $user_dueno == $user || $permisos == 4) {
												?>           
				<td><?php echo $rsteam['nombreentre'];?></td>
					<td><?php echo $rsteam['intensidad'];?></td>
					<td><?php echo $rsteam['conshare'];?></td>
					<td><?php echo $rsteam['nivelentrenador'];?></td>
					<td><?php echo $rsteam['auxiliares'];?></td>
					<td><?php echo $rsteam['fisioterapeutas'];?></td>
					<td><?php echo $rsteam['doctores'];?></td>
            <?php } ?>
          </tr>
          <?php
										}
										?>
        </table>
			<h3>
				<br><?php echo $text["Comentarios"][$_SESSION[IDIOMA]];?></h3>
        <?php
								$pre_com = pg_query ( $con, "select * from comentario_jugador a left join usuario b on a.id_usuario=b.id_usuario where a.id_jugador = '$id_jugador' order by fecha desc" );
								while ( $sab_com = pg_fetch_array ( $pre_com ) ) {
									$id_us_com = $sab_com ['id_usuario'];
									$fecha = $sab_com ['fecha'];
									$texto_jug = $sab_com ['comentario'];
									$id_coment = $sab_com ['id_comentario_jugador'];
									?>
        <table width="400">
				<tr>
					<td><strong><?php echo $sab_com['nombre'];?></strong><?php echo $text["datos_jugador text10"][$_SESSION[IDIOMA]];?><?php echo $fecha;?>:</td>
					<td align="left" width="20">
        <?php
									if ($id_us_com != $user) {
										$saber_com = pg_query ( $con, "select leido from comentario_jugador where id_comentario_jugador = $id_coment and leido = FALSE" );
										if ($tener_com = pg_fetch_array ( $saber_com )) {
											?>
        <a href="datos_jugador.php?Leido=<?php echo $id_coment;?>&IDj=<?php echo $id_jugador;?>"><img src="images/icons/post_square16_h.ico"></a>
        <?php
										}
									}
									if ($permisos == 4) { // solo admins pueden eliminar
										?>
        <a href="datos_jugador.php?IDC=<?php echo $sab_com['id_comentario_jugador'];?>&id=<?php echo $sab_com['id_jugador'];?>"><img
							src="images/icons/delete_x16_h.ico"></a>
		<?php }?></td>
				</tr>
				<tr>
					<td colspan="2" <?php if ($sab_com['leido']=='f'){?> style="background-color: #77CC77;" <?php }else{?> style="background-color: #CCCCCC;"
						<?php }?> align="center"><p>
							<em><?php echo $texto_jug;?></em><br> <br>
						</p></td>
				</tr>
			</table>
        <?php
								}
								?>
        <h3>
				<br> <br><?php echo $text["Grafica"][$_SESSION[IDIOMA]];?></h3>
			<div class="list">
				<div id="visualization" style="width: 500px; height: 400px;"></div>
			</div>
			<div id="footer"></div>

</body>
</html>