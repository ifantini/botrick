<?php
require_once("conexion.php");
session_start();
$RegistrosAMostrar=30;
$paginar = FALSE;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
	$paginar = TRUE;
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if($_POST['Desplegar']=='Desplegar'){
	//if que recibe la info del formulario
	$pos = is_numeric($_POST['cbo_pos'])?$_POST['cbo_pos']:0;
	$ord = is_numeric($_POST['cbo_ord'])?$_POST['cbo_ord']:0;
	$edad_min = is_numeric($_POST['cbo_min'])?$_POST['cbo_min']:0;
	$edad_max = is_numeric($_POST['cbo_max'])?$_POST['cbo_max']:0;
	$esp = is_numeric($_POST['cbo_esp'])?$_POST['cbo_esp']:0;
	$orden = str_replace(';', '',pg_escape_string($_POST['cbo_por']));//variable que almacena si es ordenado por APORTE o por TTI
	$paginar = TRUE;
}
if($_GET['Desplegar']=='Desplegar'){
	//if que recibe la info del formulario
	$pos = is_numeric($_GET['cbo_pos'])?$_GET['cbo_pos']:0;
	$ord = is_numeric($_GET['cbo_ord'])?$_GET['cbo_ord']:0;
	$edad_min = is_numeric($_GET['cbo_min'])?$_GET['cbo_min']:0;
	$edad_max = is_numeric($_GET['cbo_max'])?$_GET['cbo_max']:0;
	$esp = is_numeric($_GET['cbo_esp'])?$_GET['cbo_esp']:0;
	$orden = str_replace(';', '',pg_escape_string($_GET['cbo_por']));//variable que almacena si es ordenado por APORTE o por TTI
	$paginar = TRUE;
}
require_once("nombres.php");
require_once("textos.php");
require_once("head.php");
?>
<body><?php include_once("seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="jugadores"; include_once("mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/titlejugadores.gif" alt="" width="209" height="30" />
                <p><?php echo $text["jugadores text1"][$_SESSION[IDIOMA]];?>.<br><br></p>
                <form action="jugadores.php" method="post">
                <table cellspacing="2">
                  <tr>
                    <td><?php echo $text["Posicion"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_pos" size="1" id="cbo_pos" style="width: 100%">
                    <?php 
                    $sentencia = pg_query($con,"select id_aporte,nombre from aporte where id_listado_aportes=1;");
					while($rs = pg_fetch_array($sentencia)){?>
                     <option value="<?php echo $rs['id_aporte'];  ?>" <?php if($pos==$rs['id_aporte']){echo " selected";}?>  ><?php echo $rs['nombre']; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Edad Minima"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_min" size="1" id="cbo_min" style="width: 100%">
                    <option value="17" selected>17</option>
                    <?php 
                    for ($i = 18; $i <= 35; $i++) {
                    ?>
                    <option value=<?php echo '"'.$i.'"'; if($edad_min==$i){echo " selected";}  ?>><?php echo $i; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Edad Maxima"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_max" size="1" id="cbo_max" style="width: 100%">
                    <option value="35" selected>35</option>
                    <?php 
                    for ($i = 34; $i >= 17; $i--) {
                    ?>
                    <option value=<?php echo '"'.$i.'"'; if($edad_max==$i){echo " selected";}  ?>><?php echo $i; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Especialidad"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_esp" size="1" id="cbo_esp" style="width: 100%">
                    <option value="-1" selected><?php echo $text["Cualquiera"][$_SESSION[IDIOMA]];?></option>
					<option value="0"<?php if($esp=='0'){echo " selected";} ?>><?php echo denominacion(0,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="1"<?php if($esp==1){echo " selected";} ?>><?php echo denominacion(1,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="2"<?php if($esp==2){echo " selected";} ?>><?php echo denominacion(2,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="3"<?php if($esp==3){echo " selected";} ?>><?php echo denominacion(3,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="4"<?php if($esp==4){echo " selected";} ?>><?php echo denominacion(4,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="5"<?php if($esp==5){echo " selected";} ?>><?php echo denominacion(5,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="6"<?php if($esp==6){echo " selected";} ?>><?php echo denominacion(6,$_SESSION[IDIOMA],'especialidad',$con);?></option>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["jugadores text2"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_por" size="1" id="cbo_por" style="width: 100%">
                    <!--<option value="tti"<?php if($orden=="tti"){echo " selected";} ?>>tti</option>-->
					<!--<option value="potencial"<?php if($orden=="potencial"){echo " selected";} ?>>Potencial</option>-->
					<option value="aporte"<?php if($orden=="aporte"){echo " selected";} ?>><?php echo $text["Aporte"][$_SESSION[IDIOMA]];?></option>
                    </select></td>
                  </tr>
                  <tr>
                  	<td></td>
                    <td align="center" colspan="2"><input name="Desplegar" type="submit" id="Desplegar" value="Desplegar" style="width: 100%"></td>
                  </tr>
                </table>
                </form>
                <div align="center"><object id="counters99" allowscriptaccess="always" type="application/x-shockwave-flash" data="http://static.99widgets.com/counters/swf/counters.swf?id=839795_2&ln=es" width="175" height="200" wmode="transparent"><param name="allowscriptaccess" value="always" /><param name="movie" value="http://static.99widgets.com/counters/swf/counters.swf?id=839795_2&ln=es" /><param name="wmode" value="transparent" /><embed src="http://static.99widgets.com/counters/swf/counters.swf?id=839795_2&ln=es" type="application/x-shockwave-flash" allowscriptaccess="always" wmode="transparent" width="175" height="200"></embed><br><a href="http://www.startdatingtoday.com/">Online dating sites</a> <a href="http://www.99shoutbox.com/">Shoutbox Widget</a> <a href="http://www.amigafx.com/">forex</a> <a href="http://www.onlinecasinoau.com/casino-games-pokies.html">Pokies</a> <a href="http://www.superonlinecasino.com/ireland/">ONLINE CASINO IRELAND</a></object></div>
		  	</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["jugadores text3"][$_SESSION[IDIOMA]];?></h3>
        <p>
        	<?php echo $text["jugadores text4"][$_SESSION[IDIOMA]];?><br><br>
        </p>
        <div class="block"><div class="block">
            <table width="700" border=”0″ cellpadding=”0″ cellspacing=”0″ class="tabla sortable">
              <tr>
                <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
              	<th><img src="/images/icons/onsale.png"></th>                
                <th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
                <th><?php echo $text["EE"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
				<th><img src="/images/icons/2card.png"></th>
				<th><img src="/images/icons/1injury.png"></th>
				<th><?php echo $text["Fo"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Co"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ex"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Li"][$_SESSION[IDIOMA]];?></th>
				<th>TTI</th>
				<th><?php echo $text["Pot"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Aporte"][$_SESSION[IDIOMA]];?></th>
                <th><img src="images/favicon.ico"></th>
              </tr>
          <?php
		  $valor = FALSE;
		  if($_POST['Desplegar'] || $_GET['Desplegar']){//if que recibe la info del formulario
			$sentencia = pg_query($con,"select * from top_jugadores($pos,$edad_min,$edad_max,$esp) where pais = 17 order by $orden desc offset $RegistrosAEmpezar limit $RegistrosAMostrar;");
			while($rsteam = pg_fetch_array($sentencia)){
				$valor = TRUE;
		  ?>
              <tr class="modo1">
                <th><?php echo substr($rsteam['nombre'],0,23);?></th>
                <td><?php if ($rsteam['en_venta']=="t"){?><img src="/images/icons/onsale.png"><?php }?></td>              
                <td sorttable_customkey="<?php echo $rsteam['dias'];?>"><?php echo floor($rsteam['dias']/112).'.'.($rsteam['dias']%112);?></td>
                <td sorttable_customkey="<?php echo $rsteam['especialidad'];?>"><?php if($rsteam['especialidad']>0){echo '<img src="/images/icons/spec'.$rsteam['especialidad'].'.png">';}?></td>
				<td sorttable_customkey="<?php echo $rsteam['pais'];?>"><img src="/images/flags/<?php echo $rsteam['pais'];?>flag.png"></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tarjetas'];?>"><?php if($rsteam['tarjetas']>0){echo '<img src="/images/icons/'.$rsteam['tarjetas'].'card.png">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lesion'];?>"><?php if($rsteam['lesion']>0){echo '<img src="/images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="/images/icons/0injury.png">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['forma']; ?>"><?php echo $rsteam['forma'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['condicion']; ?>"><?php echo $rsteam['condicion'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['experiencia']; ?>"><?php echo $rsteam['experiencia'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['liderazgo']; ?>"><?php echo $rsteam['liderazgo'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tti']; ?>"><?php echo $rsteam['tti'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['potencial']; ?>"><?php echo number_format ( 100*$rsteam['potencial'] , 1 ,  '.' ,',' )."%";?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['aporte']; ?>"><?php echo number_format ( $rsteam['aporte'] ,0,  '.' ,',' );?></td>
                <td><?php if($rsteam['listas'] >= 1){?><img src="images/favicon.ico"><?php }else{?>&nbsp;<?php }?></td>             
              </tr>
          <?php
			}//end while
		  }//end if
		  if($valor == FALSE){
		  ?>
          <tr>
          	<td colspan="12" align="center"><?php echo $text["jugadores text5"][$_SESSION[IDIOMA]];?>.</td>
          </tr>
          <?php
		  }
		  ?>
        </table>
        <table><tr>
        <?php
		if($paginar == TRUE){
	$sentencia = pg_query($con,"select * from top_jugadores($pos,$edad_min,$edad_max,$esp) where pais = 17 order by $orden desc");
	$NroRegistros=pg_num_rows($sentencia);
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;
 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='jugadores.php?pag=1&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='jugadores.php?pag=$PagAnt&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='jugadores.php?pag=$PagSig&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='jugadores.php?pag=$PagUlt&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
}
  ?>
  		</tr></table>
        </div>        
        </div>
        </div>
        <?php include_once('footer.php');?>
    </body>
</html>