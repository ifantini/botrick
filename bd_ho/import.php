﻿<?php

header("Content-Type: text/html;charset=utf-8");


$allowed_extensions = array('csv');

$upload_path = '/tmp/';

if (!empty($_FILES['file'])) {

	if ($_FILES['file']['error'] == 0) {
			
		// check extension
		$file = explode(".", $_FILES['file']['name']);
		$extension = array_pop($file);
		
		if (in_array($extension, $allowed_extensions)) {
	
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) {
				

				$fila = 1;

				if (($handle = fopen($upload_path.'/'.$_FILES['file']['name'], "r")) !== false) {
					

					while (!feof($handle) ) {
					
						$line_of_text = fgetcsv($handle, 1024);
						
																		
 						if ($line_of_text[0] != null){
 							
 								$id= $line_of_text[0];
 								$nombre= $line_of_text[1];
 								$edad= $line_of_text[2];
 								$con= $line_of_text[3];
 								$for= $line_of_text[4];
 								$lid= $line_of_text[5];
 								$xp= $line_of_text[6];
 								$lesion= $line_of_text[7];
 								$dias= $line_of_text[8];
 								$spe= $line_of_text[9];
 								$por= $line_of_text[10];
 								$def= $line_of_text[11];
 								$jug= $line_of_text[12];
 								$lat= $line_of_text[13];
 								$ano= $line_of_text[14];
 								$pas= $line_of_text[15];
 								$bp= $line_of_text[16];
		
 						}
						
 						
						
					}
					
						fclose($handle);  
				}
				
			}
			
		} else {
			$message = '<span class="red">Only .csv file format is allowed</span>';
		}
		
	} else {
		$message = '<span class="red">Debe subir un archivo de configuracion</span>';
	}
	
}

?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" lang="es-es" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BD for HO</title>

<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>

<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900|Quicksand:400,700|Questrial" rel="stylesheet" />
<link href="css/default_insert.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" media="all" />


<script src="js/jquery-1.8.2.min.js"></script>

</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">Importar Configuración</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Chile</a></li>
				<li><a href="rival.php" accesskey="2" title="">Otros Países</a></li>
				<li><a href="https://docs.google.com/document/d/1yU_4rIQ5UT8-srTZb-jJM0fIJDVpyqwIB2NKEy-k-JQ/edit?usp=sharing" accesskey="3" title="">Tutorial</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="page-wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
			
			<form action="import.php" method="post" enctype="multipart/form-data">
				
				<input type="submit" value="Cargar Configuración" />
				<input type="file" name="file" id="file" size="30" />
				
				<br></br>
				<table>
						<tr>
						<td>Id: </td>
						<td><input type="text" name="t_id" size="1" value="0" readonly="readonly"></td>
						<td>Nombre: </td>
						<td><input type="text" name="t_name" size="1" value="<?php echo $nombre; ?>" readonly="readonly"></td>
						<td>Edad: </td>
						<td><input type="text" name="t_edad" size="1" value="<?php echo $edad; ?>" readonly="readonly"></td>
						<td>Condición: </td>
						<td><input type="text" name="t_con" size="1" value="<?php echo $con; ?>" readonly="readonly"></td>
						<td>Forma: </td>
						<td><input type="text" name="t_for" size="1" value="<?php echo $for; ?>" readonly="readonly"></td>
						<td>Liderazgo: </td>
						<td><input type="text" name="t_lid" size="1" value="<?php echo $lid; ?>" readonly="readonly"></td>
						<td>Experiencia: </td>
						<td><input type="text" name="t_xp" size="1" value="<?php echo $xp; ?>" readonly="readonly"></td>
						<td>Lesión: </td>
						<td><input type="text" name="t_lesion" size="1" value="<?php echo $lesion; ?>" readonly="readonly"></td>
						<td>Edad Días: </td>
						<td><input type="text" name="t_dias" size="1" value="<?php echo $dias; ?>" readonly="readonly"></td>
						<td>Especialidad: </td>
						<td><input type="text" name="t_spe" size="1" value="<?php echo $spe; ?>" readonly="readonly"></td>
						</tr>
						<tr>
						<td>Portería: </td>
						<td><input type="text" name="t_por" size="1" value="<?php echo $por; ?>" readonly="readonly"></td>
						<td>Defensa: </td>
						<td><input type="text" name="t_def" size="1" value="<?php echo $def; ?>" readonly="readonly"></td>
						<td>Jugadas: </td>
						<td><input type="text" name="t_jug" size="1" value="<?php echo $jug; ?>" readonly="readonly"></td>
						<td>Lateral: </td>
						<td><input type="text" name="t_lat" size="1" value="<?php echo $lat; ?>" readonly="readonly"></td>
						<td>Anotación: </td>
						<td><input type="text" name="t_ano" size="1" value="<?php echo $ano; ?>" readonly="readonly"></td>
						<td>Pases: </td>
						<td><input type="text" name="t_pas" size="1" value="<?php echo $pas; ?>" readonly="readonly"></td>
						<td>Balon Parado: </td>
						<td><input type="text" name="t_bp" size="1" value="<?php echo $bp; ?>" readonly="readonly"></td>
						</tr>
					
					</table>

			</form>
			<br>
			<?php  
				echo "<a href='index.php?t_name=$nombre&t_edad=$edad&t_con=$con&t_for=$for&t_lid=$lid&t_xp=$xp&t_lesion=$lesion&t_dias=$dias&t_spe=$spe&t_por=$por&t_def=$def&t_jug=$jug&t_lat=$lat&t_ano=$ano&t_pas=$pas&t_bp=$bp'>IMPORTAR ÉSTA CONFIGURACIÓN</a>";
			?>
			</br>
			</div>
			
		</div>
		
	</div>
</div>

</body>
</html>
