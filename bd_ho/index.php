﻿<?php
header("Content-Type: text/html;charset=utf-8");

$txt_cantNom = $_POST['txt_cantNom'];

//obtener diferencia de fechas
function daysDifference($endDate, $beginDate){
$date_parts1=explode("-", $beginDate);
$date_parts2=explode("-", $endDate);
$start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
$end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
return $end_date - $start_date;
}

$difDias = daysDifference(date("Y-m-d"),'2014-01-18');
//

if($_POST[cargar]){

$message = null;

$txt_id = $_POST['txt_id'];
$txt_name = $_POST['txt_name'];
$txt_edad = $_POST['txt_edad'];
$txt_con = $_POST['txt_con'];
$txt_for = $_POST['txt_for'];
$txt_lid = $_POST['txt_lid'];
$txt_spe = $_POST['txt_spe'];
$txt_xp = $_POST['txt_xp'];
$txt_lesion = $_POST['txt_lesion'];
$txt_dias = $_POST['txt_dias'];
$txt_por = $_POST['txt_por'];
$txt_def = $_POST['txt_def'];
$txt_jug = $_POST['txt_jug'];
$txt_lat = $_POST['txt_lat'];
$txt_ano = $_POST['txt_ano'];
$txt_pases = $_POST['txt_pases'];
$txt_bp = $_POST['txt_bp'];


$allowed_extensions = array('csv');

$upload_path = '/tmp/';

if (!empty($_FILES['file'])) {

	if ($_FILES['file']['error'] == 0) {
			
		// check extension
		$file = explode(".", $_FILES['file']['name']);
		$extension = array_pop($file);
		
		if (in_array($extension, $allowed_extensions)) {
	
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) {
				
				if (($handle = fopen($upload_path.'/'.$_FILES['file']['name'], "r")) !== false) {
					
					$jugadores = 0;
					$contNom = 0;
					while (!feof($handle) ) {
					
						$line_of_text = fgetcsv($handle, 1024);

						//otros
						$id = $line_of_text[$txt_id];
						$nombre = $line_of_text[$txt_name];
						$anos = $line_of_text[$txt_edad];
						$con = intval($line_of_text[$txt_con]);
						$for = intval($line_of_text[$txt_for]);
						$lid = intval($line_of_text[$txt_lid]);
						$spe = $line_of_text[$txt_spe];
						$xp = intval($line_of_text[$txt_xp]);
						$lesion = $line_of_text[$txt_lesion];
						$dias = $line_of_text[$txt_dias];
						//fin otros
						
						//habilidades
						$por = intval($line_of_text[$txt_por]);
						$def = intval($line_of_text[$txt_def]);
						$jug = intval($line_of_text[$txt_jug]);
						$lat = intval($line_of_text[$txt_lat]);
						$ano = intval($line_of_text[$txt_ano]);
						$pas = intval($line_of_text[$txt_pases]);
						$bp = intval($line_of_text[$txt_bp]);
						//fin habilidades

						
						//subnivel
						$subpor = explode(".",$line_of_text[$txt_por]);
						$subdef = explode(".",$line_of_text[$txt_def]);
						$subjug = explode(".",$line_of_text[$txt_jug]);
						$sublat = explode(".",$line_of_text[$txt_lat]);
						$subano = explode(".",$line_of_text[$txt_ano]);
						$subpas = explode(".",$line_of_text[$txt_pases]);
						$subbp = explode(".",$line_of_text[$txt_bp]);
						//fin subnivel
						
						//Especialidad
						if($spe=="T") {
							$varEspe = 1;
						} else if($spe=="R") {
							$varEspe = 2;
						} else if($spe=="P") {
							$varEspe = 3;
						} else if($spe=="I") {
							$varEspe = 4;
						} else if($spe=="C") {
							$varEspe = 5;						
						} else {
							$varEspe = 0;
						}
						//fin especialidad

						
 						if (substr($id, 0, 1) != null){
 							
 							if (substr($id, 0, 1) == '0' or substr($id, 0, 1) == '1' or substr($id, 0, 1) == '2' or substr($id, 0, 1) == '3' or substr($id, 0, 1) == '4' or substr($id, 0, 1) == '5' or substr($id, 0, 1) == '6' or substr($id, 0, 1) == '7' or substr($id, 0, 1) == '8' or substr($id, 0, 1) == '9'){

 								$insert[$jugadores]='INSERT INTO "PUBLIC"."PUBLIC".SPIELER (HRF_ID, DATUM, GELBEKARTEN, SPIELERID, "NAME", AGE, KONDITION, FORM, TORWART, VERTEIDIGUNG, SPIELAUFBAU, FLUEGEL, TORSCHUSS, PASSPIEL, STANDARDS, SUBTORWART, SUBVERTEIDIGUNG, SUBSPIELAUFBAU, SUBFLUEGEL, SUBTORSCHUSS, SUBPASSPIEL, SUBSTANDARDS, OFFSETTORWART, OFFSETVERTEIDIGUNG, OFFSETSPIELAUFBAU, OFFSETFLUEGEL, OFFSETTORSCHUSS, OFFSETPASSPIEL, OFFSETSTANDARDS, ISPEZIALITAET, ICHARAKTER, IANSEHEN, IAGRESSIVITAET, FUEHRUNG, ERFAHRUNG, GEHALT, BONUS, LAND, MARKTWERT, VERLETZT, TOREFREUND, TORELIGA, TOREPOKAL, TOREGESAMT, HATTRICK, BEWERTUNG, TRAINERTYP, TRAINER, PLAYERNUMBER, TRANSFERLISTED, CAPS, CAPSU20, AGEDAYS, TRAININGBLOCK, LOYALTY, HOMEGROWN)  VALUES (7, ' . "'2014-01-18 16:07:57.0'" . ", " . "0" . ", " . $id . ", " . "'". $nombre ."'" . ", " . $anos . ", " . $con . ", " . $for . ", " . $por . ", " . $def . ", " . $jug . ", " . $lat . ", " . $ano . ", " . $pas . ", " . $bp . ", " . "0.".$subpor[1]. ", " . "0.".$subdef[1]. ", " . "0.".$subjug[1]. ", " . "0.".$sublat[1]. ", " . "0.".$subano[1]. ", " . "0.".$subpas[1]. ", " . "0.".$subbp[1].	", ". "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0" . ", " . $varEspe. ", " . "1, 1, 0" . ", ". $lid . ", " . $xp . ", " . "1, 0" . ", " . "17" . ", " . "0" . ", " . $lesion . ", " . "0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, " . ($dias-$difDias) . ", " . "false, 0, false) \n\n";						
 								$jugadores++;
 								
 								$nominados[$contNom]= 'INSERT INTO "PUBLIC"."PUBLIC".SPIELERNOTIZ (SPIELERID, NOTIZ, SPIELBERECHTIGT, TEAMINFOSMILIE, MANUELLERSMILIE, USERPOS)  VALUES (' . $id . ", '', true, 'A-Team.png', '', -1) \n\n";
 								$contNom++;
	
 							}
 						}

					}
				
						fclose($handle);  
				}				
			}
			
		} else {
			$message = '<span class="red">Solo .csv es permitido</span>';
		}
		
	} else {
		$message = '<span class="red">Hay algÃºn problema con tu archivo</span>';
	}
}
};

//exportar configuracion
if ($_POST[exportar]) {
	
	$txt_id = $_POST['txt_id'];
	$txt_name = $_POST['txt_name'];
	$txt_edad = $_POST['txt_edad'];
	$txt_con = $_POST['txt_con'];
	$txt_for = $_POST['txt_for'];
	$txt_lid = $_POST['txt_lid'];
	$txt_spe = $_POST['txt_spe'];
	$txt_xp = $_POST['txt_xp'];
	$txt_lesion = $_POST['txt_lesion'];
	$txt_dias = $_POST['txt_dias'];
	$txt_por = $_POST['txt_por'];
	$txt_def = $_POST['txt_def'];
	$txt_jug = $_POST['txt_jug'];
	$txt_lat = $_POST['txt_lat'];
	$txt_ano = $_POST['txt_ano'];
	$txt_pases = $_POST['txt_pases'];
	$txt_bp = $_POST['txt_bp'];
		
	$list = $txt_id . "," . $txt_name . "," . $txt_edad . "," . $txt_con . "," . $txt_for . "," . $txt_lid . "," . $txt_xp . "," . $txt_lesion . "," . $txt_dias . ","  . $txt_spe . "," . $txt_por . "," . $txt_def . "," . $txt_jug . "," . $txt_lat . "," . $txt_ano . "," . $txt_pases . "," . $txt_bp;
	
	$handle = fopen("myConf.csv", "w");
	fwrite($handle, $list);
	fclose($handle);
	
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename('myConf.csv'));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize('myConf.csv'));
	readfile('myConf.csv');
	exit;
};
//fin 


$txt_id = "0";
$txt_name = $_GET['t_name'];
$txt_edad = $_GET['t_edad'];
$txt_con = $_GET['t_con'];
$txt_for = $_GET['t_for'];
$txt_lid = $_GET['t_lid'];
$txt_spe = $_GET['t_spe'];
$txt_xp = $_GET['t_xp'];
$txt_lesion = $_GET['t_lesion'];
$txt_dias = $_GET['t_dias'];
$txt_por = $_GET['t_por'];
$txt_def = $_GET['t_def'];
$txt_jug = $_GET['t_jug'];
$txt_lat = $_GET['t_lat'];
$txt_ano = $_GET['t_ano'];
$txt_pases = $_GET['t_pas'];
$txt_bp = $_GET['t_bp'];

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-es" lang="es-es" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BD for HO</title>

<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>

<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900|Quicksand:400,700|Questrial" rel="stylesheet" />
<link href="css/default_insert.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript">
$(document).ready(function(){

	// Con este código seleccionamos el contenido de textarea bajo petición
	$("#selectall").click( function(){
		$("#areatexto").select();
		$("#areatexto").elastic();
		return false;
	});
		
});	
</script>


<script src="js/jquery-1.8.2.min.js"></script>

</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">HO CHILE</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Chile</a></li>
				<li><a href="rival.php" accesskey="2" title="">Otros Países</a></li>
				<li><a href="https://docs.google.com/document/d/1yU_4rIQ5UT8-srTZb-jJM0fIJDVpyqwIB2NKEy-k-JQ/edit?usp=sharing" accesskey="3" title="">Tutorial</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="page-wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
			
			<form action="index.php" method="post" enctype="multipart/form-data">
				
				<fieldset>
				<legend><input type="file" name="file" id="file" size="30" /></legend>
				<p></p>
				Indique cantidad de Nominados:<input type="text" name="txt_cantNom" size="1" value="">Opcional</td>
				<br></br>
				<table>
						<tr>
						<td>Id: </td>
						<td><input type="text" name="txt_id" size="1" value="0" class="Fields" readonly="readonly"></td>
						<td>  Nombre: </td>
						<td><input type="text" name="txt_name" size="1" value="<?php if($txt_name==null){echo "2";}else{echo $_GET['t_name'];}?>"></td>
						<td>Edad: </td>
						<td><input type="text" name="txt_edad" size="1" value="<?php if($txt_edad==null){echo "10";}else{echo $_GET['t_edad'];}?>"></td>
						<td>Condición: </td>
						<td><input type="text" name="txt_con" size="1" value="<?php if($txt_con==null){echo "17";}else{echo $_GET['t_con'];}?>"></td>
						<td>Forma: </td>
						<td><input type="text" name="txt_for" size="1" value="<?php if($txt_for==null){echo "16";}else{echo $_GET['t_for'];}?>"></td>
						<td>Liderazgo: </td>
						<td><input type="text" name="txt_lid" size="1" value="<?php if($txt_lid==null){echo "27";}else{echo $_GET['t_lid'];}?>"></td>
						<td>Experiencia: </td>
						<td><input type="text" name="txt_xp" size="1" value="<?php if($txt_xp==null){echo "18";}else{echo $_GET['t_xp'];}?>"></td>
						<td>Lesión: </td>
						<td><input type="text" name="txt_lesion" size="1" value="<?php if($txt_lesion==null){echo "28";}else{echo $_GET['t_lesion'];}?>"></td>
						<td>Edad Días: </td>
						<td><input type="text" name="txt_dias" size="1" value="<?php if($txt_dias==null){echo "11";}else{echo $_GET['t_dias'];}?>"></td>
						<td>Especialidad: </td>
						<td><input type="text" name="txt_spe" size="1" value="<?php if($txt_spe==null){echo "15";}else{echo $_GET['t_spe'];}?>"></td>
						</tr>
						<tr>
						<td>Portería: </td>
						<td><input type="text" name="txt_por" size="1" value="<?php if($txt_por==null){echo "23";}else{echo $_GET['t_por'];}?>"></td>
						<td>Defensa: </td>
						<td><input type="text" name="txt_def" size="1" value="<?php if($txt_def==null){echo "25";}else{echo $_GET['t_def'];}?>"></td>
						<td>Jugadas: </td>
						<td><input type="text" name="txt_jug" size="1" value="<?php if($txt_jug==null){echo "20";}else{echo $_GET['t_jug'];}?>"></td>
						<td>Lateral: </td>
						<td><input type="text" name="txt_lat" size="1" value="<?php if($txt_lat==null){echo "21";}else{echo $_GET['t_lat'];}?>"></td>
						<td>Anotación: </td>
						<td><input type="text" name="txt_ano" size="1" value="<?php if($txt_ano==null){echo "22";}else{echo $_GET['t_ano'];}?>"></td>
						<td>Pases: </td>
						<td><input type="text" name="txt_pases" size="1" value="<?php if($txt_pases==null){echo "24";}else{echo $_GET['t_pas'];}?>"></td>
						<td>Balon Parado: </td>
						<td><input type="text" name="txt_bp" size="1" value="<?php if($txt_bp==null){echo "26";}else{echo $_GET['t_bp'];}?>"></td>
						</tr>
					
					</table>
				
				<input type="submit" id="btn" name="cargar" class="button button-small" value="Crear Insert" /> <input type="submit" name="exportar" value="Exportar" /> <input type="button" id="importar" name="importar" value="Importar" onclick = "location ='import.php'"/>
				</fieldset>
			</form>
						
			<br><button id="selectall">Seleccionar todo</button></br>
			<textarea id="areatexto" cols="168" rows="30">
			<?php
				echo $message;
				//for para los insert jugadores		
				for ($a=0; $a < sizeof($insert); $a++ ){
					echo $insert[$a]; 
				}
				
				if($txt_cantNom!=null){
					//for para los nominados
					for ($b=0; $b <$txt_cantNom; $b++ ){
						echo $nominados[$b];
					}
				}

			?>
			</textarea>				
			</div>
			
		</div>
		
	</div>
</div>

</body>
</html>