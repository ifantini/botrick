﻿<?php
header("Content-Type: text/html;charset=utf-8");

//obtener diferencia de fechas
function daysDifference($endDate, $beginDate){
$date_parts1=explode("-", $beginDate);
$date_parts2=explode("-", $endDate);
$start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
$end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
return $end_date - $start_date;
}

$difDias = daysDifference(date("Y-m-d"),'2014-01-18');
//

if($_POST[cargar]){

$message = null;

$txt_id = $_POST['txt_id'];
$txt_name = $_POST['txt_name'];
$txt_edad = $_POST['txt_edad'];
$txt_con = $_POST['txt_con'];
$txt_for = $_POST['txt_for'];
$txt_lid = $_POST['txt_lid'];
$txt_spe = $_POST['txt_spe'];
$txt_xp = $_POST['txt_xp'];
$txt_lesion = $_POST['txt_lesion'];
$txt_dias = $_POST['txt_dias'];
$txt_por = $_POST['txt_por'];
$txt_def = $_POST['txt_def'];
$txt_jug = $_POST['txt_jug'];
$txt_lat = $_POST['txt_lat'];
$txt_ano = $_POST['txt_ano'];
$txt_pases = $_POST['txt_pases'];
$txt_bp = $_POST['txt_bp'];

$id_pais = $_POST['pais'];

$allowed_extensions = array('csv');

$upload_path = '/tmp/';

if (!empty($_FILES['file'])) {

	if ($_FILES['file']['error'] == 0) {
			
		// check extension
		$file = explode(".", $_FILES['file']['name']);
		$extension = array_pop($file);
		
		if (in_array($extension, $allowed_extensions)) {
	
			if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_path.'/'.$_FILES['file']['name'])) {
				
				if (($handle = fopen($upload_path.'/'.$_FILES['file']['name'], "r")) !== false) {
					
					$jugadores = 0;
					while (!feof($handle) ) {
					
						$line_of_text = fgetcsv($handle, 1024);

						//otros
						$id = $line_of_text[$txt_id];
						$nombre = $line_of_text[$txt_name];
						$anos = $line_of_text[$txt_edad];
						$con = intval($line_of_text[$txt_con]);
						$for = intval($line_of_text[$txt_for]);
						$lid = intval($line_of_text[$txt_lid]);
						$spe = $line_of_text[$txt_spe];
						$xp = intval($line_of_text[$txt_xp]);
						$lesion = $line_of_text[$txt_lesion];
						$dias = $line_of_text[$txt_dias];
						//fin otros
						
						//habilidades
						$por = intval($line_of_text[$txt_por]);
						$def = intval($line_of_text[$txt_def]);
						$jug = intval($line_of_text[$txt_jug]);
						$lat = intval($line_of_text[$txt_lat]);
						$ano = intval($line_of_text[$txt_ano]);
						$pas = intval($line_of_text[$txt_pases]);
						$bp = intval($line_of_text[$txt_bp]);
						//fin habilidades

						
						//subnivel
						$subpor = explode(".",$line_of_text[$txt_por]);
						$subdef = explode(".",$line_of_text[$txt_def]);
						$subjug = explode(".",$line_of_text[$txt_jug]);
						$sublat = explode(".",$line_of_text[$txt_lat]);
						$subano = explode(".",$line_of_text[$txt_ano]);
						$subpas = explode(".",$line_of_text[$txt_pases]);
						$subbp = explode(".",$line_of_text[$txt_bp]);
						//fin subnivel
						
						//Especialidad
						if($spe=="T") {
							$varEspe = 1;
						} else if($spe=="R") {
							$varEspe = 2;
						} else if($spe=="P") {
							$varEspe = 3;
						} else if($spe=="I") {
							$varEspe = 4;
						} else if($spe=="C") {
							$varEspe = 5;						
						} else {
							$varEspe = 0;
						}
						//fin especialidad
									
						
						
 						if (substr($id, 0, 1) != null){
 							
 							if (substr($id, 0, 1) == '0' or substr($id, 0, 1) == '1' or substr($id, 0, 1) == '2' or substr($id, 0, 1) == '3' or substr($id, 0, 1) == '4' or substr($id, 0, 1) == '5' or substr($id, 0, 1) == '6' or substr($id, 0, 1) == '7' or substr($id, 0, 1) == '8' or substr($id, 0, 1) == '9'){

 								$insert[$jugadores]='INSERT INTO "PUBLIC"."PUBLIC".SPIELER (HRF_ID, DATUM, GELBEKARTEN, SPIELERID, "NAME", AGE, KONDITION, FORM, TORWART, VERTEIDIGUNG, SPIELAUFBAU, FLUEGEL, TORSCHUSS, PASSPIEL, STANDARDS, SUBTORWART, SUBVERTEIDIGUNG, SUBSPIELAUFBAU, SUBFLUEGEL, SUBTORSCHUSS, SUBPASSPIEL, SUBSTANDARDS, OFFSETTORWART, OFFSETVERTEIDIGUNG, OFFSETSPIELAUFBAU, OFFSETFLUEGEL, OFFSETTORSCHUSS, OFFSETPASSPIEL, OFFSETSTANDARDS, ISPEZIALITAET, ICHARAKTER, IANSEHEN, IAGRESSIVITAET, FUEHRUNG, ERFAHRUNG, GEHALT, BONUS, LAND, MARKTWERT, VERLETZT, TOREFREUND, TORELIGA, TOREPOKAL, TOREGESAMT, HATTRICK, BEWERTUNG, TRAINERTYP, TRAINER, PLAYERNUMBER, TRANSFERLISTED, CAPS, CAPSU20, AGEDAYS, TRAININGBLOCK, LOYALTY, HOMEGROWN)  VALUES (7, ' . "'2014-01-18 16:07:57.0'" . ", " . "0" . ", " . $id . ", " . "'". $nombre ."'" . ", " . $anos . ", " . $con . ", " . $for . ", " . $por . ", " . $def . ", " . $jug . ", " . $lat . ", " . $ano . ", " . $pas . ", " . $bp . ", " . "0.".$subpor[1]. ", " . "0.".$subdef[1]. ", " . "0.".$subjug[1]. ", " . "0.".$sublat[1]. ", " . "0.".$subano[1]. ", " . "0.".$subpas[1]. ", " . "0.".$subbp[1].	", ". "0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0" . ", " . $varEspe. ", " . "1, 1, 0" . ", ". $lid . ", " . $xp . ", " . "1, 0" . ", " . $id_pais . ", " . "0" . ", " . $lesion . ", " . "0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, " . ($dias-$difDias) . ", " . "false, 0, false) \n\n";						
 								$jugadores++;
 							}
 						}	
					}
					
						fclose($handle);  
				}				
			}
			
		} else {
			$message = '<span class="red">Solo .csv es permitido</span>';
		}
		
	} else {
		$message = '<span class="red">Hay algÃºn problema con tu archivo</span>';
	}
}
};

//exportar configuracion
if ($_POST[exportar]) {
	
	$txt_id = $_POST['txt_id'];
	$txt_name = $_POST['txt_name'];
	$txt_edad = $_POST['txt_edad'];
	$txt_con = $_POST['txt_con'];
	$txt_for = $_POST['txt_for'];
	$txt_lid = $_POST['txt_lid'];
	$txt_spe = $_POST['txt_spe'];
	$txt_xp = $_POST['txt_xp'];
	$txt_lesion = $_POST['txt_lesion'];
	$txt_dias = $_POST['txt_dias'];
	$txt_por = $_POST['txt_por'];
	$txt_def = $_POST['txt_def'];
	$txt_jug = $_POST['txt_jug'];
	$txt_lat = $_POST['txt_lat'];
	$txt_ano = $_POST['txt_ano'];
	$txt_pases = $_POST['txt_pases'];
	$txt_bp = $_POST['txt_bp'];
		
	$list = $txt_id . "," . $txt_name . "," . $txt_edad . "," . $txt_con . "," . $txt_for . "," . $txt_lid . "," . $txt_xp . "," . $txt_lesion . "," . $txt_dias . ","  . $txt_spe . "," . $txt_por . "," . $txt_def . "," . $txt_jug . "," . $txt_lat . "," . $txt_ano . "," . $txt_pases . "," . $txt_bp;
	
	$handle = fopen("myConf.csv", "w");
	fwrite($handle, $list);
	fclose($handle);
	
	header('Content-Type: application/octet-stream');
	header('Content-Disposition: attachment; filename='.basename('myConf.csv'));
	header('Expires: 0');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');
	header('Content-Length: ' . filesize('myConf.csv'));
	readfile('myConf.csv');
	exit;
};
//fin 


$txt_id = "0";
$txt_name = $_GET['t_name'];
$txt_edad = $_GET['t_edad'];
$txt_con = $_GET['t_con'];
$txt_for = $_GET['t_for'];
$txt_lid = $_GET['t_lid'];
$txt_spe = $_GET['t_spe'];
$txt_xp = $_GET['t_xp'];
$txt_lesion = $_GET['t_lesion'];
$txt_dias = $_GET['t_dias'];
$txt_por = $_GET['t_por'];
$txt_def = $_GET['t_def'];
$txt_jug = $_GET['t_jug'];
$txt_lat = $_GET['t_lat'];
$txt_ano = $_GET['t_ano'];
$txt_pases = $_GET['t_pas'];
$txt_bp = $_GET['t_bp'];

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BD for HO</title>

<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.elastic.source.js" type="text/javascript" charset="utf-8"></script>

<meta name="keywords" content="" />
<meta name="description" content="" />
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900|Quicksand:400,700|Questrial" rel="stylesheet" />
<link href="css/default_rival.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/fonts.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript">
$(document).ready(function(){

	// Con este código seleccionamos el contenido de textarea bajo petición
	$("#selectall").click( function(){
		$("#areatexto").select();
		$("#areatexto").elastic();
		return false;
	});
		
});	
</script>

<script>
    function ordenarSelect(id_componente)
    {
      var selectToSort = jQuery('#' + id_componente);
      var optionActual = selectToSort.val();
      selectToSort.html(selectToSort.children('option').sort(function (a, b) {
        return a.text === b.text ? 0 : a.text < b.text ? -1 : 1;
      })).val(optionActual);
    }
    $(document).ready(function () {
      ordenarSelect('pais');
    });
  </script>


<script src="js/jquery-1.8.2.min.js"></script>

</head>
<body>
<div id="header-wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><a href="#">HO PAÍSES</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li><a href="index.php" accesskey="1" title="">Chile</a></li>
				<li><a href="rival.php" accesskey="2" title="">Otros Países</a></li>
				<li><a href="https://docs.google.com/document/d/1yU_4rIQ5UT8-srTZb-jJM0fIJDVpyqwIB2NKEy-k-JQ/edit?usp=sharing" accesskey="3" title="">Tutorial</a></li>
			</ul>
		</div>
	</div>
</div>
<div id="page-wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
			
			<form action="rival.php" method="post" enctype="multipart/form-data">
				<h3>Elige un País:</h3>
				<select id="pais" name="pais" style="width:150px">
				  	
				  	<option value="94">Albania</option>
					<option value="1">Sweden</option>                    
					<option value="2">England</option>                  
					<option value="3">Germany</option>                
					<option value="4">Italy</option>                     
					<option value="5">France</option>                    
					<option value="6">Mexico</option>                    
					<option value="7">Argentina</option>                 
					<option value="8">USA</option>                       
					<option value="9">Norway</option>                    
					<option value="10">Denmark</option>                   
					<option value="11">Finland</option>                   
					<option value="12">Netherlands</option>               
					<option value="13">Oceania</option>                   
					<option value="22">Brazil</option>                    
					<option value="14">Canada</option>                    
					<option value="17">Chile</option>                    
					<option value="18">Colombia</option>                  
					<option value="27">India</option>                     
					<option value="16">Ireland</option>                   
					<option value="25">Japan</option>                     
					<option value="21">Peru</option>                      
					<option value="26">Poland</option>                    
					<option value="23">Portugal</option>                  
					<option value="15">Scotland</option>                  
					<option value="24">South Africa</option>              
					<option value="19">Uruguay</option>                   
					<option value="20">Venezuela</option>                 
					<option value="29">South Korea</option>               
					<option value="30">Thailand</option>                  
					<option value="31">Turkey</option>                    
					<option value="32">Egypt</option>                     
					<option value="28">People's Republic of China</option>
					<option value="34">Russia</option>                    
					<option value="35">Spain</option>                     
					<option value="36">Romania</option>                   
					<option value="37">Iceland</option>                   
					<option value="33">Austria</option>                   
					<option value="38">Belgium</option>                   
					<option value="39">Malaysia</option>                  
					<option value="40">Switzerland</option>               
					<option value="41">Singapore</option>                 
					<option value="45">Greece</option>                    
					<option value="44">Hungary</option>                   
					<option value="46">Czech Republic</option>            
					<option value="48">Latvia</option>                    
					<option value="49">Indonesia</option>                 
					<option value="50">Philippines</option>               
					<option value="47">Estonia</option>                   
					<option value="43">Serbia</option>                    
					<option value="42">Croatia</option>                   
					<option value="53">Hong Kong</option>                 
					<option value="52">Chinese Taipei</option>            
					<option value="56">Wales</option>                     
					<option value="55">Bulgaria</option>                  
					<option value="51">Israel</option>                    
					<option value="57">Slovenia</option>                  
					<option value="61">Lithuania</option>                 
					<option value="66">Slovakia</option>                  
					<option value="62">Ukraine</option>                   
					<option value="63">Bosnia and Herzegovina</option>    
					<option value="65">Vietnam</option>                   
					<option value="64">Pakistan</option>                  
					<option value="67">Paraguay</option>                  
					<option value="68">Ecuador</option>                   
					<option value="69">Bolivia</option>                   
					<option value="70">Nigeria</option>                  
					<option value="71">Faroe Islands</option>             
					<option value="72">Morocco</option>                   
					<option value="75">Saudi Arabia</option>              
					<option value="76">Tunisia</option>                   
					<option value="77">Costa Rica</option>                
					<option value="78">United Arab Emirates</option>      
					<option value="79">Luxembourg</option>                
					<option value="80">Iran</option>                      
					<option value="83">Dominican Republic</option>        
					<option value="82">Cyprus</option>                    
					<option value="87">Belarus</option>                   
					<option value="88">Northern Ireland</option>          
					<option value="89">Jamaica</option>                   
					<option value="90">Kenya</option>                     
					<option value="91">Panama</option>                    
					<option value="92">FYR Macedonia</option>                                
					<option value="95">Honduras</option>                  
					<option value="96">El Salvador</option>               
					<option value="97">Malta</option>                     
					<option value="98">Kyrgyzstan</option>                
					<option value="99">Moldova</option>                  
					<option value="100">Georgia</option>                   
					<option value="101">Andorra</option>                   
					<option value="103">Jordan</option>                    
					<option value="102">Guatemala</option>                 
					<option value="105">Trinidad & Tobago</option>         
					<option value="121">Nicaragua</option>                 
					<option value="122">Kazakhstan</option>                
					<option value="123">Suriname</option>                 
					<option value="125">Liechtenstein</option>             
					<option value="126">Algeria</option>                   
					<option value="127">Mongolia</option>                  
					<option value="128">Lebanon</option>                   
					<option value="86">Senegal</option>                   
					<option value="104">Armenia</option>                   
					<option value="129">Bahrain</option>                   
					<option value="130">Barbados</option>                  
					<option value="131">Cape Verde</option>                
					<option value="132">Côte d’Ivoire</option>             
					<option value="134">Kuwait</option>                    
					<option value="135">Iraq</option>                      
					<option value="133">Azerbaijan</option>                
					<option value="137">Angola</option>                    
					<option value="136">Montenegro</option>                
					<option value="138">Bangladesh</option>                
					<option value="139">Yemen</option>                    
					<option value="140">Oman</option>                      
					<option value="142">Mozambique</option>                
					<option value="143">Brunei</option>                    
					<option value="144">Ghana</option>                     
					<option value="145">Cambodia</option>                  
					<option value="147">Benin</option>                    
					<option value="148">Syria</option>                     
					<option value="149">Qatar</option>                     
					<option value="150">Tanzania</option>                  
					<option value="153">Uganda</option>                    
					<option value="154">Maldives</option>                  
					<option value="163">Uzbekistan</option>                
					<option value="165">Cameroon</option>                  
					<option value="93">Cuba</option>                      
					<option value="166">Palestine</option>
				  
				</select>
				
				<fieldset>
				
				<legend><input type="file" name="file" id="file" size="30" /></legend>
				<br></br>
				<table>
						<tr>
						<td>Id: </td>
						<td><input type="text" name="txt_id" size="1" value="0" class="Fields" readonly="readonly"></td>
						<td>  Nombre: </td>
						<td><input type="text" name="txt_name" size="1" value="<?php if($txt_name==null){echo "2";}else{echo $_GET['t_name'];}?>"></td>
						<td>Edad: </td>
						<td><input type="text" name="txt_edad" size="1" value="<?php if($txt_edad==null){echo "9";}else{echo $_GET['t_edad'];}?>"></td>
						<td>Condición: </td>
						<td><input type="text" name="txt_con" size="1" value="<?php if($txt_con==null){echo "16";}else{echo $_GET['t_con'];}?>"></td>
						<td>Forma: </td>
						<td><input type="text" name="txt_for" size="1" value="<?php if($txt_for==null){echo "15";}else{echo $_GET['t_for'];}?>"></td>
						<td>Liderazgo: </td>
						<td><input type="text" name="txt_lid" size="1" value="<?php if($txt_lid==null){echo "26";}else{echo $_GET['t_lid'];}?>"></td>
						<td>Experiencia: </td>
						<td><input type="text" name="txt_xp" size="1" value="<?php if($txt_xp==null){echo "17";}else{echo $_GET['t_xp'];}?>"></td>
						<td>Lesión: </td>
						<td><input type="text" name="txt_lesion" size="1" value="<?php if($txt_lesion==null){echo "27";}else{echo $_GET['t_lesion'];}?>"></td>
						<td>Edad Días: </td>
						<td><input type="text" name="txt_dias" size="1" value="<?php if($txt_dias==null){echo "10";}else{echo $_GET['t_dias'];}?>"></td>
						<td>Especialidad: </td>
						<td><input type="text" name="txt_spe" size="1" value="<?php if($txt_spe==null){echo "14";}else{echo $_GET['t_spe'];}?>"></td>
						</tr>
						<tr>
						<td>Portería: </td>
						<td><input type="text" name="txt_por" size="1" value="<?php if($txt_por==null){echo "22";}else{echo $_GET['t_por'];}?>"></td>
						<td>Defensa: </td>
						<td><input type="text" name="txt_def" size="1" value="<?php if($txt_def==null){echo "24";}else{echo $_GET['t_def'];}?>"></td>
						<td>Jugadas: </td>
						<td><input type="text" name="txt_jug" size="1" value="<?php if($txt_jug==null){echo "19";}else{echo $_GET['t_jug'];}?>"></td>
						<td>Lateral: </td>
						<td><input type="text" name="txt_lat" size="1" value="<?php if($txt_lat==null){echo "20";}else{echo $_GET['t_lat'];}?>"></td>
						<td>Anotación: </td>
						<td><input type="text" name="txt_ano" size="1" value="<?php if($txt_ano==null){echo "21";}else{echo $_GET['t_ano'];}?>"></td>
						<td>Pases: </td>
						<td><input type="text" name="txt_pases" size="1" value="<?php if($txt_pases==null){echo "23";}else{echo $_GET['t_pas'];}?>"></td>
						<td>Balon Parado: </td>
						<td><input type="text" name="txt_bp" size="1" value="<?php if($txt_bp==null){echo "25";}else{echo $_GET['t_bp'];}?>"></td>
						</tr>
					
					</table>
				
				<input type="submit" id="btn" name="cargar" class="button button-small" value="Crear Insert" /> <input type="submit" name="exportar" value="Exportar" /> <input type="button" id="importar" name="importar" value="Importar" onclick = "location ='import_rival.php'"/>
				</fieldset>
			</form>
						
			<br><button id="selectall">Seleccionar todo</button></br>
			<textarea id="areatexto" cols="168" rows="30">
			<?php
				echo $message;		
				for ($a=0; $a < sizeof($insert); $a++ ){
					echo $insert[$a]; 
				}
			?>
			</textarea>				
			</div>
			
		</div>
		
	</div>
</div>

</body>
</html>
