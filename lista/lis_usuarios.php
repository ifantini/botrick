<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$nacion = $_SESSION[ID_PAIS];
	$consultar = pg_query($con,"select usuario.nombre,perfil.pais from usuario,perfil where usuario.id_usuario = '$user' and usuario.id_perfil = perfil.id_perfil");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		
		if($permisos < 2){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Lista']){
	$_SESSION['ID_LISTA'] = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
}
if($_POST['Enviar']){
	$id_lista = $_SESSION['ID_LISTA'];
	foreach ($_POST['checkbox'] as $id) {
		$saber = pg_query($con,"select * from listado_usuario where id_lista = $id_lista and id_usuario =".(is_numeric($id)?$id:0));
		if(!$conocer = pg_fetch_array($saber)){
				$sentencia = pg_query($con,"insert into listado_usuario values($id_lista,$id)");
				header("Location:usuarios_lis.php?ID=$id_lista");
		}
    }
}
$id_listado = $_SESSION['ID_LISTA'];
$titulo = "agregar scouts";
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />			
			<div>
				<?php include_once("menu_listas.php");?> 
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Seleccionar Usuarios"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["lis_usuarios text1"][$_SESSION[IDIOMA]];?></br></br></p>
        <h3></br></br><?php echo $text["Listado de usuarios"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <div class="block">
        <form action="lis_usuarios.php" method="post">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Permisos"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Seleccionar"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia = "select usuario.id_usuario,usuario.nombre,perfil.nombre as cargo from usuario,perfil where usuario.id_perfil = perfil.id_perfil and perfil.pais = '$nacion' and usuario.id_usuario not in (select id_usuario from listado_usuario where id_lista=".$_SESSION['ID_LISTA'].") order by permisos desc";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['id_usuario'];?></th>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo $rs['cargo'];?></td>
            <td><input name="checkbox[]" type="checkbox" value="<?php echo $rs['id_usuario'];?>"></td>
          </tr>
          <?
		  }
		  ?>
          <tr>
          	<td colspan="4" align="center"><input name="Enviar" type="submit" id="Enviar" value="<?php echo $text["Enviar"][$_SESSION[IDIOMA]];?>"></td>
          </tr>
        </table>
        </form>
        </div>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>