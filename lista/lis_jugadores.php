<?php
require_once("../conexion.php"); 
session_start();
$HT = $_SESSION['HT'];
$RegistrosAMostrar=30;
$paginar = FALSE;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
	$paginar = TRUE;
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$nacion = $_SESSION[ID_PAIS];
	$consultar = pg_query($con,"select usuario.nombre,perfil.pais from usuario,perfil where usuario.id_usuario = '$user' and usuario.id_perfil = perfil.id_perfil");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 1){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Lista']){
	$_SESSION['ID_LISTA'] = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
}
if($_POST['Enviar']){
	$id_lista = $_SESSION['ID_LISTA'];
	foreach ($_POST['checkbox'] as $id) {
		$saber = pg_query($con,"select * from listadojugador where id_lista = '$id_lista' and id_jugador = ".(is_numeric($id)?$id:0));
		if(!$conocer = pg_fetch_array($saber)){
	        $sentencia = pg_query($con,"insert into listadojugador values($id_lista,$id)");
			header("Location:usuarios_lis.php?ID=$id_lista");
		}
    }
}
if($_GET['Desplegar']=='Desplegar'){
	//if que recibe la info del formulario
	$pos = is_numeric($_GET['cbo_pos'])?$_GET['cbo_pos']:0;
	$ord = is_numeric($_GET['cbo_ord'])?$_GET['cbo_ord']:0;
	$edad_min = is_numeric($_GET['cbo_min'])?$_GET['cbo_min']:0;
	$edad_max = is_numeric($_GET['cbo_max'])?$_GET['cbo_max']:0;
	$esp = is_numeric($_GET['cbo_esp'])?$_GET['cbo_esp']:0;
	$orden = str_replace(';', '',pg_escape_string($_GET['cbo_por']));//variable que almacena si es ordenado por APORTE o por TTI
	$paginar = TRUE;
}
if($_POST['Desplegar']=='Desplegar'){
	//if que recibe la info del formulario
	$pos = is_numeric($_POST['cbo_pos'])?$_POST['cbo_pos']:0;
	$ord = is_numeric($_POST['cbo_ord'])?$_POST['cbo_ord']:0;
	$edad_min = is_numeric($_POST['cbo_min'])?$_POST['cbo_min']:0;
	$edad_max = is_numeric($_POST['cbo_max'])?$_POST['cbo_max']:0;
	$esp = is_numeric($_POST['cbo_esp'])?$_POST['cbo_esp']:0;
	$orden = str_replace(';', '',pg_escape_string($_POST['cbo_por']));//variable que almacena si es ordenado por APORTE o por TTI
	$paginar = TRUE;
}
$id_listado = $_SESSION['ID_LISTA'];
$titulo = "agregar jugadores";
require_once("../nombres.php");
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" />
			<div style="margin-bottom: 30px;">
			<?php include_once("menu_listas.php");?> 
			</div>
			<div>
            <p><?php echo $text["lis_jugadores text1"][$_SESSION[IDIOMA]];?>.<br><br></p>
                <form action="lis_jugadores.php" method="post">
                <table cellspacing="2">
                  <tr>
                    <td><?php echo $text["Posicion"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_pos" size="1" id="cbo_pos" style="width: 100%">
                    <?php 
                    $sentencia = pg_query($con,"select id_aporte,nombre from aporte where id_listado_aportes=1;");
					while($rs = pg_fetch_array($sentencia)){?>
                     <option value="<?php echo $rs['id_aporte'];  ?>" <?php if($pos==$rs['id_aporte']){echo " selected";}?>  ><?php echo $rs['nombre']; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Edad Minima"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_min" size="1" id="cbo_min" style="width: 100%">
                    <option value="17" selected>17</option>
                    <?php 
                    for ($i = 18; $i <= 35; $i++) {
                    ?>
                    <option value=<?php echo '"'.$i.'"'; if($edad_min==$i){echo " selected";}  ?>><?php echo $i; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Edad Maxima"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_max" size="1" id="cbo_max" style="width: 100%">
                    <option value="35" selected>35</option>
                    <?php 
                    for ($i = 34; $i >= 17; $i--) {
                    ?>
                    <option value=<?php echo '"'.$i.'"'; if($edad_max==$i){echo " selected";}  ?>><?php echo $i; ?> </option><?php } ?>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Especialidad"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_esp" size="1" id="cbo_esp" style="width: 100%">
                    <option value="-1" selected><?php echo $text["Cualquiera"][$_SESSION[IDIOMA]];?></option>
					<option value="0"<?php if($esp=='0'){echo " selected";} ?>><?php echo denominacion(0,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="1"<?php if($esp==1){echo " selected";} ?>><?php echo denominacion(1,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="2"<?php if($esp==2){echo " selected";} ?>><?php echo denominacion(2,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="3"<?php if($esp==3){echo " selected";} ?>><?php echo denominacion(3,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="4"<?php if($esp==4){echo " selected";} ?>><?php echo denominacion(4,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="5"<?php if($esp==5){echo " selected";} ?>><?php echo denominacion(5,$_SESSION[IDIOMA],'especialidad',$con);?></option>
					<option value="6"<?php if($esp==6){echo " selected";} ?>><?php echo denominacion(6,$_SESSION[IDIOMA],'especialidad',$con);?></option>
                    </select></td>
                  </tr>
                  <tr>
                    <td><?php echo $text["Ordenar por"][$_SESSION[IDIOMA]];?>:</td>
                    <td><select name="cbo_por" size="1" id="cbo_por" style="width: 100%">
                    <!--<option value="tti"<?php if($orden=="tti"){echo " selected";} ?>>tti</option>-->
					<!--<option value="potencial"<?php if($orden=="potencial"){echo " selected";} ?>>Potencial</option>-->
					<option value="aporte"<?php if($orden=="aporte"){echo " selected";} ?>><?php echo $text["Aporte"][$_SESSION[IDIOMA]];?></option>
                    </select></td>
                  </tr>
                  <tr>
                  	<td></td>
                    <td align="center" colspan="2"><input name="Desplegar" type="submit" id="Desplegar" value="Desplegar" style="width: 100%"></td>
                  </tr>
                </table>
                </form>
                </p> 
            </div>  
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
			
		</div>
        <div class="list">
        
        <h3><?php echo $text["Seleccionar Jugadores"][$_SESSION[IDIOMA]];?></h3>
        <p>
        	<?php echo $text["lis_jugadores text2"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <h3><?php echo $text["Consultar por un jugador"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["lis_jugadores text3"][$_SESSION[IDIOMA]];?><br></p>
        <div class="list">
        <form action="lis_jugadores.php" method="post">
        <table cellspacing="5" width="300">
          <tr>
            <td>ID <?php echo $text["jugador"][$_SESSION[IDIOMA]];?>:</td>
            <td><input name="txt_id_jugador" type="text" id="txt_id_jugador"></td>
          </tr>
          <tr>
            <td align="center" colspan="2"><input name="Consultar" type="submit" id="Consultar" value="Consultar"></td>
          </tr>
        </table>
        </form>
        <?php
			if($_POST['Consultar']){
				$id_j = $_POST['txt_id_jugador'];
				$buscar = pg_query($con,"select * from jugador where id_jugador = '$id_j' and activo = TRUE and pais = '$nacion'");
				if($tener = pg_fetch_array($buscar)){				
		?>
		 <p><br><table border="0" cellpadding="0" cellspacing="0" class="tabla2">
              <tr>
                <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
                <th><?php echo $text["Agregar"][$_SESSION[IDIOMA]];?></th>
              </tr>
              <tr class="modo1">
                <th><?php echo $tener['nombre'];?></th>
				<td><img src="/images/flags/<?php echo $tener['pais'];?>flag.png"></td>
                <td><a href="/lista/usuarios_lis.php?Agregar=<?php echo $id_j;?>&ID_Lista=<?php echo $_SESSION['ID_LISTA'];?>"><img src="/images/icons/user_add16_h.png"></a></td>
              </tr>
         </table></p>
		<?php
				}else{
		?>
 		</div>
        <table width="680" align="center"><tr><td><div class="Estilo1"><?php echo $text["lis_jugadores text4"][$_SESSION[IDIOMA]];?>.</div></td></tr></table>
        <div>
        <?php
				}
			}
		?>
        </div>
        <h3></br></br><?php echo $text["Listado de jugadores"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <div class="block">
        <form action="lis_jugadores.php" method="post">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla">
              <tr>
                <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
                <th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
                <th><?php echo $text["EE"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
				<th><img src="/images/icons/2card.png"></th>
				<th><img src="/images/icons/1injury.png"></th>
				<th><?php echo $text["Fo"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Co"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ju"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["De"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pa"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["La"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["An"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Po"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["BP"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ex"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Li"][$_SESSION[IDIOMA]];?></th>
				<th>TTI</th>
				<th><?php echo $text["Pot"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Aporte"][$_SESSION[IDIOMA]];?></th>
                <th><img src="../images/favicon.ico"></th>
                <th><img src="../images/icons/check_box.png"></th>
              </tr>
          <?php
		  $valor = FALSE;
		  if($_POST['Desplegar'] || $_GET['Desplegar']){//if que recibe la info del formulario
			if($nacion != 0 ){
				$sentencia = pg_query($con,"select * from top_jugadores($pos,$edad_min,$edad_max,$esp) where pais = $nacion order by $orden desc offset $RegistrosAEmpezar limit $RegistrosAMostrar;");
			}else{
				$sentencia = pg_query($con,"select * from top_jugadores($pos,$edad_min,$edad_max,$esp) order by $orden desc offset $RegistrosAEmpezar limit $RegistrosAMostrar;");
			}
			while($rsteam = pg_fetch_array($sentencia)){
					$valor = TRUE;
		  ?>
              <tr class="modo1">
                <th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>"><?php echo $rsteam['nombre'];?></th>
                <td><?php echo floor($rsteam['dias']/112).'.'.($rsteam['dias']%112);?></td>
                <td><?php if($rsteam['especialidad']>0){echo '<img src="/images/icons/spec'.$rsteam['especialidad'].'.png">';}?></td>
				<td><img src="/images/flags/<?php echo $rsteam['pais'];?>flag.png"></td>
				<td><?php if($rsteam['tarjetas']>0){echo '<img src="/images/icons/'.$rsteam['tarjetas'].'card.png">';}?></td>
				<td><?php if($rsteam['lesion']>0){echo '<img src="/images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="/images/icons/0injury.png">';}?></td>
				<td><?php echo $rsteam['forma'];?></td>
				<td><?php echo $rsteam['condicion'];?></td>
				<td><?php echo $rsteam['jugadas'];?></td>
				<td><?php echo $rsteam['defensa'];?></td>
				<td><?php echo $rsteam['asistencias'];?></td>
				<td><?php echo $rsteam['lateral'];?></td>
				<td><?php echo $rsteam['anotacion'];?></td>
				<td><?php echo $rsteam['porteria'];?></td>
				<td><?php echo $rsteam['balonparado'];?></td>
				<td><?php echo $rsteam['experiencia'];?></td>
				<td><?php echo $rsteam['liderazgo'];?></td>
				<td><?php echo $rsteam['tti'];?></td>
				<td><?php echo number_format ( 100*$rsteam['potencial'] , 1 ,  '.' ,',' )."%";?></td>
				<td><?php echo number_format ( $rsteam['aporte'] ,0,  '.' ,',' );?></td> 
                <td><?php if($rsteam['listas'] >= 1){?><img src="../images/favicon.ico"><?php }else{?>&nbsp;<?php }?></td> 
                <td><input name="checkbox[]" type="checkbox" value="<?php echo $rsteam['id_jugador'];?>"></td>
              </tr>
          <?php
			}//end while
		  }//end if
		  ?>
            <tr>
          		<td colspan="21" align="center"><input name="Enviar" type="submit" id="Enviar" value="<?php echo $text["Enviar"][$_SESSION[IDIOMA]];?>"></td>
          	</tr>
          <?php
		  if($valor == FALSE){
		  ?>
          <tr>
          	<td colspan="21" align="center"><?php echo $text["lis_jugadores text5"][$_SESSION[IDIOMA]];?>.</td>
          </tr>
          <?php
		  }
		  ?>
        </table>
        </form>
        <table><tr>
        <?php
		if($paginar == TRUE){
$NroRegistros=pg_num_rows(pg_query($con,"select * from top_jugadores($pos,$edad_min,$edad_max,$esp) where pais = $nacion"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;
 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='lis_jugadores.php?pag=1&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='lis_jugadores.php?pag=$PagAnt&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='lis_jugadores.php?pag=$PagSig&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='lis_jugadores.php?pag=$PagUlt&cbo_pos=$pos&cbo_min=$edad_min&cbo_max=$edad_max&cbo_esp=$esp&cbo_por=$orden&Desplegar=Desplegar'><img src='/images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
}
  ?>
  		</tr></table>
                </div>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>