<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 1){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['ID']){
	$id_lista = is_numeric($_GET['ID'])?$_GET['ID']:0;
	$_SESSION['ID_LISTA'] = $id_lista;
	$sentencia = pg_query($con,"select nombre from listado where id_lista = $id_lista");
	if($ejecutar = pg_fetch_array($sentencia)){
		$nom_lista = $ejecutar['nombre'];	
	}else{
		header('Location:../error.php');
	}
}
$titulo = "monitoreo granjas";
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>
				<?php include_once("menu_listas.php");?>
            <p>&nbsp;<br><br><p class="Estilo1"><strong>- <?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></strong></p><?php echo $text["usuarios_lis text1"][$_SESSION[IDIOMA]];?>.<br><br><p class="Estilo1"><strong>- <?php echo $text["Scout"][$_SESSION[IDIOMA]];?></strong></p><p><?php echo $text["usuarios_lis text2"][$_SESSION[IDIOMA]];?>.</p> 
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["equipos_lis text4"][$_SESSION[IDIOMA]];?>: <?php echo $nom_lista;?></h3>
        <p>
        	<?php echo $text["equipos_lis text5"][$_SESSION[IDIOMA]];?>
        </p>
        </div>
        <div class="list">
        <?php
			$saber_categorias = pg_query($con,"select planes_entrenamiento.nombre from planes_entrenamiento,listadojugador,jugador where listadojugador.id_lista = $id_lista and listadojugador.id_jugador = jugador.id_jugador and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento and jugador.id_planes_entrenamiento is not null group by planes_entrenamiento.nombre");
			while($tener_categorias = pg_fetch_array($saber_categorias)){
		?>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
          	<th colspan="4" align="center"><?php echo $nombre_categoria = $tener_categorias['nombre'];?></th>
          </tr>
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?> del Equipo</th>
            <th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>           
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia = "select jugador.id_jugador,jugador.nombre as nom1,equipo.nombre as nom2,equipo.pais,planes_entrenamiento.nick from jugador,equipo,planes_entrenamiento,listadojugador where listadojugador.id_lista = $id_lista and listadojugador.id_jugador = jugador.id_jugador and jugador.id_equipo = equipo.id_equipo and planes_entrenamiento.nombre = '$nombre_categoria' and planes_entrenamiento.id_planes_entrenamiento = jugador.id_planes_entrenamiento";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['nom2'];?></th>
            <th><?php if($rs['pais'] == 18){ echo "Chile";}else{?><div class="Estilo1"><?php echo "Extranjero"; ?></div><?php }?></th>
            <td><a href="../datos_jugador.php?id=<?php echo $rs['id_jugador'];?>" target="_blank"><?php echo $rs['nom1'];?></a></td>           
            <td><?php echo $rs['nick'];?></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        <img src="../images/fondo_blanco_hor.jpg">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th colspan="3" align="center">Recuento de <?php echo $nombre_categoria;?></th>
        </tr>
        <tr>
        	<th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?> del Equipo</th>
            <th>Jugadores</th>
        </tr>
        <?php
			$rec_cat = pg_query($con,"select equipo.id_equipo,equipo.nombre,count(jugador.id_jugador) as jugadores from equipo,jugador,listadojugador,planes_entrenamiento where listadojugador.id_lista = $id_lista and planes_entrenamiento.nombre = '$nombre_categoria' and listadojugador.id_jugador = jugador.id_jugador and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento and jugador.id_equipo = equipo.id_equipo group by equipo.id_equipo,equipo.nombre order by equipo.nombre asc");
			while($ten_cat = pg_fetch_array($rec_cat)){
		?>
        <tr class="modo1">
        	<th><?php echo $ten_cat['id_equipo'];?></th>
            <td><?php echo $ten_cat['nombre'];?></td>
            <td><?php echo $ten_cat['jugadores'];?></td>
        </tr>
        <?php
			}
		?>
        </table>
        <img src="../images/fondo_blanco_hor.jpg">        
        <?php
		}
		?>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
          	<th colspan="3" align="center">Jugadores Sin Categoría</th>
          </tr>
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?> del Equipo</th>
            <th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
          </tr>
        <?php
		$conocer_categoria = pg_query($con,"select jugador.id_jugador,equipo.nombre as nom1,jugador.nombre as nom2,equipo.pais from jugador,listadojugador,equipo where listadojugador.id_lista = $id_lista and listadojugador.id_jugador = jugador.id_jugador and equipo.id_equipo = jugador.id_equipo and jugador.id_planes_entrenamiento is null");
		while($recibir_categoria = pg_fetch_array($conocer_categoria)){
		?>
          <tr class="modo1">
          	<th><?php echo $recibir_categoria['nom1'];?></th>
            <th><?php if($rs['pais'] == 18){ echo "Chile";}else{?><div class="Estilo1"><?php echo "Extranjero"; ?></div><?php }?></th>
            <td><a href="../datos_jugador.php?id=<?php echo $rs['id_jugador'];?>" target="_blank"><?php echo $recibir_categoria['nom2'];?></a></td>
          </tr>
        <?php
		}
		?>
        </table>
        <img src="../images/fondo_blanco_hor.jpg">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th colspan="3" align="center">Recuento de Jugadores Sin Categoría</th>
        </tr>
        <tr>
        	<th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?> del Equipo</th>
            <th>Jugadores</th>
        </tr>
        <?php
		$rec_cat = pg_query($con,"select equipo.id_equipo,equipo.nombre,count(jugador.id_jugador) as jugadores from equipo,jugador,listadojugador where listadojugador.id_lista = $id_lista and listadojugador.id_jugador = jugador.id_jugador and jugador.id_equipo = equipo.id_equipo  and jugador.id_planes_entrenamiento is null group by equipo.id_equipo,equipo.nombre order by equipo.nombre asc");
			while($ten_cat = pg_fetch_array($rec_cat)){
		?>
       	 <tr class="modo1">
        	<th><?php echo $ten_cat['id_equipo'];?></th>
            <td><?php echo $ten_cat['nombre'];?></td>
            <td><?php echo $ten_cat['jugadores'];?></td>
        </tr>
        <?php
			}
		?>
        </table>
        </div>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>