<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = $user");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 1){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
$titulo = "monitoreo";
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/titleadm.gif" alt="" width="209" height="30" />
				<p><strong><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?>: <?php echo $nombre;?></strong>.
                <br><br><?php echo $text["seccion modificar listas"][$_SESSION[IDIOMA]];?>
                <br><br>             
                <a href="/listas_scouts.php"><?php echo $text["Listas"][$_SESSION[IDIOMA]];?></a>                
				</p>
	  	  </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Listas a las que perteneces"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["listas_scouts text1"][$_SESSION[IDIOMA]];?></p>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></th> 
            <th>+ <?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alertas"][$_SESSION[IDIOMA]];?></th>                      
            <th><?php echo $text["Ver Lista"][$_SESSION[IDIOMA]];?></th>                        
          </tr>
          <?php
		  $sentencia = "select * from listado_usuario where id_usuario = '$user' order by id_lista asc";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
		  	$id_listado = $rs['id_lista'];
			$encontrar = pg_query($con,"select id_usuario,nombre from listado where id_lista = $id_listado");
			if($hayado = pg_fetch_array($encontrar)){
				$id_coor = $hayado['id_usuario'];
				$coordinador = pg_query($con,"select nombre from usuario where id_usuario = $id_coor");
					if($pillado = pg_fetch_array($coordinador)){
          ?>
          <tr class="modo1">
            <th><?php echo $hayado['nombre'];?></th>
            <td><?php echo $pillado['nombre'];?></td> 
            <td><a href="/lista/lis_jugadores.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/user_add16_h.png"></a></td>          
			<td><a href="/lista/alertas.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/advertising.png"></a></td>                        
            <td><a href="/lista/usuarios_lis.php?ID=<?php echo $rs['id_lista'];?>"><img src="/images/icons/print_preview16_h.ico"></a></td>
          </tr>
          <?
					}
				}
		  }
		  ?>
        </table>
        </div>
        </div>
        </div>
        <?php include_once('footer.php');?>
    </body>
</html>