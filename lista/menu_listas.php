<img src="/images/titleadm.gif" alt="" width="209" height="30" />
	<p><?php echo $text["Seccion para administrar listas en Botrick"][$_SESSION[IDIOMA]];?>.<br><br></p>
	<ul id="leftnav">
		<li><h3><?php echo $text["Monitoreo"][$_SESSION[IDIOMA]];?></h3>
			<ul>
				<li><a href="/lista/listas.php"><?php echo $text["Vista general"][$_SESSION[IDIOMA]];?></a></li>
				<?php if ($permisos>1){?>
				<li><a href="/lista/granjas.php"><?php echo $text["Granjas"][$_SESSION[IDIOMA]];?></a></li>
				<?php } if($permisos>2){?>
                <li><a href="/lista/info_dt.php"><?php echo $text["Datos DT"][$_SESSION[IDIOMA]];?></a></li>
                <?php }?>
			</ul>
		</li>
		<?php
			$sentencia = "select distinct b.id_lista,b.nombre from listado_usuario a left join listado b on a.id_lista=b.id_lista where a.id_usuario=$user";
			$consultar = pg_query($con,$sentencia);
			while($rs = pg_fetch_array($consultar)){
		?>
		<li><h3><?php echo $rs['nombre'];?></h3>
			<ul>
				<?php if ($permisos>1){?>
				<li><a href="/lista/lis_usuarios.php?Lista=<?php echo $rs['id_lista'];?>"><?php echo $text["Agregar scouts"][$_SESSION[IDIOMA]];?></a></li>
	            <?php }?>
	            <li><a href="/lista/lis_jugadores.php?Lista=<?php echo $rs['id_lista'];?>"><?php echo $text["Agregar jugadores"][$_SESSION[IDIOMA]];?></a></li>
	            <li><a href="/lista/alertas.php?Lista=<?php echo $rs['id_lista'];?>"><?php echo $text["Alertas"][$_SESSION[IDIOMA]];?></a></li>           
	            <li><a href="/lista/usuarios_lis.php?ID=<?php echo $rs['id_lista'];?>"><?php echo $text["Ver jugadores"][$_SESSION[IDIOMA]];?></a></li>
                <li><a href="/lista/equipos_lis.php?ID=<?php echo $rs['id_lista'];?>"><?php echo $text["Ver equipos"][$_SESSION[IDIOMA]];?></a></li>
			</ul>
		</li>
		<?php 
			 }
		?>
	</ul>

   