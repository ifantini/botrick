<?php
require_once("../conexion.php"); 
session_start();
$existe_jugador = FALSE;
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 1){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Agregar'] && $_GET['ID_Lista']){
	$id_player = is_numeric($_GET['Agregar'])?$_GET['Agregar']:0;
	$idlista = is_numeric($_GET['ID_Lista'])?$_GET['ID_Lista']:0;
	$revisar = pg_query($con,"select * from listadojugador where id_lista = $idlista and id_jugador = $id_player");
	if(!$comprobar = pg_fetch_array($revisar)){
		$insertar = pg_query($con,"insert into listadojugador values($idlista,$id_player)");
	}else{
		$existe_jugador = TRUE;
	}
}
if($_GET['ID']){
	$id_lista = is_numeric($_GET['ID'])?$_GET['ID']:0;
	$_SESSION['ID_LISTA'] = $id_lista;
	$sentencia = pg_query($con,"select nombre from listado where id_lista = $id_lista");
	if($ejecutar = pg_fetch_array($sentencia)){
		$nom_lista = $ejecutar['nombre'];	
	}else{
		header('Location:../error.php');
	}
}elseif(isset ($_SESSION['ID_LISTA'])){
	$id_lista = $_SESSION['ID_LISTA'];
	$sentencia = pg_query($con,"select nombre from listado where id_lista = $id_lista");
	if($ejecutar = pg_fetch_array($sentencia)){
		$nom_lista = $ejecutar['nombre'];
	}
}elseif(isset ($_POST['Mostrar'])){
	$id_lista = $_SESSION['ID_LISTA'];
	$sentencia = pg_query($con,"select nombre from listado where id_lista = $id_lista");
	if($ejecutar = pg_fetch_array($sentencia)){
		$nom_lista = $ejecutar['nombre'];
	}
}
$saber_owner = pg_query($con,"select id_usuario from listado where id_lista = $id_lista");
if($tener_owner = pg_fetch_array($saber_owner)){
	$dueño_lista = $tener_owner['id_usuario'];
}
if($_GET['Eliminar']){
	$id_usu_eli = is_numeric($_GET['Eliminar'])?$_GET['Eliminar']:0;
	$id_lis_eli = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
	$realizar = pg_query($con,"delete from listado_usuario where id_usuario = '$id_usu_eli' and id_lista = '$id_lis_eli'");
}
if($_GET['Jugador']){
	$id_jug_eli = is_numeric($_GET['Jugador'])?$_GET['Jugador']:0;
	$id_lis_eli = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
	$realizar = pg_query($con,"delete from listadojugador where id_jugador = '$id_jug_eli' and id_lista = '$id_lis_eli'");
}
$titulo = "lista";
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>
				<?php include_once("menu_listas.php");?>
            <p>&nbsp;<br><br><p class="Estilo1"><strong>- <?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></strong></p><?php echo $text["usuarios_lis text1"][$_SESSION[IDIOMA]];?>.<br><br><p class="Estilo1"><strong>- <?php echo $text["Scout"][$_SESSION[IDIOMA]];?></strong></p><p><?php echo $text["usuarios_lis text2"][$_SESSION[IDIOMA]];?>.</p> 
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <?php
        if($existe_jugador == TRUE){
		?>
        <h3><?php echo $text["Validacion"][$_SESSION[IDIOMA]];?></h3>
        <p class="Estilo1"><?php echo $text["usuarios_lis text3"][$_SESSION[IDIOMA]];?>.<br><br></p>
        <?
        }
		?>
        <h3><?php echo $text["usuarios_lis text4"][$_SESSION[IDIOMA]];?>: <?php echo $nom_lista;?></h3>
        <p>
        	<?php echo $text["usuarios_lis text5"][$_SESSION[IDIOMA]];?>.
        </p>
        </div>
        <div class="list">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?></th>           
            <th><?php echo $text["Cargo"][$_SESSION[IDIOMA]];?></th>
            <?php if($permisos == 4 || $user == $dueño_lista){?><th>Eliminar</th><?php }?>
          </tr>
          <?php
		  $sentencia = "select a.nombre nomlis,c.nombre dueno,c.id_usuario id_dueno,d.nombre,d.id_usuario,e.nombre nomper from listado a left join listado_usuario b on a.id_lista=b.id_lista left join usuario c on a.id_usuario=c.id_usuario left join usuario d on b.id_usuario=d.id_usuario left join perfil e on d.id_perfil=e.id_perfil where a.id_lista = $id_lista";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
				$usu = $rs['id_usuario'];
				$owner = $rs['id_dueno'];
				$car = $rs['id_perfil'];
          ?>
          <tr class="modo1">
            <th><?php echo $rs['nomlis'];?></th>
            <td><?php echo $rs['nombre'];?></td>           
            <td><?php echo $rs['nomper'];?></td>
            <?php if($permisos == 4 || $user == $dueño_lista){
			if($usu == $owner){
			?>
            <td><?php echo $text["usuarios_lis text6"][$_SESSION[IDIOMA]];?></td>
			<?php }else{ ?>
            <td><a href="/lista/usuarios_lis.php?Eliminar=<?php echo $rs['id_usuario'];?>&Lista=<?php echo $id_lista;?>"><img src="/images/icons/delete_x16_h.ico"></a></td>
            <?php } }?>
          </tr>
          <?
		  }
		  ?>
        </table>
        </div>
        <div class="list">
      <?php $can_jugadores = pg_query($con,"select count(id_jugador) from listadojugador where id_lista = $id_lista");
		if($rs_jug = pg_fetch_array($can_jugadores)){
			$conteo = $rs_jug[0];
		}?>
        <h3><br><br><?php echo $text["usuarios_lis text7"][$_SESSION[IDIOMA]];?>: <?php echo $nom_lista;?> / <?php echo $text["usuarios_lis text8"][$_SESSION[IDIOMA]];?> = <?php echo $conteo;?></h3>        
        <p>
        	<?php echo $text["usuarios_lis text9"][$_SESSION[IDIOMA]];?>.<br>
        </p>
        <div class="list">
        <form action="usuarios_lis.php" method="post">
        <table width="300">
        <tr>
        	<td>Categoría:</td>
            <td><select name="cbo_cat" size="1" id="cbo_cat">   
                <option value="0" selected><?php echo $text["Todas"][$_SESSION[IDIOMA]];?></option>                     
                  <?php
				  $ejecutar = pg_query($con,"select planes_entrenamiento.id_planes_entrenamiento,planes_entrenamiento.nombre from planes_entrenamiento,listadojugador,jugador where listadojugador.id_lista = $id_lista and listadojugador.id_jugador = jugador.id_jugador and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento group by planes_entrenamiento.nombre,planes_entrenamiento.id_planes_entrenamiento order by planes_entrenamiento.id_planes_entrenamiento asc");
				  while($rs = pg_fetch_array($ejecutar)){
				  echo "<option value=\"".$rs['id_planes_entrenamiento']."\">".$rs['nombre']."</option>";
				  }
				  ?>  
                </select></td>
        </tr>
        <tr>
        	<td colspan="2" align="center"><input name="Mostrar" type="submit" id="Mostrar" value="Mostrar"></td>
        </tr>        
        </table>
        </form>
        </div>
        <div class="list">&nbsp;</div>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="sortable tabla">
          		<tr>
          		<th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
              	<th><img src="/images/icons/onsale.png"></th>                 
          		<th><?php echo $text["Edad"][$_SESSION[IDIOMA]];?></th>
          		<th><?php echo $text["EE"][$_SESSION[IDIOMA]];?></th>
		  		<th><?php echo $text["Pais"][$_SESSION[IDIOMA]];?></th>
				<th><img src="/images/icons/2card.png"></th>
				<th><img src="/images/icons/1injury.png"></th>
				<th><?php echo $text["Fo"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Co"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ju"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["De"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Pa"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["La"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["An"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Po"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["BP"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Ex"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Li"][$_SESSION[IDIOMA]];?></th>
				<th>TTI</th>
				<th><?php echo $text["Pot"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["Cat"][$_SESSION[IDIOMA]];?></th>                
            	<?php if($permisos >= 1){?><th><img src="/images/icons/delete_x16_h.ico"></th><?php }?>
          </tr>
          <?php
		  if($_POST['Mostrar']){
		      $id_plan_entreno = $_POST['cbo_cat'];
			  if($id_plan_entreno == 0){
			  	$sentencia = "select * from jugadores_lista($id_lista)";
			  }else{
				$sentencia = "select * from jugadores_lista($id_lista) where id_planes_entrenamiento = $id_plan_entreno";
			  }
		  }else{
			  $sentencia = "select * from jugadores_lista($id_lista)";		  
		  }
		  $consultar = pg_query($con,$sentencia);
		  while($rsteam = pg_fetch_array($consultar)){
			$id_mono = $rsteam['id_jugador'];
			$con_cat = pg_query($con,"select id_planes_entrenamiento from jugador where id_jugador = $id_mono");
			if($sab_cat = pg_fetch_array($con_cat)){
				$id_categoria = $sab_cat['id_planes_entrenamiento'];
				if(empty($id_categoria) == 1){
					$categoria = "Vacio";
				}else{
					$con_cat = pg_query($con,"select nick from planes_entrenamiento where id_planes_entrenamiento = $id_categoria");
					if($ten_cat = pg_fetch_array($con_cat)){$categoria = $ten_cat['nick'];}
				}
			}
			$cons1 = pg_query($con,"select nombre from listado where id_lista = '$id_lista'");
			if($ejecutar1 = pg_fetch_array($cons1)){
          ?>
          <tr class="modo1">
				<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo $rsteam['nombre'];?></a></th>
				<td><?php if ($rsteam['en_venta']=="t"){?><img src="/images/icons/onsale.png"><?php }else{?>&nbsp;<?php }?>                
                <td sorttable_customkey="<?php echo $rsteam['dias'];?>"><?php echo floor($rsteam['dias']/112).'.'.($rsteam['dias']%112);?></td>
                <td sorttable_customkey="<?php echo $rsteam['especialidad'];?>"><?php if($rsteam['especialidad']>0){echo '<img src="/images/icons/spec'.$rsteam['especialidad'].'.png">';}?></td>
				<td sorttable_customkey="<?php echo $rsteam['pais'];?>"><img src="/images/flags/<?php echo $rsteam['pais'];?>flag.png"></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tarjetas'];?>"><?php if($rsteam['tarjetas']>0){echo '<img src="/images/icons/'.$rsteam['tarjetas'].'card.png">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lesion'];?>"><?php if($rsteam['lesion']>0){echo '<img src="/images/icons/1injury.png">'.$rsteam['lesion'];} if($rsteam['lesion']==0){echo '<img src="/images/icons/0injury.png">';}?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['forma'];?>"><?php echo $rsteam['forma'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['condicion'];?>"><?php echo $rsteam['condicion'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['jugadas'];?>"><?php echo $rsteam['jugadas'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['defensa'];?>"><?php echo $rsteam['defensa'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['asistencias'];?>"><?php echo $rsteam['asistencias'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['lateral'];?>"><?php echo $rsteam['lateral'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['anotacion'];?>"><?php echo $rsteam['anotacion'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['porteria'];?>"><?php echo $rsteam['porteria'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['balonparado'];?>"><?php echo $rsteam['balonparado'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['experiencia'];?>"><?php echo $rsteam['experiencia'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['liderazgo'];?>"><?php echo $rsteam['liderazgo'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['tti'];?>"><?php echo $rsteam['tti'];?></td>
				<td sorttable_customkey="<?php echo -1*$rsteam['potencial'];?>"><?php echo number_format ( 100*$rsteam['potencial'] , 1 ,  '.' ,',' )."%";?></td>
                <?php if($permisos > 1){?>
                <td sorttable_customkey="<?php echo $categoria;?>"><?php if($categoria == "Vacio"){?><a href="/datos_jugador.php?Jugador=<?php echo $rsteam['id_jugador'];?>&Lista=<?php echo $id_lista;?>"><img src="/images/icons/add.png"></a><?php }else{ echo $categoria; }?></td>
                <?php }else{?>
                <td sorttable_customkey="<?php echo $categoria;?>"><?php if($categoria == "Vacio"){?>S/C<?php }else{ echo $categoria; }?></td>
                <?php } if($permisos >= 1){?><td><a href="/lista/usuarios_lis.php?Jugador=<?php echo $rsteam['id_jugador'];?>&Lista=<?php echo $id_lista;?>"><img src="/images/icons/delete_x16_h.ico"></a></td><?php }?>
          </tr>
          <?	
		  	}
		  }
		  ?>
        </table>
        </div>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>