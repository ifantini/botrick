<?php
require_once("../conexion.php"); 
session_start();
$RegistrosAMostrar=20;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs && $permisos >= 1){
		$nombre = $rs['nombre'];
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if(empty($_GET[Eliminar])==false){
	$id_lis=$_GET[Eliminar];
	$revisa = pg_query($con,"select * from listado where id_usuario=$_SESSION[USUARIO] and id_lista = $id_lis ");
	if (($rs = pg_fetch_array($revisa)) || $permisos>1){
	$id_lis = is_numeric($_GET['Eliminar'])?$_GET['Eliminar']:0;
	$eliminar2 = pg_query($con,"delete from listado_usuario where id_lista = $id_lis");
	$eliminar3 = pg_query($con,"delete from listadojugador where id_lista = $id_lis");
	$eliminar = pg_query($con,"delete from listado where id_lista = $id_lis");
	}
	header('Location:listas.php');
}
if($_POST['Crear']){
	if($rs && $permisos > 1){
		$nombre = $rs['nombre'];
	
	$nom = str_replace(';', '',pg_escape_string($_POST['txt_nom_lis']));
	$ver = pg_query($con,"select * from listado where nombre = '$nom' order by nombre");
	if(!$saber = pg_fetch_array($ver)){
		$sentencia = "insert into listado values(DEFAULT,'$user','$nom')";
		$ejecutar = pg_query($con,$sentencia);
		$verificar = pg_query($con," insert into listado_usuario select id_lista,id_usuario from listado where id_lista not in (select id_lista from listado_usuario);");
	}else{
		header('Location:../error.php');
	}
	}
}
$titulo = "monitoreo";
require_once("../textos.php");
require_once("../head.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>
            <?php include_once("menu_listas.php");?>      
            </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
	
        <div class="list">
        <h3><?php echo $text["Administrar Listas"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["listas text1"][$_SESSION[IDIOMA]];?></p>
        
        <?php if ($permisos>1){?>
        <h3></br></br><?php echo $text["Crear nueva lista"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="listas.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["Nombre lista"][$_SESSION[IDIOMA]];?>:</td><td><input name="txt_nom_lis" type="text" size="15"></td>
                </tr>
                <tr>
                <td colspan="2" align="center"><input name="Crear" type="submit" id="Crear" value="Crear"></td>
                </tr>
                </table>        
        </form>
        <h3></br></br><?php echo $text["Listas creadas por ti"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["listas text2"][$_SESSION[IDIOMA]];?></p>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></th>           
            <th><?php echo $text["Modificar"][$_SESSION[IDIOMA]];?></th>
            <th>+ <?php echo $text["Scout"][$_SESSION[IDIOMA]];?></th>
            <th>+ <?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alarmas"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Ver Lista"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Ver equipos"][$_SESSION[IDIOMA]];?></th>
            <th><img src="../images/icons/delete_x16_h.ico"></th>
          </tr>
          <?php
		  $sentencia = "select * from listado where id_usuario = '$user' order by nombre asc";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs['nombre'];?></th>
            <td><?php echo $nombre;?></td>           
            <td><a href="/lista/mod_listas.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/refresh16.ico"></a></td>
            <td><a href="/lista/lis_usuarios.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/group_user16.png"></a></td>
            <td><a href="/lista/lis_jugadores.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/user_add16_h.png"></a></td>
            <td><a href="/lista/alertas.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/advertising.png"></a></td>            
            <td><a href="/lista/usuarios_lis.php?ID=<?php echo $rs['id_lista'];?>"><img src="/images/icons/print_preview16_h.ico"></a></td>
            <td><a href="/lista/equipos_lis.php?ID=<?php echo $rs['id_lista'];?>"><img src="../images/icons/application_form_magnify.png"></a></td>
            <td><a href="/lista/listas.php?Eliminar=<?php echo $rs['id_lista'];?>"><img src="/images/icons/delete_x16_h.ico"></a></td>
          </tr>
          <?
		  }
		  ?>
        </table>
        </div>
        <?php }?>
        <h3></br></br><?php echo $text["Listas a las que perteneces"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["listas text3"][$_SESSION[IDIOMA]];?></p>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></th> 
            <th>+ <?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alarmas"][$_SESSION[IDIOMA]];?></th>            
            <th><?php echo $text["Ver Lista"][$_SESSION[IDIOMA]];?></th>                      
          </tr>
          <?php
		  $sentencia = "select * from listado_usuario where id_usuario = '$user' order by id_lista asc";
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
		  	$id_listado = $rs['id_lista'];
			$encontrar = pg_query($con,"select id_usuario,nombre from listado where id_lista = $id_listado");
			if($hayado = pg_fetch_array($encontrar)){
		  		$id_coor = $hayado['id_usuario'];
				$coordinador = pg_query($con,"select nombre from usuario where id_usuario = $id_coor");
					if($pillado = pg_fetch_array($coordinador)){
          ?>
          <tr class="modo1">
            <th><?php echo $hayado['nombre'];?></th>
            <td><?php echo $pillado['nombre'];?></td> 
            <td><a href="/lista/lis_jugadores.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/user_add16_h.png"></a></td>  
            <td><a href="/lista/alertas.php?Lista=<?php echo $rs['id_lista'];?>"><img src="/images/icons/advertising.png"></a></td>            
            <td><a href="/lista/usuarios_lis.php?ID=<?php echo $rs['id_lista'];?>"><img src="/images/icons/print_preview16_h.ico"></a></td>
          </tr>
          <?
					}
				}
		  }
		  ?>
        </table>
        </div>
        <?php
		if($permisos >= 3){
		?>
        <h3></br></br><?php echo $text["Listas creadas (Administradores)"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["listas text4"][$_SESSION[IDIOMA]];?></p>
        <div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
          <tr>
            <th>ID</th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Coordinador"][$_SESSION[IDIOMA]];?></th>           
            <?php if($permisos == 4){?>
            <th><?php echo $text["Modificar"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Eliminar"][$_SESSION[IDIOMA]];?></th>
            <?php }?>
            <th><?php echo $text["Alarmas"][$_SESSION[IDIOMA]];?></th>            
            <th><?php echo $text["Ver Lista"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Ver equipos"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  $sentencia1 = "select * from listado order by nombre asc offset $RegistrosAEmpezar limit $RegistrosAMostrar";
		  $consultar1 = pg_query($con,$sentencia1);
		  while($rs1 = pg_fetch_array($consultar1)){
			$id_user = $rs1['id_usuario'];
			$sen = pg_query($con,"select nombre from usuario where id_usuario = $id_user");
			if($rs_nom = pg_fetch_array($sen)){
          ?>
          <tr class="modo1">
            <th><?php echo $rs1['id_lista'];?></th>
            <td><?php echo $rs1['nombre'];?></td>
            <td><?php echo $rs_nom['nombre'];?></td>           
            <?php if($permisos == 4){?>
            <td><a href="/lista/mod_listas.php?Lista=<?php echo $rs1['id_lista'];?>"><img src="/images/icons/refresh16.ico"></a></td>
            <td><a href="/lista/listas.php?Eliminar=<?php echo $rs1['id_lista'];?>"><img src="/images/icons/delete_x16_h.ico"></a></td>
            <?php }?>
            <td><a href="/lista/alertas.php?Lista=<?php echo $rs1['id_lista'];?>"><img src="/images/icons/advertising.png"></a></td>            
            <td><a href="/lista/usuarios_lis.php?ID=<?php echo $rs1['id_lista'];?>"><img src="/images/icons/print_preview16_h.ico"></a></td>
            <td><a href="/lista/equipos_lis.php?ID=<?php echo $rs1['id_lista'];?>"><img src="../images/icons/application_form_magnify.png"></a></td>
          </tr>
          <?
		  	}
		  }
		  ?>
        </table>
        <table><tr>
        <?php
$NroRegistros=pg_num_rows(pg_query($con,"select * from listado order by nombre asc"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;

 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='listas.php?pag=1'><img src='/images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='listas.php?pag=$PagAnt'><img src='/images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='listas.php?pag=$PagSig'><img src='/images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='listas.php?pag=$PagUlt'><img src='/images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
  ?>
  		</tr></table>
        <?php
		}
		?>
        </div>        
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>