<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$nacion = $_SESSION[ID_PAIS_LIGA];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($permisos < 3){
		header('Location:../error.php');
	}
}
$titulo = "monitoreo";
require_once("../textos.php");
require_once("../head.php");
require_once("../nombres.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>
            <?php include_once("menu_listas.php");?>      
            </div>
            <img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Info_DT"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["info1"][$_SESSION[IDIOMA]];?><br><br></p>
        <h3><?php echo $text["Consultar equipo"][$_SESSION[IDIOMA]];?></h3>
        <div class="list">
        <form action="info_dt.php" method="post">
        <table cellspacing="5" width="300">
          <tr>
            <td>ID del Equipo:</td>
            <td><input name="txt_club" type="text" value="<?php echo $_POST['txt_club'];?>"></td>
          </tr>
          <tr>
            <td align="center" colspan="2"><input name="Consultar" type="submit" id="Consultar" value="Consultar"></td>
          </tr>
        </table>
        </form>
        </div>
        <h3></br></br><?php echo $text["Info desplegada"][$_SESSION[IDIOMA]];?></h3>
        <div class="block"><div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla3">
          <tr>
            <th colspan="4" align="center"><?php echo $text["Titulo Info Equipo"][$_SESSION[IDIOMA]];?></th>
          </tr>
          <?php
		  if($_POST['Consultar']){
		  	$id_equipo = is_numeric($_POST['txt_club'])?$_POST['txt_club']:0;
		  	$sentencia_sql = pg_query($con,"select a.*,b.nombre,d.nivelentrenador,c.entreno,c.intensidad,c.condicion,e.nombre nombreentre from staff a,equipo b,entreno c left join jugador d on c.id_entrenador=d.id_jugador left join nombres e on c.entreno=e.id_hattrick where e.tipo='entrenamiento' and e.idioma='$_SESSION[IDIOMA]' and a.id_equipo=$id_equipo and b.id_equipo=$id_equipo and c.id_equipo=$id_equipo and d.id_equipo=$id_equipo");
			if($rs = pg_fetch_array($sentencia_sql)){
          ?>
          <tr class="modo1">
            <td><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[nombre];?></div></td>          	
            <td>ID <?php echo $text["Equipo"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[id_equipo];?></div></td>
          </tr>
          <tr class="modo1">
            <td><?php echo $text["Nivel Entrenador"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[nivelentrenador];?></div></td>          
            <td><?php echo $text["Entrenamiento"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[nombreentre];?></div></td>
          </tr>
          <tr class="modo1">
            <td><?php echo $text["Intensidad"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[intensidad];?></div></td>
            <td>% <?php echo $text["Condicion"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[condicion];?></div></td>
          </tr>
		  <tr class="modo1">
			<td><?php echo $text["Fisios"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[fisioterapeutas];?></div></td>
            <td><?php echo $text["Doctores"][$_SESSION[IDIOMA]];?></td>
            <td><div class="Estilo1"><?php echo $rs[doctores];?></div></td>
          </tr>
          <tr class="modo1">
            <td><?php echo $text["Auxiliares"][$_SESSION[IDIOMA]];?></td>  
            <td><div class="Estilo1"><?php echo $rs[auxiliares];?></div></td>
            <td><?php echo $text["Total Emp"][$_SESSION[IDIOMA]];?></td>  
            <td><div class="Estilo1"><?php echo $rs[doctores]+$rs[auxiliares]+$rs[fisioterapeutas]+$rs[psicologos]+$rs[portavoces];?></div></td>            
          </tr>
          <?
		  	}
		  }
		  ?>
        </table>
        </div>        
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>