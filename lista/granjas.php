<?php
require_once("../conexion.php"); 
session_start();
$RegistrosAMostrar=40;
if(isset($_GET['pag'])){
	$RegistrosAEmpezar=($_GET['pag']-1)*$RegistrosAMostrar;
	$PagAct=$_GET['pag'];
}else{
	$RegistrosAEmpezar=0;
	$PagAct=1;
}
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$nacion = $_SESSION[ID_PAIS_LIGA];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($permisos < 2){
		header('Location:../error.php');
	}
}
$can_usuarios = pg_query($con,"select count(id_equipo) from equipo where granja >= 0");
		if($rs_usu = pg_fetch_array($can_usuarios)){
			$conteo = $rs_usu[0];
		}
$titulo = "granjas";
require_once("../textos.php");
require_once("../head.php");
require_once("../nombres.php");
?>
<body><?php include_once("../seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="../images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="../images/top.gif" alt="" width="231" height="5" /><br />
			<div>
            <?php include_once("menu_listas.php");?>      
            </div>
            <img src="../images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Analizar Granjas"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["granjas text1"][$_SESSION[IDIOMA]];?><br><br></p>
        <h3><?php echo $text["Consultar por tipo de granja"][$_SESSION[IDIOMA]];?></h3>
        <div class="list">
        <form action="granjas.php" method="post">
        <table cellspacing="5" width="300">
          <tr>
            <td><?php echo $text["Tipo Entreno"][$_SESSION[IDIOMA]];?>:</td>
            <td><select name="cbo_granja" size="1" id="cbo_granja">
                  <option value="0"><?php echo $text["Cualquier entrenamiento"][$_SESSION[IDIOMA]];?></option>
                  <option value="2"><?php echo $text["Balon Parado"][$_SESSION[IDIOMA]];?></option>
                  <option value="3"><?php echo $text["Defensa"][$_SESSION[IDIOMA]];?></option>
                  <option value="4"><?php echo $text["Anotacion"][$_SESSION[IDIOMA]];?></option>
                  <option value="5"><?php echo $text["Lateral"][$_SESSION[IDIOMA]];?></option>
                  <option value="7"><?php echo $text["Asistencias"][$_SESSION[IDIOMA]];?></option>
                  <option value="8"><?php echo $text["Jugadas"][$_SESSION[IDIOMA]];?></option>
                  <option value="9"><?php echo $text["Porteria"][$_SESSION[IDIOMA]];?></option></select>
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2"><input name="Consultar" type="submit" id="Consultar" value="<?php echo $text["Consultar"][$_SESSION[IDIOMA]];?>"></td>
          </tr>
        </table>
        </form>
        </div>
        <h3></br></br><?php echo $text["Listado de granjas"][$_SESSION[IDIOMA]];?> / <?php echo $conteo." ";echo $text["Conteo de granjas"][$_SESSION[IDIOMA]];?></h3>
        <div class="block"><div class="block">
        <table border="0" cellpadding="0" cellspacing="0" class="tabla">
          <tr>
            <th>ID <?php echo $text["Equipo"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Nombre"][$_SESSION[IDIOMA]];?></th>
            <th><img src="../images/icons/onsale.png"></th>
            <th><?php echo $text["Entrenamiento"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Nivel Entrenador"][$_SESSION[IDIOMA]];?></th>
            <th>% <?php echo $text["Condicion"][$_SESSION[IDIOMA]];?></th>                                    
          </tr>
          <?php
		  if($_POST['Consultar']){
		  $entreno = $_POST['cbo_granja'];
		  $sentencia = "select * from equipo, entreno where granja = '$entreno' and pais = '$nacion' and equipo.id_equipo = entreno.id_equipo order by dinero desc";
		  }else{
		  $sentencia = "select * from equipo, entreno where granja >= 0 and pais = '$nacion' and equipo.id_equipo = entreno.id_equipo order by dinero desc offset $RegistrosAEmpezar limit $RegistrosAMostrar";
		  }
		  $consultar = pg_query($con,$sentencia);
		  while($rs = pg_fetch_array($consultar)){
		  	$dinero = $rs['dinero'];
			$tipo = $rs['granja'];
			$equipo = $rs['id_equipo'];
			$por_condicion = $rs['condicion'];
			if($tipo == 0){
				$entreno = "Cualquier entrenamiento";
			}elseif($tipo == 2){
				$entreno = "Balon Parado";
			}elseif($tipo == 3){
				$entreno = "Defensa";
			}elseif($tipo == 4){
				$entreno = "Anotacion";
			}elseif($tipo == 5){
				$entreno = "Lateral";
			}elseif($tipo == 7){
				$entreno = "Pases Cortos";
			}elseif($tipo == 8){
				$entreno = "Jugadas";
			}elseif($tipo == 9){
				$entreno = "Porteria";
			}
			$sen_dt = pg_query($con,"select nivelentrenador from jugador,entreno where entreno.id_equipo = '$equipo' and entreno.id_entrenador = jugador.id_jugador");
			if($sacar = pg_fetch_array($sen_dt)){
			$dt_nivel=denominacion($sacar['nivelentrenador'],$_SESSION[IDIOMA],'habilidad',$con);
          ?>
          <tr class="modo1">
            <th><?php echo $rs['id_equipo'];?></th>
            <td><?php echo $rs['nombre'];?></td>
            <td><?php echo "$".number_format($dinero,0,",",".");?></td>
            <td><?php echo $text[$entreno][$_SESSION[IDIOMA]];?></td>  
            <td><?php echo $dt_nivel;?></td>
            <td><?php echo $por_condicion;?>%</td>                                    
          </tr>
          <?
			}
		  }
		  ?>
        </table>
        <table><tr>
        <?php
		if(!$_POST['Consultar']){
$NroRegistros=pg_num_rows(pg_query($con,"select * from equipo where granja >= 0 and pais = '$nacion' order by dinero desc"));
		
 $PagAnt=$PagAct-1;
 $PagSig=$PagAct+1;
 $PagUlt=$NroRegistros/$RegistrosAMostrar;
 
 $Res=$NroRegistros%$RegistrosAMostrar;

 
  if($Res>0){
	
		$PagUlt=floor($PagUlt)+1;
	echo "<td><a href='/lista/granjas.php?pag=1'><img src='../images/icons/first16.ico'>&nbsp;</a></td>";
		
	if($PagAct>1){
	 echo "<td><a href='/lista/granjas.php?pag=$PagAnt'><img src='../images/icons/arrowleft_green16.ico'>&nbsp;</a></td>";
    }
	
	if($PagAct<$PagUlt){
	echo "<td><a href='/lista/granjas.php?pag=$PagSig'><img src='../images/icons/arrowright_green16.ico'>&nbsp;</a></td>";
	echo "<td><a href='/lista/granjas.php?pag=$PagUlt'><img src='../images/icons/last16.ico'>&nbsp;</a></td>";
 	}	
}
}
  ?>
  		</tr></table>
        </div>        
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>