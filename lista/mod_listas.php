<?php
require_once("../conexion.php");
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$consultar = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 2){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Lista']){
	$id_lis = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
	$_SESSION['ID_LISTA'] = $id_lis;
	$preguntar = pg_query($con,"select * from listado where id_lista = '$id_lis'");
	while($rs_lis = pg_fetch_array($preguntar)){
		$nom = $rs_lis['nombre'];
		$jefe = $rs_lis['id_usuario'];
	}
	$sacar = pg_query($con,"delete from listado_usuario where id_lista = '$id_lis' and id_usuario = '$jefe'");
}
if($_POST['Modificar']){
	$id = $_SESSION['ID_LISTA'];
	$nom = str_replace(';', '',pg_escape_string($_POST['txt_nom_lis']));
	$usu = $_POST['cbo_per'];
	$sentencia = "update listado set nombre = '$nom',id_usuario = '$usu' where id_lista = '$id'";
	$ejecutar = pg_query($con,$sentencia);
	$revisar = pg_query($con,"select * from listado_usuario where id_lista = '$id' and id_usuario = '$usu'");
	if(!$revisado = pg_fetch_array($revisar)){
		$actualizar = pg_query($con,"insert into listado_usuario values('$id','$usu')");
	}
	header('Location:listas.php');
}
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/titleadm.gif" alt="" width="209" height="30" />
				<p><strong><?php echo $text["Usuario"][$_SESSION[IDIOMA]];?>: <?php echo $nombre;?></strong>.
                <br><br><?php echo $text["Seccion para modificar listas en Botrick"][$_SESSION[IDIOMA]];?>.
                <br><br><!--agregar acá más links -->
				</p>
	  	  </div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Modificar lista"][$_SESSION[IDIOMA]];?></h3>
        <div class="block">
        <form action="mod_listas.php" method="post">
        <table width="300" cellspacing="7">
                <tr>
                <td><?php echo $text["Nombre lista"][$_SESSION[IDIOMA]];?>:</td><td><input name="txt_nom_lis" type="text" size="15" value="<?php echo $nom;?>"></td>
                </tr>
                <tr>
                <td><?php echo $text["Dueno de lista"][$_SESSION[IDIOMA]];?>:</td><td><select name="cbo_per" size="1" id="cbo_per" <?php if($permisos != 4){?>readonly="readonly"<?php }?>>
                  <?php
	  			  $listar = "select usuario.nombre,usuario.id_usuario from usuario,perfil where usuario.id_perfil = perfil.id_perfil and perfil.permisos > 1";
				  $ejecutar = pg_query($con,$listar);
				  while($rs = pg_fetch_array($ejecutar)){
				  ?>
				  <option value=<?php echo $rs['id_usuario'];if($rs['id_usuario'] == $jefe){ echo " selected";}?>><?php echo $rs['nombre'];?></option>
                  <?php
				  }
				  ?>
                </select></td>
                </tr>              
                <tr>
                <td colspan="2" align="center"><input name="Modificar" type="submit" id="Modificar" value="<?php echo $text["Modificar"][$_SESSION[IDIOMA]];?>"></td>
                </tr>
                </table>        
        </form>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>