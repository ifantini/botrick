<?php
require_once("../conexion.php"); 
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$nacion = $_SESSION[ID_PAIS_LIGA];
	$consultar = pg_query($con,"select usuario.nombre,perfil.pais from usuario,perfil where usuario.id_usuario = '$user' and usuario.id_perfil = perfil.id_perfil");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$nombre = $rs['nombre'];
		if($permisos < 1){
			header('Location:../error.php');
		}
	}else{
		header('Location:../error.php');
	}
}else{
	header('Location:../error.php');
}
if($_GET['Lista']){
	$_SESSION['ID_LISTA'] = is_numeric($_GET['Lista'])?$_GET['Lista']:0;
	$id_lista = $_SESSION['ID_LISTA'];
}
$titulo = "alertas";
require_once("../textos.php");
require_once("../head.php");
?>
<body>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="administracion"; include_once("../mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>
				<?php include_once("menu_listas.php");?> 
			</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
        <h3><?php echo $text["Alerta de BOT"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["alertas text1"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
			  $hay = FALSE;
			  $listar = pg_query($con,"select a.id_jugador,b.nombre,extract(DAYS from now()-d.lastlogin) dias from listadojugador a left join jugador b on a.id_jugador=b.id_jugador left join equipo c on b.id_equipo=c.id_equipo left join manager d on c.id_manager=d.id_manager where extract(DAYS from now()-d.lastlogin)>30 and a.id_lista=$id_lista order by extract(DAYS from now()-d.lastlogin) desc");
			  while($rsteam = pg_fetch_array($listar)){
			  	$hay = TRUE;
				$id_player_cat = $rsteam['id_jugador'];
				$saber_cat = pg_query($con,"select nick from planes_entrenamiento, jugador where jugador.id_jugador = $id_player_cat and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento");
				if($tener_cat = pg_fetch_array($saber_cat)){
					$nick = $tener_cat['nick'];
				}else{
					$nick = "S/C";
				}
        ?>
        <tr class="modo1">
        	<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
            <td><?php echo $text["El jugador no se ha conectado hace "][$_SESSION[IDIOMA]].$rsteam['dias']." ".$text["dias"][$_SESSION[IDIOMA]];?> </td>
            <td><?php echo $nick;?></td>
        </tr> 
        <?php
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="3" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		?>  
        </table>
        <h3><br><br><?php echo $text["Alerta de Quiebra"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["alertas text2"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
			  $hay = FALSE;
			  $listar = pg_query($con,"select a.id_jugador,b.nombre,dinero from listadojugador a left join jugador b on a.id_jugador=b.id_jugador left join equipo c on b.id_equipo=c.id_equipo where dinero<20000 and a.id_lista=$id_lista order by dinero");
			  while($rsteam = pg_fetch_array($listar)){
			  	$hay = TRUE;
				$id_player_cat = $rsteam['id_jugador'];
				$saber_cat = pg_query($con,"select nick from planes_entrenamiento, jugador where jugador.id_jugador = $id_player_cat and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento");
				if($tener_cat = pg_fetch_array($saber_cat)){
					$nick = $tener_cat['nick'];
				}else{
					$nick = "S/C";
				}
        ?>
        <tr class="modo1">
        	<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
            <td><?php echo $text["Al equipo le quedan solo "][$_SESSION[IDIOMA]].number_format($rsteam['dinero'],0,",",".");?>.000 pesos.</td>
        	<td><?php echo $nick;?></td>
        </tr> 
        <?php
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="3" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		?>         
        </table>
        <h3><br><br><?php echo $text["Alerta de Inactividad"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["alertas text3"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
			  $hay = FALSE;
			  $listar = pg_query($con,"select a.id_jugador,b.nombre from listadojugador a left join jugador b on a.id_jugador=b.id_jugador where not activo and a.id_lista=$id_lista");
			  while($rsteam = pg_fetch_array($listar)){
			  	$hay = TRUE;
				$id_player_cat = $rsteam['id_jugador'];
				$saber_cat = pg_query($con,"select nick from planes_entrenamiento, jugador where jugador.id_jugador = $id_player_cat and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento");
				if($tener_cat = pg_fetch_array($saber_cat)){
					$nick = $tener_cat['nick'];
				}else{
					$nick = "S/C";
				}
        ?>
        <tr class="modo1">
        	<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
            <td><?php echo $text["El jugador no se esta actualizando correctamente."][$_SESSION[IDIOMA]];?></td>
        	<td><?php echo $nick;?></td>
        </tr> 
        <?php
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="3" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		?>      
        </table>
        <h3><br><br><?php echo $text["Alerta de Comentarios no leidos"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["alertas text4"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
			  $hay = FALSE;
			  $listar = pg_query($con,"select a.id_jugador,b.nombre,count(*) mensajes from listadojugador a left join jugador b on a.id_jugador=b.id_jugador left join comentario_jugador c on a.id_jugador=c.id_jugador where a.id_lista=$id_lista and not c.leido group by a.id_jugador,b.nombre having count(*)>0");
			  while($rsteam = pg_fetch_array($listar)){
			  	$hay = TRUE;
				$id_player_cat = $rsteam['id_jugador'];
				$saber_cat = pg_query($con,"select nick from planes_entrenamiento, jugador where jugador.id_jugador = $id_player_cat and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento");
				if($tener_cat = pg_fetch_array($saber_cat)){
					$nick = $tener_cat['nick'];
				}else{
					$nick = "S/C";
				}
        ?>
        <tr class="modo1">
        	<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
            <td><?php echo $text["El jugador tiene "][$_SESSION[IDIOMA]].$rsteam['mensajes'].$text[" mensajes sin leer."][$_SESSION[IDIOMA]];?></td>
        	<td><?php echo $nick;?></td>
        </tr> 
        <?php
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="3" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		?>      
        </table>
        <h3><br><br><?php echo $text["Alerta de Venta"][$_SESSION[IDIOMA]];?></h3>
        <p><?php echo $text["alertas text4"][$_SESSION[IDIOMA]];?></br></br>
        </p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla_alerta">
        <tr>
        	<th><?php echo $text["Jugador"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Alerta"][$_SESSION[IDIOMA]];?></th>
            <th><?php echo $text["Categoria_alerta"][$_SESSION[IDIOMA]];?></th>
        </tr>
        <?php
			  $hay = FALSE;
			  $listar = pg_query($con,"select a.id_jugador,b.nombre from listadojugador a left join jugador b on a.id_jugador=b.id_jugador where en_venta and a.id_lista=$id_lista");
			  while($rsteam = pg_fetch_array($listar)){
			  	$hay = TRUE;
				$id_player_cat = $rsteam['id_jugador'];
				$saber_cat = pg_query($con,"select nick from planes_entrenamiento, jugador where jugador.id_jugador = $id_player_cat and jugador.id_planes_entrenamiento = planes_entrenamiento.id_planes_entrenamiento");
				if($tener_cat = pg_fetch_array($saber_cat)){
					$nick = $tener_cat['nick'];
				}else{
					$nick = "S/C";
				}
        ?>
        <tr class="modo1">
        	<th><a href="/datos_jugador.php?id=<?php echo $rsteam['id_jugador']; ?>" TARGET="_blank"><?php echo substr($rsteam['nombre'],0,23);?></a></th>
            <td><?php echo $text["El jugador esta en venta"][$_SESSION[IDIOMA]];?>.</td>
        	<td><?php echo $nick;?></td>
        </tr> 
        <?php
			  }
			  if($hay == FALSE){
		?>         
        <tr class="modo1">
        	<th colspan="3" align="center"><?php echo $text["No hay alertas activas"][$_SESSION[IDIOMA]];?></th>
        </tr> 
        <?php
		}
		?>      
        </table>
        </div>
        </div>
        <?php include_once('../footer.php');?>
    </body>
</html>