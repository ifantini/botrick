<?php
require_once("conexion.php");
require_once("nombres.php");
session_start();
if (isset ($_GET[lang]) ){
	$_SESSION[IDIOMA]=$_GET[lang];
}

if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
	$cargo = $_SESSION['CARGO'];
	$conocer = pg_query($con,"select * from usuario where id_usuario = '$user'");
	$saber = pg_fetch_array($conocer);
	if($saber){
		$nombre = $saber['nombre'];
	}
	$consultar = pg_query($con,"select b.nombre,b.pais,b.id_equipo from usuario a left join equipo b on a.id_manager=b.id_manager where a.id_usuario = $user");
	$rs = pg_fetch_array($consultar);
	if($rs){
		$teamname = $rs['nombre'];
		$country = $rs['pais'];
		$idteam = $rs['id_equipo'];
		$sentencia = pg_query($con,"select * from equipo,usuario where id_usuario = $user and usuario.id_manager = equipo.id_manager and activo = TRUE");
		if($rs_sen = pg_fetch_array($sentencia)){
			
			$creado = $rs_sen['fechacreacion'];
			$lev_con = $rs_sen['confianza'];
			$lev_esp = $rs_sen['espiritu'];
			$socios = $rs_sen['socios'];
			$confianza=denominacion($lev_con,$_SESSION[IDIOMA],'confianza',$con);
			$espiritu=denominacion($lev_esp,$_SESSION[IDIOMA],'espiritu',$con);
		} 
	}
	else{
		header('Location:error.php');
	}
}else{
	header('Location:error.php');
}

require_once("textos.php");
require_once("head.php");
?>
<body><?php include_once("seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																								
			<div class="search"></div>
		</div>
	</div>
	<div id="content">
		<?php $select="home"; include_once("mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />																																																																																																
			<div>	
				<img src="/images/titlebien.gif" alt="" width="209" height="30" /><br />
				<p><?php echo $text["home text1"][$_SESSION[IDIOMA]];?>.</p>
                    <br><br><p align="center"><strong><?php echo $text["Bienvenido"][$_SESSION[IDIOMA]];?> <?php echo $nombre;?>.</strong></p>
                <div class="Estilo1" align="center"><?php echo $text["Perfil"][$_SESSION[IDIOMA]];?>: <?php echo $cargo;?>.</div>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-3434860615159871";
/* Home_1 */
google_ad_slot = "5628451744";
google_ad_width = 180;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>			
</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <?php
		if($permisos == 4){
		?>
        <div class="list">
        <h3><a href="/admin/administracion.php"><?php echo $text["Administracion"][$_SESSION[IDIOMA]];?></a></h3>
        <p><?php echo $text["home text2"][$_SESSION[IDIOMA]];?>:<br><br></p>
        <table border="0" cellpadding="0" cellspacing="0" class="tabla2">
        <tr>
        	<th><?php echo $text["home text3"][$_SESSION[IDIOMA]];?>:</th>
            <th><?php echo $text["home text4"][$_SESSION[IDIOMA]];?>:</th>
            
        </tr>
        <tr class="modo1">
        	<td><a href="admin/usuarios.php"><?php echo $text["Usuarios"][$_SESSION[IDIOMA]];?></a></td>
            <td><a href="admin/permisos.php"><?php echo $text["Permisos"][$_SESSION[IDIOMA]];?></a></td>
        </tr>
        </table>
        </div>
        <?php
		}
		?>
        <div class="list">
            <table border=”0″ cellpadding=”0″ cellspacing=”0 class="tabla3">
            <tr>
            	<th colspan="4"><?php echo $text["home text5"][$_SESSION[IDIOMA]];?></th>
            </tr>
            <tr class="modo1">
                <td colspan="2" class="flag"><img src="/images/flags/transparent.gif" style="background: transparent url(/images/flags/flags.gif) no-repeat -<?php echo 20*$country;?>px 0;"></img></td>
                <td colspan="2"><?php echo $teamname;?>&nbsp;(<?php echo $idteam;?>)</td>
			</tr>
            <tr class="modo1">
            	<td><strong><?php echo $text["Socios"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo number_format($socios,0,",",".");?></td>
                <td><strong><?php echo $text["home text6"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $creado;?></td>
            </tr>
            <tr class="modo1">
            	<td><strong><?php echo $text["Confianza"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $confianza;?></td>
                <td><strong><?php echo $text["Espiritu"][$_SESSION[IDIOMA]];?>:</strong></td>
                <td><?php echo $espiritu;?></td>
            </tr>
            </table>
			<h3><br><?php echo $text["Bienvenido"][$_SESSION[IDIOMA]];?></h3>
			<p><?php echo $text["Hola"][$_SESSION[IDIOMA]];?> <?php echo $nombre;?><?php echo $text["home text7"][$_SESSION[IDIOMA]];?>:<br><br>
			<strong><?php echo $text["home text8"][$_SESSION[IDIOMA]];?></strong>: <?php echo $text["home text9"][$_SESSION[IDIOMA]];?>.<br><br>
			<strong><?php echo $text["Jugadores"][$_SESSION[IDIOMA]];?></strong>: <?php echo $text["home text10"][$_SESSION[IDIOMA]];?>.<br><br>
			<strong><?php echo $text["home text11"][$_SESSION[IDIOMA]];?></strong>: <?php echo $text["home text12"][$_SESSION[IDIOMA]];?>.<br><br>
			<strong><?php echo $text["Salir"][$_SESSION[IDIOMA]];?></strong>: <?php echo $text["home text13"][$_SESSION[IDIOMA]];?>.</p>
		</div>
	</div>
	<?php include_once('footer.php');?>
</body>
</html>
