<?php
require_once("conexion.php");
session_start();
if (isset ($_SESSION['USUARIO']) ){
	$user = $_SESSION['USUARIO'];
	$permisos = $_SESSION['PERMISOS'];
}
require_once("textos.php");
require_once("head.php");
?>
<body><?php include_once("seguimientoanalytics.php");?>
	<div id="header">
		<div>
			<a href="/<?php if(isset ($_SESSION['USUARIO']) ){echo "home";}else{echo "index";}?>.php" class="logo"><img src="/images/logo2.png" alt="" width="192" height="42" /></a>																																																	
			<div class="search"></div>
	  </div>
	</div>
	<div id="content">
		<?php $select="donaciones"; include_once("mainmenu.php");?>
		<div class="column">
			<img src="/images/top.gif" alt="" width="231" height="5" /><br />
			<div>	
				<img src="/images/titledonaciones.gif" alt="" width="209" height="30" />
                <p><?php echo $text["donaciones text1"][$_SESSION[IDIOMA]];?>.</p>
                <img style="padding: 50px;" src="https://chile.dineromail.com/imagenes/medios-pago1.gif"/>
		  	</div>
			<img src="/images/bot.gif" alt="" width="231" height="5" /><br />
		</div>
        <div class="list">
   
        <div class="block">
        <?php if ($_GET['donacion']>0){?>
        <h3><?php echo $text["donaciones text2"][$_SESSION[IDIOMA]];?><?php echo number_format($_GET['donacion'],0,",",".");?> pesos.</h3>
        <?php }?>
		<table class="tabla2 sortable">
			<tr>
				<th><?php echo $text["donaciones text3"][$_SESSION[IDIOMA]];?></th>
				<th><?php echo $text["donaciones text4"][$_SESSION[IDIOMA]];?></th>
			</tr>
			<tr class="modo1">
				<td>2.500 pesos</td>
				<td>
					<form action='https://chile.dineromail.com/Shop/Shop_Ingreso.asp' method='post'> <input type='hidden' name='NombreItem' value='Donaciones Botrick'> <input type='hidden' name='TipoMoneda' value='1'> <input type='hidden' name='PrecioItem' value='2500.00'> <input type='hidden' name='E_Comercio' value='473457'> <input type='hidden' name='NroItem' value='-'> <input type='hidden' name='DireccionExito' value='http://hattrick.altirogames.com/donaciones.php?donacion=2500'> <input type='hidden' name='DireccionFracaso' value='http://hattrick.altirogames.com/donaciones.php'> <input type='hidden' name='DireccionEnvio' value='1'> <input type='hidden' name='Mensaje' value='1'> <input type='hidden' name='MediosPago' value='4,5,6,21,23,2,7'> <input type='image' src='https://chile.dineromail.com/imagenes/botones/donar_bn.gif' border='0' name='submit' alt='Pagar con DineroMail'> </form>
				</td>
			</tr>
			<tr class="modo1">
				<td>5.000 pesos</td>
				<td>
					<form action='https://chile.dineromail.com/Shop/Shop_Ingreso.asp' method='post'> <input type='hidden' name='NombreItem' value='Donaciones Botrick'> <input type='hidden' name='TipoMoneda' value='1'> <input type='hidden' name='PrecioItem' value='5000.00'> <input type='hidden' name='E_Comercio' value='473457'> <input type='hidden' name='NroItem' value='-'> <input type='hidden' name='DireccionExito' value='http://hattrick.altirogames.com/donaciones.php?donacion=5000'> <input type='hidden' name='DireccionFracaso' value='http://hattrick.altirogames.com/donaciones.php'> <input type='hidden' name='DireccionEnvio' value='1'> <input type='hidden' name='Mensaje' value='1'> <input type='hidden' name='MediosPago' value='4,5,6,21,23,2,7'> <input type='image' src='https://chile.dineromail.com/imagenes/botones/donar_bn.gif' border='0' name='submit' alt='Pagar con DineroMail'> </form>
				</td>
			</tr>
			<tr class="modo1">
				<td>10.000 pesos</td>
				<td>
					<form action='https://chile.dineromail.com/Shop/Shop_Ingreso.asp' method='post'> <input type='hidden' name='NombreItem' value='Donaciones Botrick'> <input type='hidden' name='TipoMoneda' value='1'> <input type='hidden' name='PrecioItem' value='10000.00'> <input type='hidden' name='E_Comercio' value='473457'> <input type='hidden' name='NroItem' value='-'> <input type='hidden' name='DireccionExito' value='http://hattrick.altirogames.com/donaciones.php?donacion=10000'> <input type='hidden' name='DireccionFracaso' value='http://hattrick.altirogames.com/donaciones.php'> <input type='hidden' name='DireccionEnvio' value='1'> <input type='hidden' name='Mensaje' value='1'> <input type='hidden' name='MediosPago' value='4,5,6,21,23,2,7'> <input type='image' src='https://chile.dineromail.com/imagenes/botones/donar_bn.gif' border='0' name='submit' alt='Pagar con DineroMail'> </form>
				</td>
			</tr>
			<tr class="modo1">
				<td>20.000 pesos</td>
				<td>
					<form action='https://chile.dineromail.com/Shop/Shop_Ingreso.asp' method='post'> <input type='hidden' name='NombreItem' value='Donaciones Botrick'> <input type='hidden' name='TipoMoneda' value='1'> <input type='hidden' name='PrecioItem' value='20000.00'> <input type='hidden' name='E_Comercio' value='473457'> <input type='hidden' name='NroItem' value='-'> <input type='hidden' name='DireccionExito' value='http://hattrick.altirogames.com/donaciones.php?donacion=20000'> <input type='hidden' name='DireccionFracaso' value='http://hattrick.altirogames.com/donaciones.php'> <input type='hidden' name='DireccionEnvio' value='1'> <input type='hidden' name='Mensaje' value='1'> <input type='hidden' name='MediosPago' value='4,5,6,21,23,2,7'> <input type='image' src='https://chile.dineromail.com/imagenes/botones/donar_bn.gif' border='0' name='submit' alt='Pagar con DineroMail'> </form>
				</td>
			</tr>
		</table>
		</div>	
        </div>
        <?php include_once('footer.php');?>
    </body>
</html>