import time,urllib2,os

def act_diaria():
    try:
        response = urllib2.urlopen('http://botrick.altirogames.com/escaneajugadores.php')
        html = response.read()
        print 'diaria completada con exito'
        return True
    except:
        print 'cago la diaria'
        return False

def act_semanal(temporada,semana):
    resp=urllib2.urlopen("http://botrick.altirogames.com/id_usuario.php?temp="+str(temporada)+"&semana="+str(semana))
    ids = resp.read().split('.')
    tot=0
    for id in ids:
        tot+=1
        try:
            link="http://botrick.altirogames.com/act_equipo_masiva.php?id="+str(id)+"&temp="+str(temporada)+"&semana="+str(semana)
            resp=urllib2.urlopen(link)
            print round(100.*tot/len(ids),2)
        except:
            print 'cago '+str(id)
    return True

os.environ['TZ'] = 'Europe/Amsterdam'
time.tzset()
diaria=False
while(True):
    ahora=time.localtime()
    #inicio en semana 0 temporada 0
    t=864601200.0
    temporada=int((time.time()-t)/24/3600/7/16)
    semana=int((time.time()-t)/24/3600/7)%16
    print temporada,semana,ahora.tm_wday,str(ahora.tm_hour)+':'+str(ahora.tm_min)+':'+str(ahora.tm_sec),diaria
    if not diaria and ahora.tm_hour>=10:
        if ahora.tm_wday==4:
            diaria = act_semanal(temporada,semana)
        else:
            diaria = act_diaria()
    time.sleep(60)